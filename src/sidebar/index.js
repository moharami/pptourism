import React, {Component} from 'react';
import {Text, View,Dimensions, ScrollView, AsyncStorage, Alert, TouchableOpacity, Image} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import Axios from 'axios';
import Loader from '../components/loader'
import menu from '../assets/menu.png'
import circle from '../assets/circle.png'
import FIcon from 'react-native-vector-icons/dist/EvilIcons'

class Sidebar extends React.Component {
    constructor(){
        super();
        this.state = {
            loading: false,
            user: {},
            show: false,
            modalVisible: false,
            request: false,
            showMore : false,
            login: false
        };
    }
    closeModal() {
        this.setState({modalVisible: false});
    }
    componentWillMount() {

    }
    componentWillReceiveProps() {
        this.forceUpdate();

        let loged2 = null;
            AsyncStorage.getItem('loged').then((info) => {
                if(info !== null) {
                    const newInfo = JSON.parse(info);
                    newInfo.loged === true ? this.setState({login: true}) : null;
                }
            })
        AsyncStorage.getItem('showMore').then((info) => {
            if(info !== null) {
                const newInfo = JSON.parse(info);
                newInfo.showMore === true ? this.setState({showMore: true}) : this.setState({showMore: false});
            }
        })
    }
    logoutUser() {
        AsyncStorage.removeItem('token');
        Alert.alert('', 'شما با موفقیت از حساب کاربری خود خارج شدید')
        Actions.drawerClose();
    }
    filterAction(name) {
        AsyncStorage.getItem('token').then((info) => {
            if(info === null) {
                console.log('it is not login')
                Alert.alert('','لطفا ابتدا وارد شوید');
                if(name === 'newPlace') {
                    Actions.loginPage({newPlace: true});
                }
                else if(name === 'addedPlace') {
                    Actions.loginPage({addedPlace: true});
                }
                else if(name === 'support') {
                    Actions.loginPage({support: true});
                }
                Actions.drawerClose();
            }
            else {
                if(name === 'newPlace') {
                    Actions.newPlace({item: null})
                }
                else if(name === 'addedPlace') {
                    Actions.addedPlace()
                }
                else if(name === 'support') {
                    Actions.support()
                }
            }
        });
    }
    render(){
        // const {loged} = this.props;

        if(this.state.loading){
            return (<Loader />)
        }
        else return (

                <View style={styles.container}>
                    <Image source={menu} style={styles.image} />
                    <Image source={circle} style={styles.circle} />
                    <Image source={circle} style={styles.circle2} />
                    <Image source={circle} style={styles.circle3} />
                    <Text style={styles.title}>مریم احمدی</Text>
                    <Text style={styles.subTitle}>عضو همیار</Text>
                    <TouchableOpacity onPress={()=> Actions.drawerClose()} style={{position: 'absolute', zIndex: 100, top: 15, left: 15}}>
                        <FIcon name="close" size={25} color="white"  />
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => {Actions.drawerClose();Actions.home()}}>
                        <Text style={[styles.item, {paddingTop: '15%'}]}>صفحه اصلی</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.filterAction('newPlace')}>
                        <Text style={styles.item}>ثبت مکان جدید</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.filterAction('addedPlace') }>
                        <Text style={styles.item}>مکان های ثبت شده</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => null}>
                        <Text style={styles.item}>مسیریابی</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.filterAction('support')}>
                        <Text style={[styles.item, {paddingBottom: '15%'}]}>پشتیبانی</Text>
                    </TouchableOpacity>
                    <View style={styles.button}>
                        <TouchableOpacity onPress={() => this.logoutUser()} >
                            <Text style={{color: 'white', fontSize: 12,
                                paddingRight: 10,
                                textAlign: 'center',
                                paddingTop: 10,
                                paddingBottom: 10,
                            }}>خروج</Text>
                        </TouchableOpacity>
                    </View>
                </View>
        );
    }
}

export default Sidebar;