import { StyleSheet, Dimensions } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    scroll: {
        flex: 1,
        // backgroundColor: '#353b48',
    },
    container: {
        // backgroundColor: '#21b34f',
        // backgroundColor: '#353b48',
        alignItems: 'center',
        justifyContent: 'flex-end',
        // paddingTop: 30
        // height: Dimensions.get('window').height
    },
    profile: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingRight: 15,
        paddingBottom: 40
    },
    imageContainer: {
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20,
        borderRadius: 100,
        elevation: 8
    },
    image: {
        width: '100%',
        resizeMode: 'stretch'
    },
    name: {
        color: 'white',
        fontSize: 16,
        paddingRight: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    title: {
        paddingRight: 10,
        fontSize: 16,
        color: 'white',
        textAlign: 'center',
        paddingTop: 10,
        position: 'absolute',
        zIndex: 2,
        top: 30,
        left: '37%'

    },
    button: {
        alignItems: "center",
        justifyContent: 'center',
        borderRadius: 3,
        backgroundColor: 'rgb(255, 87, 34)',
        // marginLeft: 5,
        paddingRight: 40,
        paddingLeft: 40,

    },
    subTitle: {
        paddingRight: 10,
        fontSize: 14,
        color: 'white',
        textAlign: 'center',
        position: 'absolute',
        zIndex: 2,
        top: 70,
        left: '40%'
    },
    item: {
        paddingRight: 10,
        fontSize: 16,
        color: 'rgb(50, 50, 50)',
        textAlign: 'center',
        paddingTop: 10,
        paddingBottom: 15,
        borderBottomWidth: 1,
        borderBottomColor: 'lightgray',
    },
    circle: {
        width: 24,
        resizeMode: 'contain',
        position: 'absolute',
        zIndex: 10,
        top: '36%',
        left: 50
    },
    circle2: {
        width: 18,
        resizeMode: 'contain',
        position: 'absolute',
        zIndex: 10,
        top: '28%',
        left: 25
    },
    circle3: {
        width: 13,
        resizeMode: 'contain',
        position: 'absolute',
        zIndex: 10,
        top: '35%',
        right: 60
    }
});
