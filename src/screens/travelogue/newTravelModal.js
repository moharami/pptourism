
import React, { Component } from 'react';
import {
    Modal,
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    TextInput,
    Alert,
    ScrollView,
    ImageBackground,
    AsyncStorage
}
    from 'react-native'
import Icon from 'react-native-vector-icons/dist/FontAwesome'
import FIcon from 'react-native-vector-icons/dist/Feather'
import MIcon from 'react-native-vector-icons/dist/MaterialIcons'
import Axios from 'axios';
export const url = 'http://ppt.azarinpro.info';
Axios.defaults.baseURL = url;
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
import {Actions} from 'react-native-router-flux'

import Selectbox from 'react-native-selectbox'
import ImagePicker from 'react-native-image-picker';
import {
    BallIndicator
} from 'react-native-indicators';

class NewTravelModal extends Component {
    state = {
        text: '',
        modalVisible: false,
        loading: false,
        what: '',
        tags: [],
        tag: 0,
        active: '',
        title: '',
        travelStatus:{key: 0, label: "انتخاب نشده", value: ''},
        travelStatusTitle:null,
        content: '',
        imageCount: 0,
        imageArray: []
    };

    componentWillMount() {
        // Axios.get('/business/tag/show').then(response=> {
        //     let newTags = [];
        //     newTags.push({title: 'تگ', id: 0});
        //     response.data.data.map((item)=> newTags.push(item))
        //     this.setState({loading: false, tags: newTags});
        //     console.log('newtags', newTags)
        // })
        //     .catch((error) => {
        //         Alert.alert('مشکلی در برقراری ارتباط با سرور رخ داده لطفا مجددا تلاش نمایید')
        //         this.setState({loading: false});
        //     });
    }
    closeModal() {
        this.setState({modalVisible: false});
    }
    selectPhotoTapped2() {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true,

            }
        };
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };

                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };

                let countArray = {id: this.state.imageCount + 1, src: response.uri};
                const newImageArray =  this.state.imageArray;
                newImageArray.push(countArray);
                this.setState({
                    imageCount: ++this.state.imageCount,
                    imageArray: newImageArray
                })
                // let imgArray = this.state.imageArray;
                // let imgData = this.state.imageData;
                // imgArray[item.id-1].src = source;
                // imgData[item.id-1] = response.uri;
                //
                // this.setState({
                //     imageArray: imgArray,
                //     imageData: imgData
                // });
            }
        });
    }


    deleteImg(id) {
        const newImageArray = this.state.imageArray.filter(item => item.id !== id);
        this.setState({
            imageArray: newImageArray
        })
    }
    addTravel() {
        AsyncStorage.getItem('token').then((info) => {
            if(info !== null) {
                const newInfo = JSON.parse(info);
                console.log('token', newInfo.token)
                Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;

                let formdata = new FormData();
                this.state.title !== '' && formdata.append("title", this.state.title)
                this.state.content !== '' &&  formdata.append("content", this.state.content)
                this.state.travelStatus.value !== '' && formdata.append("type", this.state.travelStatus.value)
                let placeable_type = "";
                if(this.props.subName === 'شهر') {
                    placeable_type = "Modules\\Country\\Entities\\City"
                }
                else if(this.props.subName === 'استان') {
                    placeable_type = "Modules/\Country\\Entities\\State"
                }
                else if(this.props.subName === 'کشور') {
                    placeable_type = "Modules\\Country\\Entities\\Country"
                }
                formdata.append("placeable_id", this.props.placeId)
                formdata.append("placeable_type", placeable_type)
                console.log('placeable_type', placeable_type)

                if(this.state.imageArray.length !== 0){
                    this.state.imageArray.map((item) => {
                        formdata.append("type", "image");
                        formdata.append("document[]", {
                            uri: item.src,
                            type: 'image/jpeg',
                            name: 'document[]'
                        });
                    })
                }
                console.log('Formdata', formdata)

                this.setState({loading: true});
                Axios.post('/country/travelogue/store', formdata
                ).then(response=> {
                    this.setState({loading: false});
                    Alert.alert('','سفرنامه شما با موفقیت افزوده شد');
                    this.props.closeModal();
                    console.log('travel addding    response', response.data);
                })
                    .catch((response) => {
                        console.log('resssponse erroreie create ', response.response.data.message);
                        if(response.response.data.message === "Unauthenticated."){
                            Alert.alert('','لطفا ابتدا وارد شوید');
                            this.setState({loading: false});
                            Actions.loginPage({newPlace: true});
                        }
                    });

            }
            else {
                console.log('it is not login')
                Actions.loginPage({travel: true})
            }
        });
    }
    render() {
        const travelItems = [
            {key: 0, label: "انتخاب نشده", value: ""},
            {key: 1, label: "رفته", value: "gone"},
            {key: 2, label: "می خواهد برود", value: "want"}
        ]
        // if(this.state.loading){
        //     return <Loader />
        // }
        return (
            <View style={styles.container}>
                <Modal animationType = {"fade"} transparent = {true}
                       visible = {this.props.modalVisible}
                       onRequestClose = {() => this.props.onChange(false)}
                       onBackdropPress= {() => this.props.onChange(!this.state.modalVisible)}
                >
                    <View style = {styles.modal}>
                        <View style={styles.box}>
                            <View style={styles.headerContainer}>
                                <TouchableOpacity onPress={() => this.props.closeModal()}>
                                    <Icon name="close" size={20} color={ "black"} />
                                </TouchableOpacity>
                                <Text style={styles.headerText}>{this.props.title}</Text>
                            </View>
                            <ScrollView style={{width: '100%'}}>
                                <View style={{alignItems: 'center', justifyContent: 'center'}}>
                                    <Text style={styles.text}>افزودن عکس</Text>
                                    <View style={styles.photoContainer}>
                                        <TouchableOpacity onPress={() => this.selectPhotoTapped2()}>
                                            <View style={styles.addImage2}>
                                                <Icon name="camera" size={20} color="white"/>
                                                <Text style={styles.addText2}>افزودن عکس</Text>
                                            </View>
                                        </TouchableOpacity>
                                        {this.state.imageCount !== 0 ?
                                            this.state.imageArray.map((item, index) => {return <ImageBackground style={styles.Image2} source={{uri: item.src}} key={index}>
                                                <View style={{
                                                    backgroundColor:'rgba(0,0,0,.6)',
                                                    height: 40,
                                                    width: 40,
                                                    alignItems: "center",
                                                    justifyContent: "center",
                                                    borderRadius: 30
                                                }}>
                                                    <TouchableOpacity onPress={() => this.deleteImg(item.id)}>
                                                        <MIcon name="close" size={20} color="white" />
                                                    </TouchableOpacity>
                                                </View>
                                            </ImageBackground>
                                            }) : null
                                        }
                                    </View>
                                    <Text style={styles.text}>موضوع</Text>
                                    <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginBottom: 20, marginTop: 5, borderRadius: 7 }}>
                                        <TextInput
                                            maxLength={15}
                                            placeholder={'موضوع'}
                                            placeholderTextColor={'gray'}
                                            underlineColorAndroid='transparent'
                                            value={this.state.title}
                                            style={{
                                                height: 40,
                                                paddingRight: 15,
                                                width: '100%',
                                                color: 'gray',
                                                fontSize: 14,
                                            }}
                                            onChangeText={(text) => {this.setState({title: text})}}
                                        />
                                    </View>
                                    <Text style={styles.text}>وضعیت سفر: </Text>
                                    <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginTop: 10, marginBottom: 10, borderRadius: 7}}>
                                        {/*<Icon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>*/}
                                        <Selectbox
                                            style={{height: 42, paddingTop: '3%', paddingRight: 10}}
                                            selectLabelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingTop: 0}}
                                            optionLabelStyle={{textAlign: 'center', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                            selectedItem={this.state.travelStatus}
                                            cancelLabel="لغو"
                                            onChange={(itemValue) =>{
                                                this.setState({
                                                    travelStatus: itemValue
                                                }, () => {
                                                    this.setState({ travelStatusTitle: itemValue.label}, () => {console.log(this.state.travelStatusTitle)});
                                                })}}
                                            items={travelItems} />
                                    </View>
                                    <Text style={styles.text}>توضیحات</Text>
                                    <View style={{width: '100%', alignItems: 'center', justifyContent: 'center', marginTop: 10}}>
                                        <TextInput
                                            multiline = {true}
                                            numberOfLines = {6}
                                            value={this.state.content}
                                            onChangeText={(text) => this.setState({content: text})}
                                            placeholderTextColor={'rgb(142, 142, 142)'}
                                            underlineColorAndroid='transparent'
                                            placeholder="توضیحات..."
                                            style={{
                                                // height: 45,
                                                paddingRight: 15,
                                                width: '95%',
                                                fontSize: 14,
                                                color: 'rgb(142, 142, 142)',
                                                // textAlign:'right',
                                                backgroundColor: 'rgb(247, 247, 247)',
                                                borderWidth: 1,
                                                borderColor: 'rgb(236, 236, 236)',
                                                borderRadius: 10
                                            }}
                                        />
                                    </View>
                                </View>
                            </ScrollView>
                            {/*<Text style={styles.title}>یا</Text>*/}
                            {
                                this.state.loading ?
                                    <View style={{width: '100%', height: 120}}>
                                        <BallIndicator color='#21b34f' size={50} style={{height: 90}} />
                                    </View> :
                                    <TouchableOpacity onPress={() => this.addTravel()} style={styles.advertise}>
                                        <Text style={styles.buttonTitle}>تایید</Text>
                                    </TouchableOpacity>
                            }

                        </View>
                    </View>
                </Modal>
            </View>
        )
    }
}
export default NewTravelModal

const styles = StyleSheet.create ({
    container: {
        alignItems: 'center',
        // backgroundColor: 'white',
        padding: 10,
        position: 'absolute',
        bottom: -200
    },
    modal: {
        position: 'relative',
        zIndex: 0,
        flexGrow: 1,
        justifyContent: 'center',
        // flex: 1,
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,.6)',
        paddingRight: 20,
        paddingLeft: 20
    },
    box: {
        width: '100%',
        height: '70%',
        backgroundColor: 'white',
        // backgroundColor: 'rgb(218, 218, 218)',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        borderRadius: 15,
        paddingBottom: 20
    },
    picker: {
        height: 40,
        backgroundColor: 'white',
        width: '40%',
        color: 'black',
        borderColor: 'lightgray',
        borderWidth: 1,
        // elevation: 4,
        borderRadius: 10,
        // marginBottom: 20,
        // overflow: 'hidden'
    },
    input: {
        width: '100%',
        fontSize: 16,
        paddingTop: 0,
        textAlign: 'right',
    },
    searchButton: {
        width: '90%',
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgb(20, 122, 170)'
    },
    searchText:{
        color: 'white'
    },
    // picker: {
    //     height:50,
    //     width: "100%",
    //     alignSelf: 'flex-end'
    // },
    label: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'rgb(60, 60, 60)'
    },
    headerContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 8,
        borderBottomWidth: 1,
        borderBottomColor: 'rgb(237, 237, 237)',
        marginBottom: 20
    },
    headerText: {
        fontSize: 15,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black'
    },
    advertise: {
        width: '35%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30,
        backgroundColor: '#8dc63f',
        padding: 5,
        marginTop: 20
    },
    buttonTitle: {
        fontSize: 14,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    inputLabel: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        alignSelf: 'flex-end',
        paddingRight: '10%',
        paddingBottom: 5
    },
    insContainer: {
        width: '100%',
        flexDirection: 'row',
        flexWrap: 'wrap',
        // alignItems: 'center',
        // justifyContent: 'space-between',
        padding: 8
    },
    info: {
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingTop: 20,
        paddingBottom: 20,
        borderBottomColor: 'white',
        borderBottomWidth: 1
    },
    rowsContainer: {
        width: '70%'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    itemContainer: {
        width: '48%'
    },
    title: {
        color: 'rgb(60, 60, 60)',
        fontSize: 11,
        fontFamily: 'IRANSansMobile(FaNum)',
        alignSelf: 'flex-end',
        paddingRight: 10
    },
    imageContainer: {
        borderRadius: 15,
        padding: 7,
        backgroundColor: 'white',
        width: '20%',
        marginRight: 10,
        marginLeft: 10
    },
    text: {
        fontSize: 13,
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'right',
        color: 'black',
        alignSelf: 'flex-end'
    },
    Image2: {
        width: 80,
        height: 80,
        borderRadius: 6,
        marginLeft: 7,
        marginBottom: 13,
        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden'
    },
    photoContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: 20,
        paddingRight: 30,
        flexWrap: 'wrap',
        borderRadius: 10,
        margin: 10
    },
    addImage2: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 80,
        height: 80,
        backgroundColor: 'rgb(59, 140, 219)',
        borderRadius: 6,
        // marginBottom: 13
    },
    addText2: {
        color: 'white',
        fontSize: 10,
        paddingTop: 10,
        textAlign: 'center'
    },
})