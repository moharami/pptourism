/**
 * Created by n.moharami on 5/4/2019.
 */
/**
 * Created by n.moharami on 4/18/2019.
 */
import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, BackAndroid, TextInput, ActivityIndicator, AsyncStorage, BackHandler, Alert, Image} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import SIcon from 'react-native-vector-icons/dist/SimpleLineIcons';
import FIcon from 'react-native-vector-icons/dist/Feather';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://ppt.azarinpro.info';
Axios.defaults.baseURL = url;
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
import Loader from '../../components/loader'
import PlaceFooter from "../../components/placeFooter";
import SlideShow from "../../components/slideShow";
import weather from "../../assets/weather.png";
import moment_jalaali from 'moment-jalaali'
import HTML from 'react-native-render-html';
import InfoModal from './newTravelModal'
import img from '../../assets/usa.png'

class Travelogue extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: false,
            homeActive: true,
            modalVisible: false,
            modalTitle: '',
            weather: {}

        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    closeModal() {
        this.setState({modalVisible: false});
    }
    render() {
        const myHtml = '<p>' +'rfsdfsdkfljsdkfldsf'+'</p>';
        const photos = [

        ];
        if(this.state.loading){
            return (<Loader />)
        } else return (
            <View style={styles.container}>
                <View style={styles.top}>
                    <TouchableOpacity onPress={() => this.onBackPress()}>
                        <FIcon name="arrow-left" size={23} color="gray" />
                    </TouchableOpacity>
                </View>
                {/*<View>*/}
                {/*<SlideShow activeSlide={1} />*/}
                {/*</View>*/}
                <ScrollView style={styles.scroll}>
                    <View style={styles.body}>
                        <ScrollView style={{ transform: [
                            // { scaleX: -1},
                            // {  rotate: '180deg'},

                        ],}}
                                    horizontal={true} showsHorizontalScrollIndicator={false}
                        >
                            <View style={styles.navitemContainer}>
                                {
                                    this.props.travels.map((item, index)=> {
                                        return <TouchableOpacity onPress={() => Actions.travelDetail({item: item})} key={index}>
                                            <View style={styles.navContainer}>
                                                {/*<Image source={img} style={{ width: 50, height: 50, resizeMode: 'contain'}} />*/}
                                                {/*<Image source={item.attachments.length !== 0 ? item.attachments[0].uid : "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw0NDQ0NDQ0NDQ0NDQ0NDg0NDQ8NDQ0NFREWFhURExMYHSggGBolGxUWITEhJSk3Li4uFx8zODMtNygtLjcBCgoKBQUFDgUFDisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIALcBEwMBIgACEQEDEQH/xAAaAAEBAQEBAQEAAAAAAAAAAAAAAgEDBAUH/8QANBABAQACAAEIBwgCAwAAAAAAAAECEQMEEiExQWFxkQUTFDJRUqEiM2JygYKxwdHhQvDx/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/AP0QAAAAAAGgA0GNGgxo3QMNN0AzRpWjQJ0K0Akbo0CTTdAJGgMY0BgAAAAAAAAAAAAANCNAGgDdDQNGm6boGabpum6BOjStGgTo0rRoEaZpemWAnTNL0nQJFMBLKpNBgUAAAAAAAAAAAIEBsUyNBrWRUAbI2RsgMkVI2RsgM03TZG6BOjS9GgRo0vRoHPTNOmk2AixNjppNgIsYupoJTV1FBIUAAAAAAAAAAAIEBUUyKgCoyKgNipCRUAkVISKkBmm6duFwMsuqdHxvRHq4XIZ/yu+6f5B4Zjvqd+HyPO9f2Z39fk932OHOzH+a48Tlnyz9b/gFcPkeE6/tXv6vJw9IcLXNyk1Pdv8ASMuNnbLbei711R7uLj6zDo7ZueIPj6ZY6WJsBzsTY6WJsBzsTXSooJqK6VFBzo2sAAAAAAAAAAAIEBeKkxcBsVGRUBUXw8LeiS290duQcPDLKzOb6Nzp1H0888OFPhOySdYPFwuQZX3rMe7rr2cPkuGPZu/G9LzcTl1vuzXfemvbctY7vZN0HPiceTqxyyvdLrzefPjcS9lxndLvzd/asO/yb7Vh3+QPFzMvhl5U9Xl8t8q9vtOHf5HtOHf5A8Pq8vlvlXs5HbzdWWa6tzsV7Th3+R7Th3+QPJyng2Z3Utl6eiOF4WXy5eVfR9qw7/JXD4+OV1N76+oHyc8bOuWeM052Pf6S68fCvFQc6mrqKCKjJ0rnkCKxtYAAAAAAAAAAAQIC4uIxXAVFRMXAdODnzcplOy7/AEfX5Vhz+HddOpzo+NH1vR/E52Gu3Ho/TsB86Prcf7u/lfO4/D5udnZ1zwfR4/3d/KD52Memcly12b+DjwctZS3qlfSlmt9nxB86zXRetjpx8pcrZ1OYDHp5Lwt3nXqnV4ufG4VmWpN76YDjXbkXv/tv9OOU10V25F7/AO2/0B6S97Hwrw17vSXvY+FeGgipqqmgioyXUZA51jawAAAAAAAAAAAgQF4riIqAuKiIqAuPX6P4nNzk7Muj9ex44vGg+l6R4fRMvh0Xw/7/AC78f7u/lJZxeH+bHyv/AKco+7y/KD50VKiV6OTcHndN92fUHNfCw511590e7icLHKas8NdjODwphNdfeC8ZqajQB8/luOs9/GbOQ+/+2/078vx3jv5b9K8/IL9v9t/oG+kvex8K8Ne30n72Phf5eG0GVFVUUGVzyXUZAisbWAAAAAAAAAAAECAuKRFQFRURFQFxUrnKuUH0/RfE6MsP3T+3q5V93n4Pkcm4vMzxy7Jenw7X2crjZq2WXs3AfIxs6N9Xb2PZjy6SamGpO/8A09HqeF8uH0PVcL5cPoDj7f8Ah+p7f+H6u3quF8uH0PVcL5cPoDj7f+H6s9v/AAfX/Tv6rhfLh9D1XC+XD6A83E5bMsbOZ1zXX/pHo/7z9t/mPZ6nhfLh9G4YcPG7kxl+M0Dx+lPex8L/AC8Fr2+lbOdjq9l/l4LQZU1tTQZUVVTQTWNrAAAAAAAAAAACBAVGpigbFbQ0FytlRFbBcrZUSt2DpK3bntuwXs2jbdgrZanbNgrbLU7ZaDbWWs2zYNtTaMAqKpNBNCgAAAAAAAAAAAANjWANawBTdpaCtt2jbQXs2nZsF7No23YK2zbNs2Cts2zbNg3bKMA2wYDU1rKDKAAAAAAAAAAAAAA1gDRjQaMAUMAVs2wBu27SArbNsAbsYwGjAAYAAwAAAAAAAAAAAAAAAAAABu2ANAAawBoAAwBrAAAAYAAAAAAAAAAAP//Z"} style={{ width: 50, height: 50, resizeMode: 'contain'}} />*/}
                                                {
                                                    item.attachments.length !==0 ?
                                                        <Image source={{uri: "http://ppt.azarinpro.info/files?uid="+item.user.attachments.uid}}  style={{ width: 50, height: 50, borderRadius: 100, resizeMode: 'stretch'}} />
                                                        :
                                                        <Image source={{uri: "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxISEBQSEBIPEhMTFRAQFhMVDxASDxAQFREWFxUTExUYHSggGBolGxUTITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OFQ8PFS0ZFRkrKysrKysrKzc3Kys3LSsrLS0tNys3KysrLSsrKysrKysrKysrKysrKysrKysrKysrK//AABEIAOEA4QMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAABQYBAwQCB//EADUQAAIBAQUGAwcEAgMAAAAAAAABAgMEBREhMRJBUWFxkTKBoRMiQrHB0fBSYnLxgqIUkuH/xAAWAQEBAQAAAAAAAAAAAAAAAAAAAQL/xAAWEQEBAQAAAAAAAAAAAAAAAAAAARH/2gAMAwEAAhEDEQA/APuIAAAAAAAADI+1XpGOUfef+q8wJA5a1vpx1li+CzIS0Wyc/E8uCyRzmsTUtVvn9Me7OWpeVV/Fh0SOMFxNbZWmb1nP/szw5vi+7PIA9Kb4vuz3G0TWk5r/ACZqAHXC8aq+LHqkzqpXw/iin0ZFAYasNG8qct+D55HWmVM3ULTOHhk1y1XYmLqzgjLLeyeU1svivD/4SUZJrFZoyrIAAAAAAAAAAAAAAABptNpjBYyfRb30NVvtqprDWT0X1ZAVark8ZPF/mhZEtdFrt0qnKPBfXicgBpAAAAAAAAAAAAAAAAA6LLa5U3k8uD0ZzgCy2S2RqLLJ71vR0FUhNp4ptPiicu+8FP3ZZS9JdDNiyu8AEUAAAAAAAAOS8Laqayzk9F9WbbXaFCLk/JcWVurVcm5SzbLIlrE5NvF5tmDANIAAAAAANlGk5PCKxZLWe6Irxtt8FkhohQWanY6cdIR7YvuzckTVxVGYLaaallhLWEX5LHuNMVgE5XuiD8LcX3RE2izyg8JLo9z6F1MaQAAAAAyjAAnbst22tmXiX+y+5IFTjJp4rJrMsVgtaqR/ctV9TNiyuoAEUAAAw2ZI++LTsx2VrL0jvAjLwtXtJ/tWS+5ygG2QAAAAANlGk5yUY6vsubNZOXNZ8I7b1l6RFHVZLLGnHBa73vbN4BhoAAAAADXXoxmsJLFeq5o2ACsWqzunLZfk+KNJYrzs+3B8Y5r6orpqM0ABQAAA3WS0OElJdGuKNIAtdOakk1o8z0RNy2nWD6r6oljDQAADKzba+3Ny3aLoibvOts03xfurzK6aiUABUAAAAAAtFlXuR/jH5FXLVQ8Mei+RKsewAZUAAAAAAAAKpNYNrmy1lVq+J9X8yxK8AA0gAAAAA90ajjJSWqeJaKc00mtGsSqE7ctbGGz+l+jJViQABlUPftTOMer+xFHXek8asuWC7I5DcZAAAAAAAAC101hFLkvkQV02ZTm3LNRzw4vcT5mrAAEUAAAAAAAAKtaPHL+UvmWkhr5syTU1li8H14liVFgA0gAAAAAEhc1TCph+pYeaI822WezOL4NdhRaAAYaVe0yxnJ8ZS+ZqMyeZg2yAAAAAAAAlLin70lxSfbX6EyVqw1tipF7scH0ZZTNWAAIoAAAAAAAARl+T92K3t4+SX9EmV+9q21UaWkfd89/5yLEriABpAAAAAAAAFj/5QIP2v5gCYa0tZmDZaFhOS4SkvVmsoAAAAAAAAExcldvGLbeGDXJcCHOm76uzUi92OD6MUiyAAw0AAAAAAAA4L3ruMEotpyeGWuBAnde9baqYbo5ee84TUZoACgAAAAAAADf7HmCZ/wCI/wAwBNMRV6Qwqy54Puv7OQlL8p+9GXFYdiLLCgAAAAAAAAAAsd22jbprHVe6/udRXbutOxPPwvJ/csKZmrGQARQAADTa6+xBy4aLi9xuIK9rVtS2VpHs2WDgbxeL1eZgA0yAAAAAAAAG2zQxnFcWvmajuuenjUx/Sm/PQCfABhpyXpR2qb4r3uxXS2NFZtdHYm48NOa3GolaQAVAAAAAAAAAlrotjypy/wAXw5ESSlzWZuW29FkubJSJkAGWgAAR17W3ZWxHVrN8F9yDJm+rM3hNZ4ZPpuZDGozQAFAAAAAAAAAnLkpYQcv1P0RC04OTSWraRaKVNRiorRLAlWPYAMqEbfNnxiprWOv8SSMNY5MCpg6bdZvZzw3PNPkcxtkAN9msk5+FZcXkgNBlImaNzxXjblyWSO6lZ4x8MUvLMmriApWGpLSL88jrp3NL4pJdE39iZBNMcNC66cc3jJ89Ox2pGQRQAAAAAOKvdlOWecX+3D5HaAIedzP4Zp9Vh6nLVu6ovhx6PEsQLqYqcotapowWqpSjLxJPqjhr3TB+FuL7rsXTEGDqtNhnDNrFcVp58DlKgAbbPRc5KK3+i3sCQuWzYvbe7Jdd7Jg8UqajFRWiPZitAAAAADRbLMqkcHrqnwZW6kHFtPJrItZxXlYvaLFeJevJllSxG3bYdt4y8K9XwJ6MUlglgjXZqezCMVuS77zaKoACAAAAAAAAAAAAAAAAAAADIW9LBs+/DTeuHPoTRicU009Hl5AVRIsF22T2ccX4nry5Gq77v2XtSzeOS4Lj1JEtqSAAIoAAAAAAADzhhmu3EymZMNAZBhMyAAAAAAAAAAAAAAAAAAMOQGTzr0+YS4noAAAAAAAAAAAAAAAAA0ecGuf5/Z6AHlS8uW89GGsdRgBkHnF8DO117AZB5U1xXc9AADy5patdwPQPO1+YfUJvh3A9GHJfmphJ7/Qyo4AYz6fMylgZAAAAAAAAAAAAAAAAAAAAAAAAAAAAYOS1amAB4oeI7zIAAAAAAAAAAAAAAAAAAAD/2Q=="}}  style={{ width: 50, height: 50, borderRadius: 50, resizeMode: 'stretch'}} />
                                                }
                                                <Text style={styles.title1}>{item.user.fname} {item.user.lname}</Text>
                                                {/*<Text style={styles.subTitle}>عضو فعال</Text>*/}
                                                <View style={{flexDirection: 'row'}}>
                                                    <Text style={[styles.text, {color: 'black'}]}>{item.title}</Text>
                                                    <View style={styles.iconRightEditContainer}>
                                                        <FIcon name="check" size={14} color="white" />
                                                    </View>
                                                </View>
                                                <Text style={[styles.text, {color: 'black', textAlign: 'center', paddingBottom: 10, borderBottomWidth: 1, borderBottomColor: 'lightgray', paddingTop: 5}]}>{item.description}</Text>
                                                <Text style={[styles.text, {color: 'gray', textAlign: 'center', paddingTop: 5}]}>{item.content !== null ? item.content.substr(0, 155) +'...'  : 'توضیحاتی ثبت نشده است'}</Text>
                                                <View style={{flexDirection: 'row', paddingTop: 15}}>
                                                    <Text style={[styles.text, {color: 'gray', paddingRight: 5}]}>{moment_jalaali(item.created_at, 'YYYY-MM-DD').format('jYYYY/jMM/jDD')}</Text>
                                                    <Icon name="calendar" size={12} color="gray" />
                                                </View>
                                            </View>
                                        </TouchableOpacity>
                                    })
                                }
                            </View>
                        </ScrollView>
                        <View style={styles.contentBody}>
                            {/*<Text style={[styles.contentTxt, {width: '40%'}]}>آخرین بروزرسانی: { moment_jalaali(this.props.item.updated_at, 'YYYY-M-D').format('jYYYY/jM/jD')}</Text>*/}
                            <View style={styles.weather}>
                                {/*<View style={styles.weatherRow}>*/}
                                    {/*<TouchableOpacity onPress={() => Actions.places()} style={styles.button}>*/}
                                        {/*<Text style={[styles.title, {color: 'rgb(66, 183, 42)', fontSize: 12}]}>تمام مکان ها</Text>*/}
                                    {/*</TouchableOpacity>*/}
                                    {/*<TouchableOpacity onPress={() => Actions.events()} style={styles.button}>*/}
                                        {/*<Text style={[styles.title, {color: 'rgb(66, 183, 42)', fontSize: 12}]}>رویدادها</Text>*/}
                                    {/*</TouchableOpacity>*/}
                                {/*</View>*/}
                            </View>
                            <View style={styles.content}>
                                <Text style={styles.title}>{this.props.place}</Text>
                                <Text style={styles.placeTxt}>سفرنامه های {this.props.place}</Text>
                                <View style={styles.infoRow}>


                                    {/*<Text style={styles.contentTxt}>توسط </Text>*/}

                                    {/*<Text style={styles.contentTxt}>  1398/05/31</Text>*/}

                                    {/*{*/}
                                        {/*<Text style={styles.location}> کشور : {this.props.country} </Text>*/}

                                    {/*}*/}
                                    {
                                        <Text style={styles.location}> تعداد سفرنامه ها : {this.props.travels.length} </Text>
                                    }
                                </View>
                            </View>
                        </View>
                        <Text style={[styles.placeTxt, {lineHeight: 20, textAlign: 'left'}]}>شما نیز می توانید خاطرات و سفرنامه های خود را در برنامه ما ثبت کنید تا بازدیدکنندگان را در لحظات خود شریک کرده و تجربیات خوب و بد خود را با کاربران در میان بگذارید</Text>
                        {/*<TouchableOpacity onPress={() => this.setState({modalVisible: true})} style={styles.advertise}>*/}
                            {/*<Text style={styles.buttonTitle}>ثبت سفرنامه جدید</Text>*/}
                        {/*</TouchableOpacity>*/}
                        <TouchableOpacity onPress={() => this.setState({modalVisible: true})} style={styles.advertise}>
                            <Text style={styles.travelTxt}>ثبت سفرنامه جدید</Text>
                            <Icon name="map" size={20} color="white" />
                        </TouchableOpacity>
                        <InfoModal
                            subName={this.props.subName}
                            placeId={this.props.placeId}
                            title='ثبت سفرنامه جدید'
                            closeModal={(title) => this.closeModal(title)}
                            modalVisible={this.state.modalVisible}
                        />
                    </View>
                </ScrollView>
                <PlaceFooter active="travel" item={this.props.item} fav={this.props.fav} subName={this.props.subName} comments={this.props.comments} />
            </View>
        );
    }
}
export default Travelogue;
