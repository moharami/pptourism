/**
 * Created by n.moharami on 5/25/2019.
 */

import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, BackAndroid, TextInput, ActivityIndicator, AsyncStorage, BackHandler, Alert, Image} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import SIcon from 'react-native-vector-icons/dist/SimpleLineIcons';
import FIcon from 'react-native-vector-icons/dist/Feather';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://ppt.azarinpro.info';
Axios.defaults.baseURL = url;
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
import Loader from '../../components/loader'
import PlaceFooter from "../../components/placeFooter";
import SlideShow from "../../components/slideShow";
import weather from "../../assets/weather.png";
import moment_jalaali from 'moment-jalaali'
import HTML from 'react-native-render-html';

class TravelDetail extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: false,
            homeActive: true,
            modalVisible: false,
            modalTitle: '',
            weather: {},
            eventValidator: false,
            placeValidator: false,
            events: [],
            places: [],
            travels: [],
            item: []
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    render() {
        const myHtml = '<p>' + this.props.item.content + '</p>';
        const photos = [

        ];
        if(this.state.loading){
            return (<Loader />)
        } else return (
            <View style={styles.container}>
                <View style={styles.top}>
                    <TouchableOpacity onPress={() => this.onBackPress()}>
                        <FIcon name="arrow-left" size={23} color="gray" />
                    </TouchableOpacity>
                </View>
                {/*<View>*/}
                {/*<SlideShow activeSlide={1} />*/}
                {/*</View>*/}
                <ScrollView style={styles.scroll}>
                    <View style={styles.body}>
                        {/*<SlideShow activeSlide={1} images={this.props.item.attachments} item={this.props.item}/>*/}
                        <SlideShow activeSlide={1} images={this.props.item.attachments}/>
                        <View style={styles.contentBody}>
                            <View style={styles.weather}>
                                <Text style={[styles.contentTxt, {paddingTop: 20}]}>آخرین بروزرسانی: { moment_jalaali(this.props.item.updated_at, 'YYYY-MM-DD').format('jYYYY/jMM/jDD')}</Text>
                            </View>
                            <View style={styles.content}>
                                <Text style={styles.title}>{this.props.item.title}</Text>
                                <Text style={styles.placeTxt}>سفرنامه</Text>
                                <View style={styles.infoRow}>
                                    {
                                        this.props.item.user ?
                                            <Text style={styles.contentTxt}>توسط {this.props.item.user.fname } {this.props.item.user.lname }</Text>
                                            :
                                            <Text style={styles.contentTxt}>  {this.props.item.created_at !== null ? moment_jalaali(this.props.item.created_at, 'YYYY-MM-DD').format('jYYYY/jMM/jDD') : null}</Text>
                                    }
                                    {
                                        this.props.item.country || this.props.item.city ?
                                            <Text style={styles.location}>{this.props.item.country ? this.props.item.country.title : null}- { this.props.item.city ? this.props.item.city.title : null} . </Text>
                                            : (
                                            this.props.item.city_name || this.props.item.country_name ?
                                                <Text style={styles.location}>{this.props.item.country_name ? this.props.item.country_name : null}{this.props.item.city_name && this.props.item.country_name ? '-' : null} { this.props.item.city_name ? this.props.item.city_name : null} . </Text>
                                                : null
                                        )
                                    }
                                </View>
                            </View>
                        </View>
                        {/*<Text style={[styles.placeTxt, {lineHeight: 20}]}>{this.props.item.content}</Text>*/}
                        {
                            this.props.item.content !== null ?
                                <HTML html={myHtml} tagsStyles={{
                                    p: {
                                        fontFamily: 'IRANSansMobile(FaNum)',
                                        color: 'rgba(51, 54, 64, 1)',
                                        lineHeight: 30,
                                        paddingBottom: 10,
                                        paddingTop: 20,
                                        fontSize: 14,
                                        textAlign: 'right'
                                    },
                                    img: {
                                        width: 300,
                                        height: 200,
                                        marginRight: 'auto',
                                        marginLeft: 'auto'
                                    }
                                }} />
                                : <Text> </Text>
                        }
                    </View>
                </ScrollView>
            </View>
        );
    }
}
export default TravelDetail;
