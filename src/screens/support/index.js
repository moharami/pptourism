/**
 * Created by n.moharami on 4/18/2019.
 */

import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, BackAndroid, TextInput, ActivityIndicator, AsyncStorage, BackHandler, Alert, Image} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import FIcon from 'react-native-vector-icons/dist/Feather';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://ppt.azarinpro.info';
Axios.defaults.baseURL = url;
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
import Loader from '../../components/loader'
import PlaceFooter from "../../components/placeFooter";
import Selectbox from 'react-native-selectbox'

class Support extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: false,
            title: '',
            status:{key: 0, label: "انتخاب نشده", value: ''},
            statusTitle:null,
            content: ''

        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.home();
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    sendTicket() {
        if(this.state.status.value === -1 || this.state.title === '' || this.state.content === '') {
            Alert.alert('','لطفا تمام موارد را پر نمایید');
        }
        else {
            AsyncStorage.getItem('token').then((info) => {
                if(info !== null) {
                    const newInfo = JSON.parse(info);
                    console.log('token', newInfo.token)
                    Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;

                    this.setState({loading: true});
                    Axios.post('/ticketing/create',{
                        priority: this.state.status.value,
                        title: this.state.title,
                        content: this.state.content
                    }).then(response=> {
                        this.setState({loading: false});
                        Alert.alert('','تیکت شما با موفقیت افزوده شد');
                        console.log('travel addding  response', response.data);
                    })
                        .catch((response) => {
                            console.log('resssponse erroreie create ', response.response.data.message);
                            if(response.response.data.message === "Unauthenticated."){
                                Alert.alert('','لطفا ابتدا وارد شوید');
                                this.setState({loading: false});
                                Actions.loginPage({newPlace: true});
                            }
                            else{
                                Alert.alert('','خطایی رخ داده مجددا سعی نمایید');
                                this.setState({loading: false});
                            }
                        });

                }
                else {
                    console.log('it is not login')
                    Alert.alert('','لطفا ابتدا وارد شوید');
                    Actions.loginPage({support: true})
                }
            });
        }
    }
    render() {
        const Items = [
            {key: 0, label: "انتخاب نشده", value: -1},
            {key: 1, label: "اضطراری", value: 0},
            {key: 2, label: "مهم", value: 1},
            {key: 2, label: "عادی", value: 2}
        ]

        if(this.state.loading){
            return (<Loader />)
        } else return (
            <View style={styles.container}>
                <View style={styles.top}>
                    <TouchableOpacity onPress={() => this.onBackPress()}>
                        <FIcon name="arrow-left" size={23} color="gray" />
                    </TouchableOpacity>
                    <Text style={[styles.title, {paddingLeft: '30%'}]}>گفتگو با پشتیبانی</Text>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={{alignItems: 'center', justifyContent: 'center', paddingTop: 20}}>
                        <Text style={styles.text}>وضعیت سفر: </Text>
                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginTop: 10, marginBottom: 10, borderRadius: 7}}>
                            {/*<Icon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>*/}
                            <Selectbox
                                style={{height: 42, paddingTop: '3%', paddingRight: 10}}
                                selectLabelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingTop: 0}}
                                optionLabelStyle={{textAlign: 'center', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                selectedItem={this.state.status}
                                cancelLabel="لغو"
                                onChange={(itemValue) =>{
                                    this.setState({
                                        status: itemValue
                                    }, () => {
                                        this.setState({ statusTitle: itemValue.label}, () => {console.log(this.state.statusTitle)});
                                    })}}
                                items={Items} />
                        </View>
                        <Text style={styles.text}>موضوع:</Text>
                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginBottom: 20, marginTop: 5, borderRadius: 7 }}>
                            <TextInput
                                maxLength={15}
                                placeholder={'موضوع'}
                                placeholderTextColor={'gray'}
                                underlineColorAndroid='transparent'
                                value={this.state.title}
                                style={{
                                    height: 40,
                                    paddingRight: 15,
                                    width: '100%',
                                    color: 'gray',
                                    fontSize: 14,
                                }}
                                onChangeText={(text) => {this.setState({title: text})}}
                            />
                        </View>
                        <Text style={styles.text}>توضیحات:</Text>
                        <View style={{width: '100%', alignItems: 'center', justifyContent: 'center', marginTop: 10}}>
                            <TextInput
                                multiline = {true}
                                numberOfLines = {6}
                                value={this.state.content}
                                onChangeText={(text) => this.setState({content: text})}
                                placeholderTextColor={'rgb(142, 142, 142)'}
                                underlineColorAndroid='transparent'
                                placeholder="توضیحات..."
                                style={{
                                    // height: 45,
                                    paddingRight: 15,
                                    width: '95%',
                                    fontSize: 14,
                                    color: 'rgb(142, 142, 142)',
                                    // textAlign:'right',
                                    backgroundColor: 'rgb(237, 237, 237)',
                                    borderWidth: 1,
                                    borderColor: 'rgb(236, 236, 236)',
                                    borderRadius: 10
                                }}
                            />
                        </View>
                        <TouchableOpacity onPress={() => this.sendTicket()} style={styles.advertise}>
                            <Text style={styles.buttonTitle}>تایید</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
                {/*<PlaceFooter active="comment" item={this.props.item} subName={this.props.subName} fav={this.props.fav} comments={this.props.comments} />*/}
            </View>
        );
    }
}
export default Support;
