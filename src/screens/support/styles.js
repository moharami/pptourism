/**
 * Created by n.moharami on 5/25/2019.
 */

import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(244, 244, 244)'
    },
    scroll: {
        paddingTop: 0,
        paddingRight: 20,
        paddingLeft: 20,
        // position: 'absolute',
        // top: 190,
        // bottom: 60,
        width: '100%',
        paddingBottom: 50
    },
    body: {
        paddingTop: 10,
        width: '100%',
        paddingBottom: 100
    },
    navitemContainer: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-between',
        paddingTop: 45,
        paddingBottom: 30,
        // paddingRight: 20,
        height: 120,
        width: '100%',
    },
    navContainer: {
        alignItems: "center",
        justifyContent: 'center',
        width: 64,
        height: 56,
        // marginLeft: 20,
        backgroundColor: 'white'
    },
    top: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'flex-start',
        padding: 15,
        backgroundColor: 'white',
        width: '100%'
    },
    contentBody: {
        width: '100%',
        // backgroundColor:'red',
        flexDirection: 'row',
        marginBottom: 20,
        alignItems: "center",
        justifyContent: 'center',
    },
    content: {
        // backgroundColor: 'white',
        // paddingRight: 30,
        width: '60%',
        paddingTop: 15,

    },
    title: {
        fontSize: 13,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black'
    },
    placeTxt: {
        fontSize: 11,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'gray'
    },
    infoRow: {
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingTop: 10
    },
    location: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: '#42b72a'
    },
    contentTxt: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'gray'
    },
    circleContainer: {
        width: 40,
        height: 40,
        borderRadius: 40,
        backgroundColor: '#42b72a',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'flex-end',
        marginBottom: 20
    },
    circleTxt: {
        fontSize: 14,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'white'
    },
    text: {
        fontSize: 13,
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'right',
        color: 'black',
        alignSelf: 'flex-end'
    },
    advertise: {
        width: '35%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 20,
        backgroundColor: '#8dc63f',
        padding: 5,
        marginTop: 20
    },
    buttonTitle: {
        fontSize: 14,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
});
