import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, BackAndroid, TextInput, ActivityIndicator, AsyncStorage, BackHandler, Alert, Picker} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import FIcon from 'react-native-vector-icons/dist/Feather';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://ppt.azarinpro.info';
Axios.defaults.baseURL = url;
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
import Loader from '../../components/loader'
import FooterMenu from "../../components/footerMenu/index";
import AppHeader from "../../components/appHeader";
import homeIcon from "../../assets/homeIcon.png";
import SearchPlace from "../../components/searchPlace";
import FilterModal from './filterModal'

class Search extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: false,
            loadingSearchAll: false,
            loadingSearchPlace: false,
            homeActive: true,
            what: '',
            where: '',
            active: '',
            modalVisible: false,
            modalVisible2: false,
            searchAll: {},
            searchPlace: {},
            business_cat_id: null,
            searchResult: [],
            placeId: null,
            placeType: '',
            count: 0,
            filter: null,
            filterType: null

        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);

    }
    filterLabel(item) {
        switch (item) {
            case 'businessCategory':
                return 'دسته';
                break;
            case 'business':
                return 'مکان';
                break;
            case 'travelogue':
                return 'سفرنامه';
                break;
            case 'country':
                return 'کشور';
                break;
            case 'city':
                return 'شهر';
                break;
            case 'state':
                return 'استان';
                break;
            case 'village':
                return 'روستا';
                break;
            case 'event':
                return 'رویداد';
                break;
            default:
                break;
        }
    }
    searchAll(text) {

        this.setState({loadingSearchAll: true });
        Axios.post('/business/search-all', {q: text}).then(response => {


            const res = response.data.data;
            const filtered = Object.keys(response.data.data);
            const allLabeles = filtered.filter((item)=> {return res[item].length !== 0 && item !== 'content'});

            let allData = {};
            allLabeles.map((item) => {
                allData[item] = res[item];
            })

            this.setState({loadingSearchAll: false, searchAll: allData });
        })
            .catch((error) => {
                console.log(error);
                this.setState({loadingSearchAll: false});

            });
    }
    searchPlace(text) {
        this.setState({loadingSearchPlace: true });
        Axios.post('/business/search-place', {q: text}).then(response => {

            const res = response.data.data;
            const filtered = Object.keys(response.data.data);
            const allLabeles = filtered.filter((item)=> {return res[item].length !== 0 && item !== 'content'});

            let allData = {};
            allLabeles.map((item) => {
                allData[item] = res[item];
            })

            this.setState({loadingSearchPlace: false, searchPlace: allData });
        })
            .catch((error) => {
                console.log(error);
                this.setState({loadingSearchPlace: false});

            });
    }
    startSearch(value, type) {
        if((this.state.what === '' && this.state.where === '') || (this.state.what === '' && this.state.where !== '') ) {
            Alert.alert('','عبارت مورد جستجو نمی تواند خالی باشد')
        }
        else {
            this.setState({loading: true, count: 0 , filterType: type === 'tag' ? type : null, filter: type === 'tag' ? value : null });
            let allData = {};
            if(this.state.business_cat_id !== null) {
                allData['business_cat_id'] = this.state.business_cat_id;
            }
            else {
                allData['q'] = this.state.what
            }
            if(this.state.where !== '') {
                switch (this.state.placeType) {
                    case 'country':
                        allData['country_id'] = this.state.placeId;
                        break;
                    case 'city':
                        allData['city_id'] = this.state.placeId;
                        break;
                    case 'state':
                        allData['state_id'] = this.state.placeId;
                        break;
                    case 'village':
                        allData['village_id'] = this.state.placeId;
                        break;
                }
            }
            if(type === 'tag') {
                allData['tag'] = value;
            }
            Axios.post('/business/search-result', allData).then(response => {

                const res = response.data.data;
                const filtered = Object.keys(response.data.data);
                const allLabeles = filtered.filter((item)=> {return res[item].length !== 0 && item !== 'content'});

                let allData = {};
                allLabeles.map((item) => {
                    allData[item] = res[item];
                })
                let count = this.state.count;
                if(!Object.keys(allLabeles).length) {
                    console.log('empty')
                   this.setState({count: 0})
                }
                else {
                    Object.keys(allData).map((item) => {
                        allData[item].map(() => {
                            ++count;
                        })
                    })
                }
                this.setState({loading: false, searchResult: allData, count: count});
            })
                .catch((error) => {
                    console.log(error);
                    this.setState({loading: false});

                });
        }
    }
    closeModal() {
        this.setState({modalVisible2: false});
    }
    filterChange(value, type) {
        if(type === 'tag') {
            this.setState({filter: value, filterType: type}, () => this.startSearch(value, type))

        }
        else{
            this.setState({filter: value, filterType: type})
        }

    }
    render() {
        console.log('this.sate.filter', this.state.filter)
        console.log('this.state.serach result', this.state.searchResult)

        if(this.state.loading){
            return (<Loader />)
        } else return (
            <View style={styles.container}>
                <View style={styles.top}>
                    <TextInput
                        placeholder={'جستجوی رستوران، هتل و ...'}
                        placeholderTextColor={'gray'}
                        underlineColorAndroid='transparent'
                        value={this.state.what}
                        style={{
                            height: 40,
                            width: '90%',
                            color: 'gray',
                            fontSize: 14,
                            textAlign: 'center',
                            borderBottomColor: 'gray',
                            borderBottomWidth: 1
                        }}
                        onChangeText={(text) =>  this.setState({what: text, modalVisible: text !== '', active: text === '' ? '' :  'what'}, () => {this.searchAll(text)})}
                    />
                    <View style={styles.locationContainer}>
                        <TouchableOpacity onPress={() => this.startSearch()} style={{position: 'absolute', zIndex: 9992, top: 20, left: '8%'}}>
                            <FIcon name="search" size={20} color="gray"  />
                        </TouchableOpacity>
                        <TextInput
                            placeholder={'ایران - تهران'}
                            placeholderTextColor={'gray'}
                            underlineColorAndroid='transparent'
                            value={this.state.where}
                            style={{
                                height: 40,
                                width: '83%',
                                color: 'gray',
                                fontSize: 14,
                                textAlign: 'left',
                                paddingRight: 15,
                                paddingLeft: 30,
                                borderBottomColor: 'gray',
                                borderBottomWidth: 1
                            }}
                            onChangeText={(text) => this.setState({where: text, modalVisible: text !== '', active: text === '' ? '' : 'where'}, () => {this.searchPlace(text)})}
                        />
                        <Text style={styles.topTxt}>در</Text>
                    </View>
                </View>
                {
                    this.state.active !== '' && this.state.modalVisible === true ?
                        <TouchableOpacity onPress={() => this.setState({modalVisible: false})} style={[styles.whatContainer, { top: this.state.active === 'what' ? 55 : 100}]}>
                            <View style={styles.whatContainerInner}>
                                <ScrollView style={styles.topScroll}>
                                    {
                                        this.state.active === 'what' ? (
                                            this.state.loadingSearchAll ?
                                                <Text style={styles.loadingText}>در حال بارگذاری ... </Text>
                                                :
                                                !Object.keys(this.state.searchAll).length ?
                                                    <Text style={styles.loadingText}> نتیجه ای یافت نشد !</Text>
                                                    :
                                                    Object.keys(this.state.searchAll).map((item, index) => {
                                                        return (
                                                            <View style={{alignItems: 'flex-end', justifyContent: 'flex-end'}} key={index}>
                                                                <TouchableOpacity onPress={() => this.setState({modalVisible: false})}>
                                                                    <Text style={styles.whatText}>{this.filterLabel(item)}</Text>
                                                                </TouchableOpacity>
                                                                {
                                                                    this.state.searchAll[item].map((item2, index) => {
                                                                        return <TouchableOpacity key={index} onPress={() => this.setState({modalVisible: false}, () => {

                                                                            if(item !== 'businessCategory') {
                                                                                Actions.placeDetail({item: item2})
                                                                            }
                                                                            else {
                                                                                this.setState({business_cat_id: item2.id, what: item2.title})
                                                                            }
                                                                        })}>
                                                                            <Text style={styles.whatSubText}>{item2.title}</Text>
                                                                        </TouchableOpacity>
                                                                    })
                                                                }
                                                            </View>
                                                        )}

                                                    )
                                        ) : (
                                            this.state.loadingSearchPlace ?
                                                <Text style={styles.loadingText}>در حال بارگذاری ... </Text>
                                                :
                                                !Object.keys(this.state.searchPlace).length ?
                                                    <Text style={styles.loadingText}> نتیجه ای یافت نشد !</Text>
                                                    :
                                                    Object.keys(this.state.searchPlace).map((item, index) => {
                                                        return (
                                                            <View style={{alignItems: 'flex-end', justifyContent: 'flex-end'}} key={index}>
                                                                <TouchableOpacity onPress={() => this.setState({modalVisible: false})}>
                                                                    <Text style={styles.whatText}>{this.filterLabel(item)}</Text>
                                                                </TouchableOpacity>
                                                                {
                                                                    this.state.searchPlace[item].map((item2, index) => {
                                                                        return <TouchableOpacity key={index} onPress={() => this.setState({modalVisible: false, placeId: item2.id, placeType: item, where: item2.title })}>
                                                                            <Text style={styles.whatSubText}>{item2.title}</Text>
                                                                        </TouchableOpacity>
                                                                    })
                                                                }
                                                            </View>
                                                        )}

                                                    )
                                        )
                                    }
                                </ScrollView>
                            </View>
                        </TouchableOpacity>
                        : null
                }

                <View style={styles.resultContainer}>
                    <TouchableOpacity onPress={() => this.setState({modalVisible2: true})} style={styles.filterRow}>
                        <Icon name="filter" size={18} color="black" />
                        <Text style={styles.result}>فیلتر</Text>
                    </TouchableOpacity>
                    <View style={styles.filterRow}>
                        <Text style={styles.count}>({this.state.count} مورد)</Text>
                        <Text style={styles.result}>نتایج یافت شده</Text>
                    </View>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.body}>

                        {
                            this.state.filter !== null ?(
                                this.state.filterType === 'what' ?
                                    !Object.keys(this.state.searchResult).length ?
                                        <Text style={styles.loadingText}> نتیجه ای یافت نشد !</Text>
                                        :
                                        <View style={{width: '100%'}} >
                                            {
                                                this.state.searchResult[this.state.filter] ?
                                                    this.state.searchResult[this.state.filter].map((item2, index) => {
                                                        return <SearchPlace name={this.filterLabel(this.state.filter)} item={item2} key={index} />
                                                    }) :
                                                    <Text style={styles.loadingText}> نتیجه ای یافت نشد !</Text>
                                            }
                                        </View>
                                    :
                                    this.state.filterType === 'tag' ?
                                        !Object.keys(this.state.searchResult).length ?
                                            <Text style={styles.loadingText}> نتیجه ای یافت نشد !</Text>
                                            :
                                            Object.keys(this.state.searchResult).map((item, index) => {
                                                return (
                                                    <View style={{width: '100%'}} key={index}>
                                                        {
                                                            this.state.searchResult[item].map((item2, index) => {
                                                                return  <SearchPlace name={this.filterLabel(item)} item={item2} key={index} />
                                                            })
                                                        }
                                                    </View>
                                                )}

                                            )
                                        :
                                        <Text style={styles.loadingText}> نتیجه ای یافت نشد !</Text>
                            )
                                :
                            !Object.keys(this.state.searchResult).length ?
                                <Text style={styles.loadingText}> نتیجه ای یافت نشد !</Text>
                                :
                                Object.keys(this.state.searchResult).map((item, index) => {
                                    return (
                                        <View style={{width: '100%'}} key={index}>
                                            {
                                                this.state.searchResult[item].map((item2, index) => {
                                                    return <SearchPlace name={this.filterLabel(item)} item={item2} key={index} />
                                                })
                                            }
                                        </View>
                                    )}

                                )
                        }
                        <FilterModal
                            filterChange={(value, type) => this.filterChange(value, type)}
                            closeModal={(title) => this.closeModal(title)}
                            modalVisible={this.state.modalVisible2}
                        />
                    </View>
                </ScrollView>
                <FooterMenu active="search" />
            </View>
        );
    }
}
export default Search;
