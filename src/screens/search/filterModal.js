
import React, { Component } from 'react';
import {
    Modal,
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Picker,
    Alert
}
    from 'react-native'
import Icon from 'react-native-vector-icons/dist/FontAwesome'
import FIcon from 'react-native-vector-icons/dist/Feather'
import Axios from 'axios';
export const url = 'http://ppt.azarinpro.info';
Axios.defaults.baseURL = url;
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
import Loader from '../../components/loader'


class FilterModal extends Component {
    state = {
        text: '',
        modalVisible: false,
        loading: false,
        what: '',
        tags: [],
        tag: 0,
        active: ''
    };

    componentWillMount() {
        Axios.get('/business/tag/show').then(response=> {
            let newTags = [];
            newTags.push({title: 'تگ', id: 0});
            response.data.data.map((item)=> newTags.push(item))
            this.setState({loading: false, tags: newTags});
            console.log('newtags', newTags)
        })
            .catch((error) => {
                Alert.alert('مشکلی در برقراری ارتباط با سرور رخ داده لطفا مجددا تلاش نمایید')
                this.setState({loading: false});
            });
    }
    closeModal() {
        this.setState({modalVisible: false});
    }
    filterMethode() {
        if(this.state.active === 'what') {
            this.props.filterChange(this.state.what, 'what');
            this.props.closeModal();
        }
        else if(this.state.active === 'tag') {
            this.props.filterChange(this.state.tag, 'tag');
            this.props.closeModal();
        }
        else {
            this.props.closeModal();
        }
    }
    render() {
        if(this.state.loading){
            return <Loader />
        }
        else return (
            <View style={styles.container}>
                <Modal animationType = {"fade"} transparent = {true}
                       visible = {this.props.modalVisible}
                       onRequestClose = {() => this.props.onChange(false)}
                       onBackdropPress= {() => this.props.onChange(!this.state.modalVisible)}
                >
                    <View style = {styles.modal}>
                        <View style={styles.box}>
                            <View style={styles.headerContainer}>
                                <TouchableOpacity onPress={() => this.props.closeModal()}>
                                    <Icon name="close" size={20} color={ "black"} />
                                </TouchableOpacity>
                                <Text style={styles.headerText}>فیلتر</Text>
                            </View>
                            <Text style={styles.title}>بر اساس {this.props.insTitle}</Text>
                            <View style={{position: 'relative', zIndex: 3, width: '90%', borderBottomColor: 'gray', borderBottomWidth: 1, paddingBottom: 5, marginBottom: 30, alignItems: 'flex-end', justifyContent: 'flex-end'}}>
                                <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 20}}/>
                                <Picker
                                    itemStyle={{textAlign: 'right'}}
                                    itemTextStyle={{textAlign: 'right', alignSelf: 'flex-end'}}
                                    mode="dropdown"
                                    style={styles.picker}
                                    selectedValue={this.state.what}
                                    onValueChange={(itemValue) => {this.setState({what: itemValue, active: 'what'})}}>

                                    <Picker.Item  label="همه" value="all" />
                                    <Picker.Item  label="مکان" value="business" />
                                    <Picker.Item  label="سفرنامه" value="travelogue" />
                                    <Picker.Item  label="رویداد" value="event" />
                                    <Picker.Item  label="کشور" value="country" />
                                    <Picker.Item  label="استان" value="state" />
                                    <Picker.Item  label="شهر" value="city" />
                                    <Picker.Item  label="روستا" value="village" />
                                </Picker>
                            </View>
                            <Text style={styles.title}>یا</Text>
                            <View style={{position: 'relative', zIndex: 3, width: '90%', borderBottomColor: 'gray', borderBottomWidth: 1, paddingBottom: 5,  marginBottom: 30, paddingLeft: 35,  alignItems: 'flex-end', justifyContent: 'flex-end'}}>
                                <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 20}}/>
                                <Picker
                                    mode="dropdown"
                                    style={styles.picker}
                                    selectedValue={this.state.tag}
                                    onValueChange={(itemValue) => {this.setState({tag: itemValue, active: 'tag'})}}>
                                    {
                                        this.state.loading ?
                                            <Picker.Item label={ 'در حال بارگذاری'} value={0}/>
                                            :
                                            this.state.tags.map((item, index)=> <Picker.Item label={item.title.toString()} value={item.id} key={index} />)
                                    }
                                </Picker>
                            </View>
                            <TouchableOpacity onPress={() => this.filterMethode()} style={styles.advertise}>
                                <Text style={styles.buttonTitle}>تایید</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Modal>
            </View>
        )
    }
}
export default FilterModal

const styles = StyleSheet.create ({
    container: {
        alignItems: 'center',
        // backgroundColor: 'white',
        padding: 10,
        position: 'absolute',
        bottom: -200
    },
    modal: {
        position: 'relative',
        zIndex: 0,
        flexGrow: 1,
        justifyContent: 'center',
        // flex: 1,
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,.6)',
        paddingRight: 20,
        paddingLeft: 20
    },
    text: {
        color: '#3f2949',
        marginTop: 10
    },
    box: {
        width: '100%',
        backgroundColor: 'white',
        // backgroundColor: 'rgb(218, 218, 218)',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        borderRadius: 15,
        paddingBottom: 20
    },
    picker: {
        height: 40,
        backgroundColor: 'white',
        width: '40%',
        color: 'black',
        borderColor: 'lightgray',
        borderWidth: 1,
        // elevation: 4,
        borderRadius: 10,
        // marginBottom: 20,
        // overflow: 'hidden'
    },
    input: {
        width: '100%',
        fontSize: 16,
        paddingTop: 0,
        textAlign: 'right',
    },
    searchButton: {
        width: '90%',
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgb(20, 122, 170)'
    },
    searchText:{
        color: 'white'
    },
    // picker: {
    //     height:50,
    //     width: "100%",
    //     alignSelf: 'flex-end'
    // },
    label: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'rgb(60, 60, 60)'
    },
    headerContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 8,
        borderBottomWidth: 1,
        borderBottomColor: 'rgb(237, 237, 237)',
        marginBottom: 20
    },
    headerText: {
        fontSize: 15,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black'
    },
    advertise: {
        width: '35%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30,
        backgroundColor: '#8dc63f',
        padding: 5,
        // marginTop: 0
    },
    buttonTitle: {
        fontSize: 14,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    inputLabel: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        alignSelf: 'flex-end',
        paddingRight: '10%',
        paddingBottom: 5
    },
    insContainer: {
        width: '100%',
        flexDirection: 'row',
        flexWrap: 'wrap',
        // alignItems: 'center',
        // justifyContent: 'space-between',
        padding: 8
    },
    info: {
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingTop: 20,
        paddingBottom: 20,
        borderBottomColor: 'white',
        borderBottomWidth: 1
    },
    rowsContainer: {
        width: '70%'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    itemContainer: {
        width: '48%'
    },
    title: {
        color: 'rgb(60, 60, 60)',
        fontSize: 11,
        fontFamily: 'IRANSansMobile(FaNum)',
        alignSelf: 'flex-end',
        paddingRight: 10
    },
    imageContainer: {
        borderRadius: 15,
        padding: 7,
        backgroundColor: 'white',
        width: '20%',
        marginRight: 10,
        marginLeft: 10
    },
})