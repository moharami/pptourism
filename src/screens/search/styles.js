
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(234, 234, 234)',

    },
    scroll: {
        paddingTop: 0,
        paddingRight: 20,
        paddingLeft: 20,
        // marginBottom: 90,
        // backgroundColor: 'red',
        position: 'absolute',
        top: 190,
        bottom: 60,
        width: '100%',
        zIndex: 10
    },
    body: {
        // paddingBottom: 150,
        paddingTop: 10,
        // alignItems: 'flex-start',
        // justifyContent: 'center',
        // backgroundColor: 'green',
        width: '100%'
    },
    picker: {
        height: 30,
        backgroundColor: 'white',
        width: '100%',
        color: 'gray'
    },
    navitemContainer: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-between',
        transform: [
            {rotateY: '180deg'},
        ],
        paddingTop: 45,
        paddingBottom: 30,
        paddingRight: 20,
        height: 120
    },
    navContainer: {
        alignItems: "center",
        justifyContent: 'center',
        width: 94,
        height: 96,
        paddingBottom: 40,
        marginLeft: 20,
        backgroundColor: 'white'

    },
    text: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        // paddingBottom: 55,
        position: 'absolute',
        color: 'black',
        bottom: 15

    },
    top: {
        alignItems: "center",
        justifyContent: 'center',
        padding: 15,
        backgroundColor: 'white',
        width: '100%'
    },
    headerTitleContainer: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'center',
    },
    headerTitle: {
        color: 'black',
        fontSize: 16,
        fontFamily: 'IRANSansMobile(FaNum)',
        // paddingRight: '34%'
    },
    name: {
        color: 'gray',
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    topTxt: {
        color: 'black',
        fontSize: 13,
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingLeft: 10
    },
    locationContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'center',
        paddingTop: 10,
        paddingBottom: 10,
        position: 'relative',
        zIndex: 2,
    },
    result: {
        color: 'rgb(50, 50, 50)',
        fontSize: 13,
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingLeft: 10
    },
    count: {
        color: 'gray',
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingLeft: 10
    },
    filterRow: {
        flexDirection: 'row'
    },
    resultContainer: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-between',
        padding: 20
    },
    topScroll: {
        paddingTop: 0,
        paddingRight: 20,
        paddingLeft: 20,
        // position: 'absolute',
        // zIndex: 9999,
        // top: 100,
        // bottom: 60,
        width: '100%',
        height: 150,
        // backgroundColor: 'red'
    },
    whatContainer: {
        backgroundColor: 'transparent',
        alignItems: 'center',
        justifyContent: 'flex-start',
        borderRadius: 10,
        // padding: 10,
        position: 'absolute',
        zIndex: 9999,
        width: '100%',
        // left: '5%',
        height: '100%'
    },
    whatContainerInner: {
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: 'gray',
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        borderRadius: 10,
        elevation: 5,
        // padding: 10,
        // position: 'absolute',
        // zIndex: 9999,
        // top: 55,
        width: '90%',
        // left: '5%'
    },
    whatText: {
        color: 'black',
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'right'
        // paddingLeft: 10
    },
    whatSubText: {
        color: 'gray',
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'right'

    },
    loadingText: {
        color: 'black',
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        paddingTop: 50
    }
});
