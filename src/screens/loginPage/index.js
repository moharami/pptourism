/**
 * Created by n.moharami on 4/23/2019.
 */
import React, {Component} from 'react';
import { Text, View, BackHandler, TextInput, AsyncStorage,TouchableOpacity, Image, Alert} from 'react-native';
import styles from './styles'
import Loader from '../../components/loader'
import {Actions} from 'react-native-router-flux'
import bg from '../../assets/bg4.png'
import logo from '../../assets/logo.png'
import google from '../../assets/google.jpg'
import usa from '../../assets/usa.png'
import Axios from 'axios';
export const url = 'http://ppt.azarinpro.info';
Axios.defaults.baseURL = url;
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
import {store} from '../../config/store';

class LoginPage extends Component {
    constructor(props){
        super(props);
        this.state = {
            email: '',
            password: '',
            login: true,
            loading: false,
            loginModal: false,
            signupModal: false,
            logout: false
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    onBackPress(){
        // this.logout();
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        // AsyncStorage.getItem('token').then((info) => {
        //     if(info !== null) {
        //         const newInfo = JSON.parse(info);
        //         Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;
        //         Axios.get('/core/user').then(response=> {
        //             store.dispatch({type: 'USER_INFO_FETCHED', payload: response.data.data});
        //             this.setState({loading: false});
        //             if(this.props.profile){
        //                 Actions.profile({user: response.data.data});
        //             }
        //             else if(this.props.newPlace) {
        //                 Actions.newPlace();
        //             }
        //             else if(this.props.support) {
        //                 Actions.support();
        //             }
        //             else if(this.props.addedPlace) {
        //                 Actions.addedPlace();
        //             }
        //         })
        //             .catch((error) => {
        //                 Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
        //                 this.setState({loading: false});
        //             });
        //     }
        //     else {
        //         this.setState({loading: false});
        //     }
        // })

    }
    logout() {
        Alert.alert(
            'خروج از حساب کاربری',
            'آیا از  خروج خود مطمئن هستید؟',
            [
                {text: 'خیر', onPress: () => this.setState({logout: false}), style: 'cancel'},
                {text: 'بله', onPress: () =>  this.setState({logout: true}, () => {BackHandler.exitApp()})},
            ]
        );
    }
    componentWillUpdate() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    login() {
        if(this.state.email === '' || this.state.password === ''){
            Alert.alert('', 'نام کاربری یا کلمه عبور نمی تواند خالی باشد')
        }
        else {
            this.setState({loading: true});
            Axios.post('/signin', {
                email: this.state.email,
                password: this.state.password,
            }).then(response=> {
                if(response.data === 'not exist') {
                    Alert.alert('','چنین کاربری وجود ندارد لطفا ابتدا ثبت نام کنید');
                    this.setState({loading: false});
                }
                else {
                    console.log('info', response.data);
                    Alert.alert('','شما با موفقیت وارد شدید');
                    const info = {'token': response.data, 'email': this.props.email, 'password': this.state.password};
                    const newwwwAsync = AsyncStorage.setItem('token', JSON.stringify(info)).then((info) => {

                        Axios.defaults.headers.common['Authorization'] = 'Bearer ' + response.data;
                        Axios.get('/core/user').then(response=> {
                            store.dispatch({type: 'USER_INFO_FETCHED', payload: response.data.data});
                            this.setState({loading: false});

                            if(this.props.travel) {
                                Actions.travelogue();
                            }
                            else if(this.props.profile){
                                Actions.profile({user: response.data.data});
                            }
                            else if(this.props.newPlace) {
                                Actions.newPlace();
                            }
                            else if(this.props.support) {
                                Actions.support();
                            }
                            else if(this.props.addedPlace) {
                                Actions.addedPlace();
                            }
                        })
                            .catch((error) => {
                                Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                                this.setState({loading: false});
                            });
                    });
                    console.log('newwwwAsync', newwwwAsync);


                }
            })
                .catch((error) => {
                console.log(error.response)
                    if(error.response.data === 'wrong_password') {
                        Alert.alert('','لطفا رمز عبور را صحیح وارد نمایید');
                        this.setState({loading: false});
                    }
                    else {
                        Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                        this.setState({loading: false});
                    }
                });
        }
    }
    render() {
        if(this.state.loading){
            return (<Loader />)
        } else return (
            <View style={styles.container}>
                <Image source={bg} style={{width: '100%', height: '70%'}} />
                <Image source={logo} style={{width: '30%', resizeMode: 'contain', position: 'absolute',
                    zIndex: 10,
                    top: -30,
                    left: '35%',
                }} />
                <Text style={styles.title}>پرتال فارسی توریسم</Text>
                <Text style={styles.subTitle}>Persian Portal Tourism</Text>
                <Text style={{
                    fontFamily: 'IRANSansMobile(FaNum)',
                    fontSize: 12,
                    textAlign: 'center',
                    color: 'gray',
                    position: 'absolute', bottom: 20, left: '27%'
                }}>طراحی و توسعه توسط آذرین وب</Text>
                <View style={styles.loginContainerTop}>
                    <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '80%', backgroundColor: 'white', height: 42, marginTop: 5, marginBottom: 10, borderRadius: 7}}>
                        <TextInput
                            keyboardType={'email-address'}
                            placeholder={'ایمیل'}
                            placeholderTextColor={'gray'}
                            underlineColorAndroid='transparent'
                            value={this.state.email}
                            style={{
                                height: 40,
                                paddingRight: 15,
                                paddingLeft: 15,
                                width: '100%',
                                color: 'gray',
                                fontSize: 14
                            }}
                            onChangeText={(text) => {this.setState({email: text})}}
                        />
                    </View>
                    <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '80%', backgroundColor: 'white', height: 42, marginTop: 5, marginBottom: 10, borderRadius: 7}}>
                        <TextInput
                            placeholder={'کلمه عبور'}
                            secureTextEntry
                            placeholderTextColor={'gray'}
                            underlineColorAndroid='transparent'
                            value={this.state.password}
                            style={{
                                height: 40,
                                paddingRight: 15,
                                paddingLeft: 15,
                                width: '100%',
                                color: 'gray',
                                fontSize: 14,
                                textAlign: 'right'
                            }}
                            onChangeText={(text) => {this.setState({password: text})}}
                        />
                    </View>
                    <TouchableOpacity onPress={() => this.login()} style={styles.advertise}>
                        <Text style={styles.buttonTitle}>تایید</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.body}>
                    <View style={styles.loginContainer}>
                        <TouchableOpacity onPress={() => Actions.signup()} style={styles.button}>
                            <Text style={styles.label}>عضویت در پرتال</Text>
                        </TouchableOpacity>
                    </View>

                </View>
            </View>
        );
    }
}
export default LoginPage;