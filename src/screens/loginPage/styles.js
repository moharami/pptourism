/**
 * Created by n.moharami on 5/1/2019.
 */

import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1
    },
    title: {
        paddingRight: 10,
        fontSize: 16,
        color: 'white',
        textAlign: 'center',
        // paddingTop: 10,
        position: 'absolute',
        zIndex: 2,
        top: '25%',
        left: '34%'
    },
    subTitle: {
        paddingRight: 10,
        fontSize: 12,
        color: 'white',
        textAlign: 'center',
        position: 'absolute',
        zIndex: 200,
        top: '28.5%',
        left: '34%'
    },
    loginContainerTop: {
        width: '100%',
        position: 'absolute',
        zIndex: 220,
        top: '35%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    body: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 20
    },
    loginContainer: {
        width: '50%',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        paddingTop: 10

    },
    button: {
        alignItems: "center",
        justifyContent: 'center',
        borderRadius: 3,
        // borderColor: 'rgb(66, 183, 42)',
        // borderWidth: 1,
        width: '100%',
        padding: 12,
        backgroundColor: 'rgb(83, 92, 147)'
    },
    label: {
        fontSize: 13,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'white'
    },
    changeLanguage: {
        // width: '%',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        backgroundColor: 'rgba(255, 255, 255, .5)',
        padding: 5,
        borderRadius: 10,
        position: 'absolute',
        zIndex: 200,
        top: '35%',
        left: '41%',
        paddingRight: 3,
        paddingLeft: 3,
    },
    advertise: {
        width: '30%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 7,
        backgroundColor: 'rgb(44, 124, 61)',
        padding: 7,
        marginTop: 10
    },
    buttonTitle: {
        fontSize: 15,
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
});
