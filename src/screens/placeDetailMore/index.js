/**
 * Created by n.moharami on 4/18/2019.
 */
import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, BackAndroid, TextInput, Linking, AsyncStorage, BackHandler, Alert, Image} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import AIcon from 'react-native-vector-icons/dist/AntDesign';
import MaIcon from 'react-native-vector-icons/dist/MaterialIcons';
import FIcon from 'react-native-vector-icons/dist/Feather';
import ENIcon from 'react-native-vector-icons/dist/Entypo';
import MIcon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://ppt.azarinpro.info';
Axios.defaults.baseURL = url;
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
import Loader from '../../components/loader'
import PlaceFooter from "../../components/placeFooter";
import SlideShow from "../../components/slideShow";
import weather from "../../assets/weather.png";
import moment_jalaali from 'moment-jalaali'
import HTML from 'react-native-render-html';

class PlaceDetailMore extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: false,
            homeActive: true,
            modalVisible: false,
            modalTitle: '',
            weather: {},
            eventValidator: false,
            placeValidator: false,
            events: [],
            places: [],
            travels: [],
            likeStatus: this.props.item && this.props.item.liked ? this.props.item.liked : undefined
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        // this.setState({loading: true });
        console.log('sssssssssssssssssssssssssssssssssubNamethis.props.subName', this.props.subName)

        if(this.props.subName === 'شهر') {
            this.setState({loading: true });
            console.log('this.props.item.translations.title', this.props.item.translations[0].title)
            console.log('this.props.item', this.props.item)
            Axios.get('/country/weather?city='+this.props.item.translations[0].title).then(response => {
                this.setState({weather: response.data.data});
                console.log('response.data.data',  response.data.data)
                Axios.get('country/city/single?id='+this.props.item.id).then(response => {
                    this.setState({loading: false, travels: response.data.data.travelogues});
                })
                    .catch((error) => {
                        console.log(error);
                        this.setState({loading: false});
                    });
            })
                .catch((error) => {
                    console.log(error.response);
                    this.setState({loading: false});
                });
        }
        else if(this.props.subName === 'استان') {
            this.setState({loading: true });
            Axios.get('country/state/single?id='+this.props.item.id).then(response => {
                this.setState({loading: false, travels: response.data.data.travelogues});
            })
                .catch((error) => {
                    console.log(error);
                    this.setState({loading: false});
                });
        }
        else if(this.props.subName === 'کشور') {
            this.setState({loading: true });
            Axios.get('country/single?id='+this.props.item.id).then(response => {
                this.setState({loading: false, travels: response.data.data.travelogues});
            })
                .catch((error) => {
                    console.log(error);
                    this.setState({loading: false});
                });

        }
    }
    changeLike() {
            AsyncStorage.getItem('token').then((info) => {
                if(info !== null) {
                    this.setState({loading: true });
                    Axios.post('business/like', {id: this.props.item.id }).then(response => {
                        this.setState({ likeStatus: response.data.data.liked, loading: false});
                    })
                        .catch((error) => {
                            console.log(error);
                            this.setState({loading: false});
                        });
                }
                else {
                    console.log('it is not login')
                    Alert.alert('', 'لطفا ابتدا وراد شوید')
                    Actions.loginPage();
                }
            });
    }
    render() {
        if(this.props.item && this.props.item.social_media && this.props.item.social_media !== null) {
            // console.log('JSON.parse(this.props.item.social_media', JSON.parse(this.props.item.social_media))
            // console.log(' JSON.parse(this.props.item.social_media).telegrammmmmmmmmmmmmmmmmmmmm',  JSON.parse(this.props.item.social_media).telegram)
        }
        console.log('sssssssstausss like', this.state.likeStatus)
        // console.log('this.props.item', this.props.item)
        // console.log('this.props.item.liked', this.props.item.liked)

        // console.log('this.props.item.category.form.length', this.props.item.category.form.length)
        const myHtml = '<p>' + this.props.item.content + '</p>';
        const photos = [

        ];
        if(this.state.loading){
            return (<Loader />)
        } else return (
            <View style={styles.container}>
                <View style={styles.top}>
                    <TouchableOpacity onPress={() => this.onBackPress()}>
                        <FIcon name="arrow-left" size={23} color="gray" />
                    </TouchableOpacity>
                </View>
                {/*<View>*/}
                {/*<SlideShow activeSlide={1} />*/}
                {/*</View>*/}
                <ScrollView style={styles.scroll}>
                    <View style={styles.body}>
                        {/*<SlideShow activeSlide={1} images={this.props.item.attachments} item={this.props.item} />*/}
                        <SlideShow activeSlide={1} images={this.props.item.attachments} />

                        <View style={styles.contentBody}>

                            <View style={styles.weather}>
                                {
                                    this.props.subName === 'شهر' ?
                                        <View style={styles.weatherRow}>
                                            <Image source={weather} style={{width: 50, resizeMode: 'contain',}}/>
                                            <View style={{padding: 5}}>
                                                <View style={{flexDirection: 'row', position: 'relative', zIndex: 2}}>
                                                    <Text style={styles.title}>{Math.floor(this.state.weather.main.temp)}</Text>
                                                    <Text style={[styles.title, {paddingBottom: 30, position: 'absolute', zIndex: 3, top: -3, left: 15}]}>0c</Text>
                                                </View>
                                                <Text style={[styles.title, {fontSize: 12}]}>{this.state.weather.name} - {this.state.weather.weather[0].description}</Text>
                                            </View>
                                        </View> :
                                        <View>
                                            <Text style={[styles.contentTxt, {paddingTop: 20}]}>آخرین بروزرسانی: { this.props.item.updated_at !== null && moment_jalaali(this.props.item.updated_at, 'YYYY-MM-DD').format('jYYYY/jMM/jDD')}</Text>
                                            {
                                                this.state.likeStatus !== undefined ?
                                                    <TouchableOpacity onPress={() => this.changeLike()} style={[styles.button, {  backgroundColor: this.state.likeStatus ? '#FF8C00' : 'rgb(66, 183, 42)' }]}>
                                                        <Text style={[styles.title, {color: 'white', fontSize: 12}]}>{this.state.likeStatus ? 'حذف از علاقه مندی' : 'افزودن به علاقه مندی'}</Text>
                                                    </TouchableOpacity> : null
                                            }
                                        </View>
                                }
                                {
                                    this.props.subName === 'شهر' ||  this.props.subName === 'استان' || this.props.subName === 'کشور' ?
                                        <View style={styles.weatherRow}>
                                            <TouchableOpacity onPress={() => Actions.travelogue({travels: this.state.travels, place: this.props.item.title, subName: this.props.subName, placeId: this.props.item.id, item: this.props.item})} style={styles.button2}>
                                                <Text style={[styles.title2, {color: 'rgb(66, 183, 42)', fontSize: 12}]}>سفرنامه ها</Text>
                                            </TouchableOpacity>
                                            {/*{*/}
                                            {/*this.state.eventValidator ?*/}
                                            {/*<TouchableOpacity onPress={() => Actions.events()} style={styles.button}>*/}
                                            {/*<Text style={[styles.title, {color: 'rgb(66, 183, 42)', fontSize: 12}]}>رویدادها</Text>*/}
                                            {/*</TouchableOpacity>*/}
                                            {/*: null*/}
                                            {/*}*/}
                                            {/*{*/}
                                            {/*this.state.placeValidator ?*/}
                                            {/*<TouchableOpacity onPress={() => Actions.places()} style={styles.button}>*/}
                                            {/*<Text style={[styles.title, {color: 'rgb(66, 183, 42)', fontSize: 12}]}>تمام مکان ها</Text>*/}
                                            {/*</TouchableOpacity>*/}
                                            {/*: null*/}
                                            {/*}*/}
                                        </View> : null
                                }
                            </View>
                            <View style={styles.content}>
                                <Text style={styles.title}>{this.props.item.title}</Text>
                                {
                                    this.props.fav ?
                                        <Text style={styles.placeTxt}>تعداد رای :{this.props.fav} نفر</Text>
                                        :
                                        <Text style={styles.placeTxt}>{this.props.subName}</Text>
                                }
                                {/*<View style={styles.infoRow}>*/}
                                {/*<Text style={styles.contentTxt}>توسط آرش انوشه</Text>*/}
                                {/*<Text style={styles.location}>ایران- تهران . </Text>*/}
                                {/*</View>*/}

                                <View style={styles.infoRow}>

                                    {
                                        this.props.item.user ?
                                            <Text style={styles.contentTxt}>توسط {this.props.item.user.fname } {this.props.item.user.lname }</Text>
                                            :
                                            <Text style={styles.contentTxt}>  {this.props.item.created_at !== null ? moment_jalaali(this.props.item.created_at, 'YYYY-MM-DD').format('jYYYY/jMM/jDD') : null}</Text>
                                    }
                                    {
                                        this.props.item.country || this.props.item.city ?
                                            <Text style={styles.location}>{this.props.item.country ? this.props.item.country.title : null}- { this.props.item.city ? this.props.item.city.title : null} . </Text>
                                            : (
                                            this.props.item.city_name || this.props.item.country_name ?
                                                <Text style={styles.location}>{this.props.item.country_name ? this.props.item.country_name : null}{this.props.item.city_name && this.props.item.country_name ? '-' : null} { this.props.item.city_name ? this.props.item.city_name : null} . </Text>
                                                : null
                                        )
                                    }
                                </View>
                            </View>
                        </View>
                        {/*<Text style={[styles.placeTxt, {lineHeight: 20}]}>{this.props.item.content}</Text>*/}
                        {
                            this.props.item && this.props.item.content !== null ?
                                <HTML html={myHtml} tagsStyles={{
                                    p: {
                                        alignSelf: "flex-end",
                                        fontFamily: 'IRANSansMobile(FaNum)',
                                        color: 'rgba(51, 54, 64, 1)',
                                        lineHeight: 30,
                                        paddingBottom: 10,
                                        paddingTop: 20,
                                        fontSize: 14
                                    },
                                    img: {
                                        width: 300,
                                        height: 200,
                                        marginRight: 'auto',
                                        marginLeft: 'auto'
                                    }
                                }} />
                                : <Text> </Text>
                        }
                        {
                            this.props.item && this.props.item.social_media && this.props.item.social_media !== null && JSON.parse(this.props.item.social_media).telegram !== undefined ?
                                <View style={styles.navitemContainer}>
                                    <TouchableOpacity onPress={() =>  Linking.openURL('http://t.me/'+ JSON.parse(this.props.item.social_media).telegram)}>
                                        <View style={styles.navContainer}>
                                            <MIcon name="telegram" size={25} color="#00BFFF" />
                                            <Text style={[styles.text, {color: 'black',paddingTop: 5}]}>تلگرام</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() =>  Linking.openURL('http://t.me/'+ JSON.parse(this.props.item.social_media).instagram)}>
                                        <View style={styles.navContainer}>
                                            <Icon name="instagram" size={18} color="#00BFFF" />
                                            <Text style={[styles.text, {color: 'black', paddingTop: 5}]}>اینستاگرام</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() =>  Linking.openURL('http://t.me/'+ JSON.parse(this.props.item.social_media).google)}>
                                        <View style={styles.navContainer}>
                                            <Icon name="google-plus" size={18} color="#00BFFF" />
                                            <Text style={[styles.text, {color: 'black', paddingTop: 5}]}>گوگل پلاس</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                : null
                        }
                        {
                            this.props.item && this.props.item.category && this.props.item.category.form.length !== 0 ?
                                <View style={{width: '100%'}}>
                                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end'}} >
                                        <Text style={[styles.text, {paddingTop: 15, fontSize: 12, paddingBottom: 15}]}>امکانات و ویژگی ها: </Text>
                                        <MIcon name={"format-list-checkbox"} size={20} color="#FF1493" style={{paddingLeft: 10}} />
                                    </View>
                                    <View style={styles.infoContainer}>
                                        {
                                            this.props.item.category.form.map((item, index)=> {
                                                return <View style={[styles.infoRow, {justifyContent: 'space-between'}]} key={index}>
                                                    <Text style={[styles.text, {color: 'rgb(50, 50, 50)', paddingLeft: '40%',  paddingRight: 10}]}>{item.description}</Text>
                                                    <Text style={[styles.text, {color: 'rgb(63, 175, 40)',  alignSelf: 'flex-end'}]}>{item.title}</Text>
                                                </View>
                                            })
                                        }
                                    </View>
                                </View>
                                : null
                        }
                        {
                            this.props.item && this.props.item.tag && this.props.item.tag.length !== 0 ?
                                <View style={{width: '100%'}}>
                                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end'}} >
                                        <Text style={[styles.text, {paddingTop: 15, fontSize: 12}]}>تگ ها: </Text>
                                        <AIcon name={"wifi"} size={20} color="#FF1493" style={{paddingLeft: 10, paddingTop: 15}} />
                                    </View>
                                    <ScrollView style={{
                                        transform: [
                                            { scaleX: -1},
                                        ],
                                    }}
                                                horizontal={true} showsHorizontalScrollIndicator={false}
                                    >
                                        <View style={styles.navitemContainer}>
                                            {
                                                this.props.item.tag.map((item ,index)=> {
                                                    return <TouchableOpacity key={index} onPress={() => null}>
                                                        <View style={[styles.navContainerH]}>
                                                            <Text style={[styles.text, {color: 'black', paddingRight: 3}]}>{item.title}</Text>
                                                            <ENIcon name="tumblr-with-circle" size={15} color="black" />
                                                        </View>
                                                    </TouchableOpacity>
                                                })
                                            }
                                        </View>
                                    </ScrollView>
                                </View>
                                : null
                        }
                        {
                            this.props.item && this.props.item.phone && this.props.item.website && this.props.item.email && this.props.item.address ?
                                <View style={{width: '100%'}}>
                                    <Text style={[styles.text, {color: 'black', fontSize: 12, paddingTop: 15, paddingBottom: 15}]}>اطلاعات تماس : </Text>
                                    <View style={styles.infoContainer}>
                                        <View style={styles.infoRow}>
                                            <Text style={[styles.text, {color: 'black', paddingRight: 10}]}>شماره تماس: {this.props.item.phone}</Text>
                                            <Icon name="phone" size={18} color="rgb(63, 175, 40)" />
                                        </View>
                                        <View style={styles.infoRow}>
                                            <Text style={[styles.text, {color: 'black', paddingRight: 10}]}>آدرس: {this.props.item.address}</Text>
                                            <MaIcon name="location-on" size={20} color="rgb(63, 175, 40)" />
                                        </View>
                                        <View style={styles.infoRow}>
                                            <Text style={[styles.text, {color: 'black', paddingRight: 10}]}>وبسایت: {this.props.item.website}</Text>
                                            <MIcon name="web" size={16} color="rgb(63, 175, 40)" />
                                        </View>
                                        <View style={styles.infoRow}>
                                            <Text style={[styles.text, {color: 'black', paddingRight: 10}]}>ایمیل: {this.props.item.email}</Text>
                                            <Icon name="envelope" size={16} color="rgb(63, 175, 40)" />
                                        </View>
                                    </View>
                                </View> : null
                        }
                    </View>
                </ScrollView>
                <PlaceFooter active="detail" item={this.props.item} fav={this.props.fav} subName={this.props.subName} comments={this.props.comments} />
            </View>
        );
    }
}
export default PlaceDetailMore;
