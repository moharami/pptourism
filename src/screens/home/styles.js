
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(234, 234, 234)',

    },
    scroll: {
        paddingTop: 0,
        paddingRight: 20,
        paddingLeft: 20,
        // marginBottom: 90,
        // backgroundColor: 'red',
        position: 'absolute',
        top: 190,
        bottom: 60,
        width: '100%'

    },
    body: {
        // paddingBottom: 150,
        paddingTop: 10,
        // alignItems: 'flex-start',
        // justifyContent: 'center',
        // backgroundColor: 'green',
        width: '100%'
    },
    navitemContainer: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-between',
        transform: [
            {rotateY: '180deg'},
        ],
        paddingTop: 45,
        paddingBottom: 30,
        paddingRight: 20,
        height: 120
    },
    navContainer: {
        alignItems: "center",
        justifyContent: 'center',
        width: 94,
        height: 96,
        paddingBottom: 40,
        marginLeft: 20,
        backgroundColor: 'white'

    },
    text: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        // paddingBottom: 55,
        position: 'absolute',
        color: 'black',
        bottom: 15

    },
    label: {
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',
        // paddingBottom: 55,
        color: 'black',
        textAlign: 'center',
        paddingTop:50
    }
});
