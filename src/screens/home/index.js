import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, BackAndroid, TextInput, ActivityIndicator, AsyncStorage, BackHandler, Alert, Image} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import FIcon from 'react-native-vector-icons/dist/Feather';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://ppt.azarinpro.info';
Axios.defaults.baseURL = url;
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
import Loader from '../../components/loader'
import FooterMenu from "../../components/footerMenu/index";
import AppHeader from "../../components/appHeader";
import homeIcon from "../../assets/homeIcon.png";
import event1 from "../../assets/event.png";
import Place from "../../components/place/index";


class Home extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: false,
            homeActive: true,
            business: [],
            events: [],
            active: 'business',
            pageBuis: 2,
            pageEve: 2,
            loadingMore: false,
            logout: false
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        // BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        this.logout();
        return true;
    };
    logout() {
        Alert.alert(
            'خروج از حساب کاربری',
            'آیا از  خروج خود مطمئن هستید؟',
            [
                {text: 'خیر', onPress: () => this.setState({logout: false}), style: 'cancel'},
                {text: 'بله', onPress: () =>  this.setState({logout: true}, () => {BackHandler.exitApp()})},
            ]
        );
    }
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        this.setState({loading: true});
        Axios.post('/business/show').then(response => {
            this.setState({business: response.data.data.data});
            console.log('response.data.data',  response.data.data)
            store.dispatch({type: 'BUSINESS_FETCHED', payload: response.data.data.data});
            Axios.get('/country/event/get-events').then(response => {
                this.setState({events: response.data.data.event});
                store.dispatch({type: 'EVENTS_FETCHED', payload: response.data.data.event});
                // Axios.post('/business/points', {id}).then(response=> {
                //     store.dispatch({type: 'USER_INFO_FETCHED', payload: response.data.data});
                //     this.setState({loading: false});
                // })
                //     .catch((error) => {
                //         Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                //         this.setState({loading: false});
                //     });

                AsyncStorage.getItem('token').then((info) => {
                    if(info !== null) {
                        const newInfo = JSON.parse(info);
                        Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;
                        console.log(newInfo.token)
                        Axios.get('/core/user').then(response=> {
                            store.dispatch({type: 'USER_INFO_FETCHED', payload: response.data.data});
                            this.setState({loading: false});
                        })
                            .catch((error) => {
                                Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                                this.setState({loading: false});
                            });
                    }
                    else {
                        this.setState({loading: false});
                    }
                })
            })
                .catch((error) => {
                    console.log(error);
                    Alert.alert('','خطایی رخ داده مجددا سعی نمایید')
                    this.setState({loading: false});
                });
        })
            .catch((error) => {
                console.log(error.response);
                Alert.alert('','خطایی رخ داده مجددا سعی نمایید')
                this.setState({loading: false});
            });

    }
    handleLoadMore() {
        if(this.state.active === 'business') {
            this.setState({loadingMore: true});
            Axios.post('/business/show', {page: this.state.pageBuis}).then(response => {
                console.log(response.data.data.data);
                if( response.data.data.data && response.data.data.data.length !== 0) {
                    store.dispatch({type: 'NEW_BUSINESS_FETCHED', payload: response.data.data.data});
                    this.setState({loadingMore: false, page: ++this.state.pageBuis });
                }
                else {
                    Alert.alert('','موارد بیشتری یافت نشد');
                    this.setState({loadingMore: false});
                }
            })
                .catch( (error) =>{
                    console.log(error.response);
                });
        }
        else{
            this.setState({loadingMore: true});
            Axios.get('/country/event/get-events', {page: this.state.pageEve}).then(response => {
                console.log(response.data.data.event);


                if(response.data.data.event && response.data.data.event.length !== 0) {
                    store.dispatch({type: 'NEW_EVENTS_FETCHED', payload: response.data.data.event});
                    console.log('response.data.data.event in page 2', response.data.data.event)
                    this.setState({loadingMore: false, page: ++this.state.pageEve });
                }
                else {
                    Alert.alert('','موارد بیشتری یافت نشد');
                    this.setState({loadingMore: false});
                }
            })
                .catch( (error) =>{
                    console.log(error.response);
                });
        }
    }
    render() {
        const {businesses, events} = this.props;
        console.log('businesses', businesses)
        console.log(' this.state.active ===  && events ', this.state.active === 'business' && businesses.length !==0 || this.state.active === 'event' && events.length !==0 )
        if(this.state.loading){
            return (<Loader />)
        } else return (
            <View style={styles.container}>
                <AppHeader pageTitle="اطراف من" />
                <ScrollView style={{ transform: [
                    { scaleX: -1},
                    // {  rotate: '180deg'},

                ],}}
                            horizontal={true} showsHorizontalScrollIndicator={false}
                >
                    <View style={styles.navitemContainer}>
                        <TouchableOpacity onPress={() => this.setState({active: 'event'})}>
                            <View style={styles.navContainer}>
                                <Image source={event1} style={{ width: 50, resizeMode: 'contain'}} />
                                {/*<Image source={{uri: "http://fitclub.ws/files?uid="+item.attachments[0].uid+"&width=104&height=92" }} style={{ width: 50, height: 50, tintColor: this.state.active === item.id ? 'red': 'gray'}} />*/}
                                <Text style={[styles.text, {color: this.state.active === 'event' ? 'green': 'black'}]}>رویدادها </Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.setState({active: 'business'})}>
                            <View style={styles.navContainer}>
                                <Image source={homeIcon} style={{ width: 50, resizeMode: 'contain'}} />
                                {/*<Image source={{uri: "http://fitclub.ws/files?uid="+item.attachments[0].uid+"&width=104&height=92" }} style={{ width: 50, height: 50, tintColor: this.state.active === item.id ? 'red': 'gray'}} />*/}
                                <Text style={[styles.text, {color: this.state.active === 'business' ? 'green': 'black'}]}>اماکن و کسب و کارها</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
                <ScrollView style={styles.scroll}>
                    <View style={styles.body}>
                        {

                                this.state.active === 'business' ?
                                    ( businesses!== 0  ? businesses.map((item, index)=> {
                                        return <Place item={item} key={index} name="مکان" />
                                    }):   <Text style={styles.label}>موردی یافت نشد</Text>
                            )
                                :
                            (
                                events.length !== 0 ? events.map((item, index)=> {
                                 return <Place item={item} key={index} name="رویداد" />
                                }) :   <Text style={styles.label}>موردی یافت نشد</Text>
                            )

                        }
                        {
                            // this.state.active === 'business' && businesses.length !==0 || this.state.active === 'event' && events.length !==0 ?
                            this.state.active === 'business' && businesses.length !== 0?
                                <TouchableOpacity onPress={() => this.setState({loadingMore: true}, () => {this.handleLoadMore()})} style={{flexDirection: 'row'}}>
                                    <View style={{width: '100%', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', paddingBottom: 30}}>
                                        {
                                            this.state.loadingMore ?
                                        <ActivityIndicator
                                            size={30}
                                            animating={true}
                                            color='green'
                                        />
                                                : null
                                        }
                                        <Text style={{textAlign: 'center', color: 'green'}}>مشاهده بیشتر</Text>
                                    </View>
                                </TouchableOpacity>
                                : null
                        }
                    </View>
                </ScrollView>
                <FooterMenu active="home"/>
            </View>
        );
    }
}
function mapStateToProps(state) {
    return {
        businesses: state.posts.businesses,
        events: state.posts.events
    }
}
export default connect(mapStateToProps)(Home);
