/**
 * Created by n.moharami on 4/18/2019.
 */
/**
 * Created by n.moharami on 4/18/2019.
 */
import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, BackAndroid, TextInput, ActivityIndicator, AsyncStorage, BackHandler, Alert, Image} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import SIcon from 'react-native-vector-icons/dist/SimpleLineIcons';
import FIcon from 'react-native-vector-icons/dist/Feather';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://ppt.azarinpro.info';
Axios.defaults.baseURL = url;
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
import Loader from '../../components/loader'
import PlaceFooter from "../../components/placeFooter";
import SlideShow from "../../components/slideShow";
import CommentModal from "./newCommentModal";
import CommentItem from '../../components/commentItem'

class Comments extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: false,
            homeActive: true,
            modalVisible: false,

        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);

    }
    closeModal() {
        this.setState({modalVisible: false});
    }
    render() {
        console.log('this.props.comments', this.props.comments)
        console.log('id', this.props.item)
        if(this.state.loading){
            return (<Loader />)
        } else return (
            <View style={styles.container}>
                <View style={styles.top}>
                    <TouchableOpacity onPress={() => this.onBackPress()}>
                        <FIcon name="arrow-left" size={23} color="gray" />
                    </TouchableOpacity>
                    <Text style={[styles.title, {paddingLeft: '31%'}]}>نظر و امتیازدهی</Text>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.body}>
                        {
                            this.props.subName === 'مکان' || this.props.subName === 'رویداد' ?
                                <TouchableOpacity onPress={()=> this.setState({modalVisible: true})} style={styles.circleContainer}>
                                    <FIcon name="plus" size={22} color="white" />
                                </TouchableOpacity>
                                : null
                        }
                        {
                            this.props.comments !== undefined && this.props.comments.length !== 0 ?  this.props.comments.map((item, index)=> <CommentItem item={item} key={index} />
                            ) :   <Text style={[styles.title, {paddingLeft: '31%', paddingTop: 30, paddingRight: '35%'}]}>دیدگاهی یافت نشد</Text>
                        }
                    </View>
                </ScrollView>
                <CommentModal
                    id={this.props.item.id}
                    title='ثبت نظر'
                    closeModal={(title) => this.closeModal(title)}
                    modalVisible={this.state.modalVisible}
                />
                <PlaceFooter active="comment" item={this.props.item} subName={this.props.subName} fav={this.props.fav} comments={this.props.comments} />
            </View>
        );
    }
}
export default Comments;
