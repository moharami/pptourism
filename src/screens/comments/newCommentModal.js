
import React, { Component } from 'react';
import {
    Modal,
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    TextInput,
    Alert,
    ScrollView,
    ImageBackground,
    AsyncStorage
}
    from 'react-native'
import Icon from 'react-native-vector-icons/dist/FontAwesome'
import AIcon from 'react-native-vector-icons/dist/AntDesign'
import FIcon from 'react-native-vector-icons/dist/Feather'
import MIcon from 'react-native-vector-icons/dist/MaterialIcons'
import Axios from 'axios';
export const url = 'http://ppt.azarinpro.info';
Axios.defaults.baseURL = url;
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
import Loader from '../../components/loader'
import Selectbox from 'react-native-selectbox'
import ImagePicker from 'react-native-image-picker';
import {
    BallIndicator
} from 'react-native-indicators';
import {Actions} from 'react-native-router-flux'

class NewCommentModal extends Component {
    state = {
        text: '',
        modalVisible: false,
        loading: false,
        what: '',
        tags: [],
        starArray: [false, false, false, false, false],
        tag: 0,
        active: '',
        advantage: '',
        fault: '',
        title: '',
        travelStatus:{key: 0, label: "انتخاب نشده", value: ''},
        travelStatusTitle:null,
        content: '',
        src: null
    };
    closeModal() {
        this.setState({modalVisible: false});
    }
    selectPhotoTapped() {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true
            }
        };
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };

                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.setState({src: response.uri})
            }
        });
    }
    addComment() {
        if(this.state.content === '') {
            Alert.alert('','متن نظر نمی تواند خالی باشد')
        }
        else {
            AsyncStorage.getItem('token').then((info) => {
                if(info !== null) {

                    const newInfo = JSON.parse(info);
                    console.log('token', newInfo.token)
                    Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;

                    let formdata = new FormData();
                    this.state.title !== '' && formdata.append("title", this.state.title)
                    this.state.content !== '' &&  formdata.append("comment", this.state.content)
                    formdata.append("id", this.props.id)
                    let data = [];
                    if(this.state.advantage.length !==0){
                        const adv = this.state.advantage.split('\n');
                        adv.map((item)=> {
                            data.push({type: "harm", data: item})
                        })
                    }
                    if(this.state.fault.length !==0){
                        const fa = this.state.fault.split('\n');
                        fa.map((item)=> {
                            data.push({type: "benefit", data: item})
                        })
                    }
                    data = JSON.stringify(data);
                    if(this.state.advantage.length !==0 || this.state.fault.length !==0) {
                        formdata.append("data", data)
                    }
                    let count = 0;
                    this.state.starArray.map((item)=> {
                        if(item === true){
                            ++count;
                        }
                    })
                    formdata.append("point", count)

                    if(this.state.src !== null) {
                        formdata.append("type", "image");
                        formdata.append("document[]", {
                            uri: this.state.src,
                            type: 'image/jpeg',
                            name: 'document[]'
                        });
                    }
                    console.log('Formdata', formdata)

                    this.setState({loading: true});
                    Axios.post('/business/comment', formdata
                    ).then(response=> {
                        this.setState({loading: false});
                        Alert.alert('','نظر شما با موفقیت افزوده شد');
                        this.props.closeModal();
                        console.log('travel addding  response', response.data);
                    })
                        .catch((response) => {
                            console.log('resssponse erroreie create ', response.response.data.message);
                            if(response.response.data.message === "Unauthenticated."){
                                Alert.alert('','لطفا ابتدا وارد شوید');
                                this.setState({loading: false});
                                Actions.loginPage({newPlace: true});
                            }
                            else{
                                Alert.alert('','خطایی رخ داده مجددا سعی نمایید');
                                this.setState({loading: false});
                            }
                        });

                }
                else {
                    console.log('it is not login')
                    this.props.closeModal();
                    Actions.loginPage({travel: true})
                }
            });
        }
    }
    boldStar(index) {
        let items = this.state.starArray;
        items[index] = !items[index];
        this.setState({starArray: items})
    }
    onKeyPres = ({ nativeEvent }) => {
        console.log('pppppresssed', nativeEvent)

        if (nativeEvent.key === 'Enter') {
        Alert.alert('','enter pressed')
            console.log('pppppresssed', nativeEvent)
        }
    };
    render() {
        return (
            <View style={styles.container}>
                <Modal animationType = {"fade"} transparent = {true}
                       visible = {this.props.modalVisible}
                       onRequestClose = {() => this.props.onChange(false)}
                       onBackdropPress= {() => this.props.onChange(!this.state.modalVisible)}
                >
                    <View style = {styles.modal}>
                        <View style={styles.box}>
                            <View style={styles.headerContainer}>
                                <TouchableOpacity onPress={() => this.props.closeModal()}>
                                    <Icon name="close" size={20} color={ "black"} />
                                </TouchableOpacity>
                                <Text style={styles.headerText}>{this.props.title}</Text>
                            </View>
                            <ScrollView style={{width: '100%'}}>
                                <View style={{alignItems: 'center', justifyContent: 'center'}}>
                                    <Text style={styles.text}>افزودن عکس</Text>
                                    <View style={styles.photoContainer}>
                                        <ImageBackground style={styles.Image} source={{uri: this.state.src === null ? "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSlzPUy6JN_a_IMHbXR8eD49XIkuqrxSV5opddszq8dpnWI05j-" : this.state.src}}>
                                            <TouchableOpacity onPress={() => this.selectPhotoTapped()} style={{
                                                backgroundColor:'rgba(0,0,0,.3)',
                                                height: 80,
                                                width: 80,
                                                alignItems: "center",
                                                justifyContent: "center",
                                                borderRadius: 6
                                            }}>
                                                <Icon name="camera" size={20} color="white"/>
                                                <Text style={styles.addText2}>افزودن عکس</Text>
                                            </TouchableOpacity>
                                        </ImageBackground>
                                    </View>
                                    <View style={styles.points}>
                                        <View style={styles.startContainer}>
                                            {
                                                [1, 2, 3, 4, 5].map((item, index)=> {
                                                    return <TouchableOpacity onPress={() => this.boldStar(index)} key={index}>
                                                        <AIcon name={this.state.starArray[index] === true ? "star" : "staro"} size={20} color="rgba(255, 193, 39, 1)" style={{paddingRight: 3}} />
                                                    </TouchableOpacity>
                                                }
                                                )
                                            }
                                        </View>
                                        <Text style={styles.text}>امتیازدهی</Text>
                                    </View>

                                    <Text style={styles.text}>موضوع</Text>
                                    <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginBottom: 20, marginTop: 5, borderRadius: 7 }}>
                                        <TextInput
                                            maxLength={15}
                                            placeholder={'موضوع'}
                                            placeholderTextColor={'gray'}
                                            underlineColorAndroid='transparent'
                                            value={this.state.title}
                                            style={{
                                                height: 40,
                                                paddingRight: 15,
                                                width: '100%',
                                                color: 'gray',
                                                fontSize: 14,
                                            }}
                                            onChangeText={(text) => {this.setState({title: text})}}
                                        />
                                    </View>
                                    <Text style={styles.text}>متن نظر</Text>
                                    <View style={{width: '100%', alignItems: 'center', justifyContent: 'center', marginTop: 10}}>
                                        <TextInput
                                            multiline = {true}
                                            numberOfLines = {6}
                                            value={this.state.content}
                                            onChangeText={(text) => this.setState({content: text}, () => console.log('content', this.state.content.split('\n')))}
                                            placeholderTextColor={'rgb(142, 142, 142)'}
                                            underlineColorAndroid='transparent'
                                            placeholder="متن نظر..."
                                            // onSubmitEditing={(e)=>{
                                            //     console.log(e) // called only when multiline is false
                                            // }}
                                            style={{
                                                // height: 45,
                                                paddingRight: 15,
                                                width: '95%',
                                                fontSize: 14,
                                                color: 'rgb(142, 142, 142)',
                                                // textAlign:'right',
                                                backgroundColor: 'rgb(247, 247, 247)',
                                                borderWidth: 1,
                                                borderColor: 'rgb(236, 236, 236)',
                                                borderRadius: 10
                                            }}
                                        />
                                    </View>
                                    <View style={{flexDirection: 'row', alignSelf: 'flex-end'}}>
                                        <Text style={[styles.text, {fontSize: 10, color: 'gray'}]}>(در هر خط یک مورد را بنویسید) </Text>
                                        <Text style={styles.text}>مزایا: </Text>
                                    </View>
                                    <View style={{width: '100%', alignItems: 'center', justifyContent: 'center', marginTop: 10}}>
                                        <TextInput
                                            multiline = {true}
                                            numberOfLines = {6}
                                            value={this.state.advantage}
                                            onChangeText={(text) => this.setState({advantage: text})}
                                            placeholderTextColor={'rgb(142, 142, 142)'}
                                            underlineColorAndroid='transparent'
                                            placeholder="مزایا..."
                                            style={{
                                                // height: 45,
                                                paddingRight: 15,
                                                width: '95%',
                                                fontSize: 14,
                                                color: 'rgb(142, 142, 142)',
                                                // textAlign:'right',
                                                backgroundColor: 'rgb(247, 247, 247)',
                                                borderWidth: 1,
                                                borderColor: 'rgb(236, 236, 236)',
                                                borderRadius: 10
                                            }}
                                        />
                                    </View>
                                    <View style={{flexDirection: 'row', alignSelf: 'flex-end'}}>
                                        <Text style={[styles.text, {fontSize: 10, color: 'gray'}]}>(در هر خط یک مورد را بنویسید) </Text>
                                        <Text style={styles.text}>معایب: </Text>
                                    </View>
                                    <View style={{width: '100%', alignItems: 'center', justifyContent: 'center', marginTop: 10}}>
                                        <TextInput
                                            multiline = {true}
                                            numberOfLines = {6}
                                            value={this.state.fault}
                                            onChangeText={(text) => this.setState({fault: text})}
                                            placeholderTextColor={'rgb(142, 142, 142)'}
                                            underlineColorAndroid='transparent'
                                            placeholder="معایب..."
                                            style={{
                                                // height: 45,
                                                paddingRight: 15,
                                                width: '95%',
                                                fontSize: 14,
                                                color: 'rgb(142, 142, 142)',
                                                // textAlign:'right',
                                                backgroundColor: 'rgb(247, 247, 247)',
                                                borderWidth: 1,
                                                borderColor: 'rgb(236, 236, 236)',
                                                borderRadius: 10
                                            }}
                                        />
                                    </View>
                                </View>
                            </ScrollView>
                            {/*<Text style={styles.title}>یا</Text>*/}
                            {
                                this.state.loading ?
                                    <View style={{width: '100%', height: 120}}>
                                        <BallIndicator color='#21b34f' size={50} style={{height: 90}} />
                                    </View> :
                                    <TouchableOpacity onPress={() => this.addComment()} style={styles.advertise}>
                                        <Text style={styles.buttonTitle}>تایید</Text>
                                    </TouchableOpacity>
                            }

                        </View>
                    </View>
                </Modal>
            </View>
        )
    }
}
export default NewCommentModal

const styles = StyleSheet.create ({
    container: {
        alignItems: 'center',
        // backgroundColor: 'white',
        padding: 10,
        position: 'absolute',
        bottom: -200
    },
    modal: {
        position: 'relative',
        zIndex: 0,
        flexGrow: 1,
        justifyContent: 'center',
        // flex: 1,
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,.6)',
        paddingRight: 20,
        paddingLeft: 20
    },
    box: {
        width: '100%',
        height: '70%',
        backgroundColor: 'white',
        // backgroundColor: 'rgb(218, 218, 218)',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        borderRadius: 15,
        paddingBottom: 20
    },
    picker: {
        height: 40,
        backgroundColor: 'white',
        width: '40%',
        color: 'black',
        borderColor: 'lightgray',
        borderWidth: 1,
        // elevation: 4,
        borderRadius: 10,
        // marginBottom: 20,
        // overflow: 'hidden'
    },
    input: {
        width: '100%',
        fontSize: 16,
        paddingTop: 0,
        textAlign: 'right',
    },
    searchButton: {
        width: '90%',
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgb(20, 122, 170)'
    },
    searchText:{
        color: 'white'
    },
    // picker: {
    //     height:50,
    //     width: "100%",
    //     alignSelf: 'flex-end'
    // },
    label: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'rgb(60, 60, 60)'
    },
    headerContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 8,
        borderBottomWidth: 1,
        borderBottomColor: 'rgb(237, 237, 237)',
        marginBottom: 20
    },
    headerText: {
        fontSize: 15,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black'
    },
    advertise: {
        width: '35%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30,
        backgroundColor: '#8dc63f',
        padding: 5,
        marginTop: 20
    },
    buttonTitle: {
        fontSize: 14,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    inputLabel: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        alignSelf: 'flex-end',
        paddingRight: '10%',
        paddingBottom: 5
    },
    insContainer: {
        width: '100%',
        flexDirection: 'row',
        flexWrap: 'wrap',
        // alignItems: 'center',
        // justifyContent: 'space-between',
        padding: 8
    },
    info: {
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingTop: 20,
        paddingBottom: 20,
        borderBottomColor: 'white',
        borderBottomWidth: 1
    },
    rowsContainer: {
        width: '70%'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    itemContainer: {
        width: '48%'
    },
    title: {
        color: 'rgb(60, 60, 60)',
        fontSize: 11,
        fontFamily: 'IRANSansMobile(FaNum)',
        alignSelf: 'flex-end',
        paddingRight: 10
    },
    imageContainer: {
        borderRadius: 15,
        padding: 7,
        backgroundColor: 'white',
        width: '20%',
        marginRight: 10,
        marginLeft: 10
    },
    text: {
        fontSize: 13,
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'right',
        color: 'black',
        alignSelf: 'flex-end',
        paddingTop: 10,
        paddingBottom: 10

    },
    Image2: {
        width: 80,
        height: 80,
        borderRadius: 6,
        marginLeft: 7,
        marginBottom: 13,
        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden'
    },
    photoContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        // padding: 20,
        paddingRight: 30,
        flexWrap: 'wrap',
        borderRadius: 10,
        margin: 10
    },
    addImage2: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 80,
        height: 80,
        backgroundColor: 'rgb(59, 140, 219)',
        borderRadius: 6,
        // marginBottom: 13
    },
    addText2: {
        color: 'white',
        fontSize: 10,
        paddingTop: 10,
        textAlign: 'center'
    },
    Image: {
        width: 80,
        height: 80,
        borderRadius: 6,
        marginLeft: 13,
        // marginBottom: 30,
        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden',
        margin: 10,

    },
    startContainer: {
        flexDirection: 'row',
    },
    points: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        // paddingRight: 20,
        paddingLeft: 20
    }
})