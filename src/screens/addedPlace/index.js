import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, BackAndroid, TextInput, ActivityIndicator, AsyncStorage, BackHandler, Alert, Image} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/dist/FontAwesome5';
import FIcon from 'react-native-vector-icons/dist/Feather';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://ppt.azarinpro.info';
Axios.defaults.baseURL = url;
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
import Loader from '../../components/loader'
import FooterMenu from "../../components/footerMenu/index";
import FavoriteItem from "../../components/favoriteItem";
import AppHeader from "../../components/appHeader";
import {
    BallIndicator
} from 'react-native-indicators';

class AddedPlace extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: true,
            homeActive: true,
            Items: [],
            favorites: [],
            loadingFav: []
        }
        ;
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);

        AsyncStorage.getItem('token').then((info) => {
            if(info !== null) {
                const newInfo = JSON.parse(info);
                console.log('token', newInfo.token)
                Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;
                Axios.post('/business/user-business').then(response=> {
                    this.setState({loading: false, Items: response.data.data.creator});
                    console.log('response.data.data placcccces' , response.data.data)
                })
                    .catch((error) => {
                        Alert.alert('مشکلی در برقراری ارتباط با سرور رخ داده لطفا مجددا تلاش نمایید')
                        this.setState({loading: false});
                    });
            }
            else {
                console.log('it is not login')
                Alert.alert('', 'لطفا ابتدا وارد شوید')
                Actions.loginPage({travel: true})
            }
        });
    }
    render() {
        if(this.state.loading){
            return (<Loader />)
        } else return (
            <View style={styles.container}>
                <AppHeader pageTitle="مکان های ثبت شده" icon={false} />
                <ScrollView style={styles.scroll}>
                    <View style={styles.body}>
                        {
                            this.state.Items.length !== 0 ? this.state.Items.map((item, index)=>{
                                return <View key={index} style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: 10, marginBottom: 10}}>
                                    <TouchableOpacity onPress={() => Actions.newPlace({item: item})}>
                                        <FIcon name={"edit"} size={20} color="#4169E1" style={{paddingLeft: 10}} />
                                    </TouchableOpacity>
                                    <View style={styles.right}>
                                        <Text style={[styles.text, {paddingRight: 5, paddingBottom: 3,  fontSize: 11, color: item.status === 0 ? "#DAA520" : (item.status === 1 ? "green" : (item.status === 2 ? "red": undefined))}]}>({item.status === 0 ? "بررسی نشده" : (item.status === 1 ? "تایید شده" : (item.status === 2 ? "تایید نشده": "" ))})</Text>
                                        <Text style={styles.text}>{item.title}</Text>
                                    </View>
                                </View>

                            })  :   <Text style={[styles.text, {alignSelf: 'center', color: 'gray'}]}>موردی یافت نشد</Text>

                        }
                    </View>
                </ScrollView>
                <FooterMenu active="" />
            </View>
        );
    }
}
export default AddedPlace;

