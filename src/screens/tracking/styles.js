
import { StyleSheet, Dimensions} from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(234, 234, 234)',

    },
    scroll: {
        paddingTop: 0,
        paddingRight: 20,
        paddingLeft: 20,
        // marginBottom: 90,
        // backgroundColor: 'red',
        position: 'absolute',
        top: '80%',
        bottom: 0,
        width: '100%'

    },
    content: {
        // paddingBottom: 150,
        paddingTop: 10,
        // alignItems: 'flex-start',
        // justifyContent: 'center',
        // backgroundColor: 'green',
        width: '100%'
    },
    body: {
        paddingTop: 10,
        // position: 'relative',
        // zIndex: 2,
        width: '100%'
    },
    imageContainer: {
        position: 'absolute',
        zIndex: 20,
        // top: 50,
        // left: 50,
        // width: 60,
        backgroundColor: 'red'
    },
    image: {
        position: 'absolute',
        zIndex: 22,
        top: 0,
        left: 0,
        width: 50,
        height: 50,
        resizeMode: 'stretch'
    },
    pic: {
        position: 'absolute',
        zIndex: 21,
        top: 3,
        left: 5,
        width: 40,
        height: 38,
        borderRadius: 200,
        resizeMode: 'stretch'
    },
    navitemContainer: {
        width: '100%',
        // flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'center',
        // transform: [
        //     {rotateY: '180deg'},
        // ],
        // paddingTop: 45,
        // paddingBottom: 30,
        // paddingLeft: 20,
        paddingBottom: 40,
        // height: 120
    },
    navContainer: {
        alignItems: "center",
        justifyContent: 'center',
        width: '80%',
        // height: 156,
        // paddingBottom: 40,
        // marginLeft: 20,
        // marginRight: 20,
    },
    text: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        // paddingBottom: 55,
        position: 'absolute',
        color: 'black',
        bottom: 15

    },
    mapStyle: {
        width: '100%',
        height: Dimensions.get('screen').height
    },


    // map: {
    //     ...StyleSheet.absoluteFillObject
    // },
    bubble: {
        flex: 1,
        backgroundColor: "rgba(255,255,255,0.7)",
        paddingHorizontal: 18,
        paddingVertical: 12,
        borderRadius: 20
    },
    latlng: {
        width: 200,
        alignItems: "stretch"
    },
    button: {
        width: 80,
        paddingHorizontal: 12,
        alignItems: "center",
        marginHorizontal: 10
    },
    buttonContainer: {
        flexDirection: "row",
        marginVertical: 20,
        backgroundColor: "transparent"
    }
});
