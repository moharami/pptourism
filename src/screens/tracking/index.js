import React, {Component} from 'react';
// import {View, TouchableOpacity, ScrollView, Text, BackAndroid, TextInput, ActivityIndicator, AsyncStorage, BackHandler, Alert, Image, Platform} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://ppt.azarinpro.info';
Axios.defaults.baseURL = url;
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
import Loader from '../../components/loader'
import AppHeader from "../../components/appHeader";
import marker1 from '../../assets/marker.png'
import marker_bold from '../../assets/marker-bold.png'
import RouteItem from '../../components/routeItem'
import marker_location from '../../assets/marker-location.png'
import pic from '../../assets/test.jpg'

import {
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    Platform,
    PermissionsAndroid,
    ScrollView,
    Image
} from "react-native";
import MapView, {
    Marker,
    AnimatedRegion,
    Polyline,
    PROVIDER_GOOGLE
} from "react-native-maps";
import haversine from "haversine";

// const LATITUDE = 29.95539;
// const LONGITUDE = 78.07513;
const LATITUDE_DELTA = 0.009;
const LONGITUDE_DELTA = 0.009;
const LATITUDE = 35.715298;
const LONGITUDE = 51.404343;

class Tracking extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            latitude: LATITUDE,
            longitude: LONGITUDE,
            routeCoordinates: [],
            distanceTravelled: 0,
            prevLatLng: {},
            coordinates: {
                latitude: LATITUDE,
                longitude: LONGITUDE,
            },
            coordinate: new AnimatedRegion({
                latitude: LATITUDE,
                longitude: LONGITUDE,
                latitudeDelta: 0,
                longitudeDelta: 0
            })
        };
        this.onBackPress = this.onBackPress.bind(this);
    }

    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentDidMount() {
        const { coordinate } = this.state;

        // this.requestCameraPermission();

        this.watchID = navigator.geolocation.watchPosition(
            position => {
                const { routeCoordinates, distanceTravelled } = this.state;
                const { latitude, longitude } = position.coords;

                const newCoordinate = {
                    latitude,
                    longitude
                };
                console.log({ newCoordinate });

                if (Platform.OS === "android") {
                    if (this.marker) {
                        this.marker._component.animateMarkerToCoordinate(
                            newCoordinate,
                            500
                        );
                    }
                } else {
                    coordinate.timing(newCoordinate).start();
                }

                this.setState({
                    latitude,
                    longitude,
                    routeCoordinates: routeCoordinates.concat([newCoordinate]),
                    distanceTravelled:
                    distanceTravelled + this.calcDistance(newCoordinate),
                    prevLatLng: newCoordinate
                });
            },
            error => console.log(error),
            {
                enableHighAccuracy: true,
                timeout: 20000,
                maximumAge: 1000,
                distanceFilter: 10
            }
        );
    }

    componentWillUnmount() {
        navigator.geolocation.clearWatch(this.watchID);
    }

    getMapRegion = () => ({
        latitude: this.state.latitude,
        longitude: this.state.longitude,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
    });

    calcDistance = newLatLng => {
        const { prevLatLng } = this.state;
        return haversine(prevLatLng, newLatLng) || 0;
    };

    render() {
        if(this.state.loading){
            return (<Loader />)
        } else return (
            <View style={styles.container}>
                <AppHeader pageTitle="مسیریابی" icon={false} profile={false} />
                <View style={styles.body}>
                    <MapView
                        style={styles.mapStyle}
                        provider={PROVIDER_GOOGLE}
                        showUserLocation
                        followUserLocation
                        loadingEnabled
                        region={this.getMapRegion()}
                    >
                        <Polyline coordinates={this.state.routeCoordinates} strokeWidth={5} />
                        <Marker.Animated
                            ref={marker => {
                                this.marker = marker;
                            }}
                            coordinate={this.state.coordinate}
                        />


                        {/*<Marker ref={marker => {*/}
                            {/*this.marker = marker;*/}
                        {/*}}*/}
                                     {/*coordinate={this.state.coordinates}>*/}
                            {/*/!*<View style={styles.imageContainer}>*!/*/}
                            {/*/!*<Image source={pic} style={styles.pic}/>*!/*/}
                            {/*/!*<Image source={marker_bold} style={styles.image}/>*!/*/}
                            {/*/!*</View>*!/*/}
                            {/*<View style={styles.imageContainer}>*/}
                                {/*<Image source={pic} style={[styles.pic, {width: 30, height: 28}]}/>*/}
                                {/*<Image source={marker_location} style={[styles.image, { width: 40, height: 40}]}/>*/}
                            {/*</View>*/}
                        {/*</Marker>*/}



                    </MapView>
                    <View style={styles.buttonContainer}>
                        <TouchableOpacity style={[styles.bubble, styles.button]}>
                            <Text style={styles.bottomBarContent}>
                                {parseFloat(this.state.distanceTravelled).toFixed(2)} km
                            </Text>
                        </TouchableOpacity>
                    </View>

                </View>

                <ScrollView style={styles.scroll}>
                    <View style={styles.content} >
                        <RouteItem />
                        <RouteItem />
                        <RouteItem />
                        <RouteItem />
                        <RouteItem />
                        <RouteItem />
                    </View>
                </ScrollView>
            </View>
        );
    }
}
export default Tracking;
