
import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, BackAndroid, TextInput, ActivityIndicator, AsyncStorage, BackHandler, Alert, Image} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import MIcon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import MaIcon from 'react-native-vector-icons/dist/MaterialIcons';
import SIcon from 'react-native-vector-icons/dist/SimpleLineIcons';
import FIcon from 'react-native-vector-icons/dist/Feather';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://ppt.azarinpro.info';
Axios.defaults.baseURL = url;
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
import Loader from '../../components/loader'
import PlaceFooter from "../../components/placeFooter";
import SlideShow from "../../components/slideShow";
import homeIcon from "../../assets/homeIcon.png";
import CommentItem from '../../components/commentItem'
import user from '../../assets/user.png'

class Profile extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: true,
            userSkills: []

        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);

    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        Axios.get('/core/skill/user-skill?id='+this.props.user.id).then(response => {
            console.log('id', this.props.user)
            this.setState({userSkills: response.data.data, loading: false});
            console.log('response.data.data uuuuser skills', response.data)
        })
            .catch((error) => {
                Alert.alert('', 'خطایی رخ داده لطفا مجددا سعی نمایید')
                this.setState({loading: false});
                console.log('errore  user skilll show',error.response);
            });

    }
    render() {
        const {user} = this.props;
        const photos = [

        ];
        if(this.state.loading){
            return (<Loader />)
        } else return (
            <View style={styles.container}>
                <View style={styles.top}>
                    <TouchableOpacity onPress={() => this.onBackPress()}>
                        <FIcon name="arrow-left" size={23} color="gray" />
                    </TouchableOpacity>
                    <Text style={[styles.title, {paddingLeft: '31%'}]}>پروفایل کاربر</Text>
                </View>
                <View style={styles.topProfile}>
                    <View style={styles.topHeader}>
                        <View style={styles.imageContainer}>
                            <Image style={styles.image} source={{uri: this.props.user.attachments ? "http://ppt.azarinpro.info/files?uid="+this.props.user.attachments.uid : "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTgJ_xjHIEHFLZfUV7lfksCLV0oMFtV6dDeTYNC9WUVFYiKRYOvNQ"}} />
                        </View>
                        <Text style={styles.title}>{this.props.user.fname} {this.props.user.lname}</Text>
                        {/*<Text style={styles.subTitle}>عضو همیار</Text>*/}
                    </View>
                    <TouchableOpacity onPress={() => Actions.profileEdit()} style={styles.travelContainer}>
                        <Text style={styles.travelTxt}>ویرایش پروفایل</Text>
                        <FIcon name="edit" size={20} color="white" />
                    </TouchableOpacity>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.body}>
                        <View style={styles.navitemContainer}>
                            {/*<TouchableOpacity onPress={() => this.handlePress('home')}>*/}
                                {/*<View style={styles.navContainer}>*/}
                                    {/*<Text style={[styles.text, {color: 'black', fontSize: 14}]}>89</Text>*/}
                                    {/*<Text style={[styles.text, {color: 'rgb(50, 50, 50)'}]}>تعداد ثبت</Text>*/}
                                {/*</View>*/}
                            {/*</TouchableOpacity>*/}
                            <TouchableOpacity style={{marginRight: 20}} onPress={() => this.handlePress('home')}>
                                <View style={styles.navContainer}>
                                    <Text style={[styles.text, {color: 'black', fontSize: 14}]}>{this.props.user.following}</Text>
                                    <Text style={[styles.text, {color: 'rgb(50, 50, 50)'}]}>دنبال شده ها</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => this.handlePress('home')}>
                                <View style={styles.navContainer}>
                                    <Text style={[styles.text, {color: 'black', fontSize: 14}]}>{this.props.user.follower}</Text>
                                    <Text style={[styles.text, {color: 'rgb(50, 50, 50)'}]}>دنبال کننده ها</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        {
                            this.state.userSkills.length !== 0 ?
                                <View style={{width: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                    <Text style={[styles.text, {color: 'black', fontSize: 12, paddingTop: 15, paddingBottom: 15, alignSelf: 'flex-end'}]}>مهارت ها</Text>
                                    <ScrollView style={{
                                        transform: [
                                            { scaleX: -1},
                                        ],
                                    }}
                                                horizontal={true} showsHorizontalScrollIndicator={false}
                                    >
                                        <View style={styles.navitemContainer}>
                                            {
                                                this.state.userSkills.length !== 0 &&
                                                this.state.userSkills.map((item ,index)=> {
                                                    return <TouchableOpacity key={index} onPress={() => null}>
                                                        <View style={[styles.navContainerH]}>
                                                            <Text style={[styles.text, {color: 'black'}]}>{item.skill.title}</Text>
                                                            <MIcon name="star-box" size={15} color="black" />
                                                        </View>
                                                    </TouchableOpacity>
                                                })
                                            }
                                        </View>
                                    </ScrollView>
                                </View> : null

                        }
                        <Text style={[styles.text, {color: 'black', fontSize: 12, paddingTop: 15}]}>اطلاعات تماس {this.props.user.fname} {this.props.user.lname}</Text>
                        <View style={styles.infoContainer}>
                            <View style={styles.infoRow}>
                                <Text style={[styles.text, {color: 'black', paddingRight: 10}]}>{this.props.user.mobile}</Text>
                                <Icon name="phone" size={18} color="rgb(63, 175, 40)" />
                            </View>
                            <View style={styles.infoRow}>
                                <Text style={[styles.text, {color: 'black', paddingRight: 10}]}>{this.props.user.country ? this.props.user.country.title : null} - {this.props.user.state ? this.props.user.state.title : null} - {this.props.user.city ? this.props.user.city.title : null}</Text>
                                <MaIcon name="location-on" size={18} color="rgb(63, 175, 40)" />
                            </View>
                            {/*<View style={styles.infoRow}>*/}
                                {/*<Text style={[styles.text, {color: 'black', paddingRight: 10}]}>فکس: 02166785412</Text>*/}
                                {/*<Icon name="fax" size={16} color="rgb(63, 175, 40)" />*/}
                            {/*</View>*/}
                            <View style={styles.infoRow}>
                                <Text style={[styles.text, {color: 'black', paddingRight: 10}]}>{this.props.user.email}</Text>
                                <Icon name="envelope" size={16} color="rgb(63, 175, 40)" />
                            </View>
                        </View>
                    </View>
                </ScrollView>
                {/*<PlaceFooter active="comment"/>*/}
            </View>
        );
    }
}
function mapStateToProps(state) {
    return {
        user: state.auth.user,
    }
}
export default connect(mapStateToProps)(Profile);
