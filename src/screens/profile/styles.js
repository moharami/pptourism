
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(244, 244, 244)'
    },
    scroll: {
        marginTop: 30,
        paddingRight: 20,
        paddingLeft: 20,
        // position: 'absolute',
        // top: 190,
        // bottom: 60,
        width: '100%',
        paddingBottom: 50
    },
    body: {
        paddingTop: 10,
        width: '100%',
        paddingBottom: 100
    },
    navitemContainer: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'flex-end',
        width: '100%',
    },
    navContainer: {
        alignItems: "center",
        justifyContent: 'center',
        width: '100%',
        height: 50,
        // marginLeft: 20,
        backgroundColor: 'white',
        padding: 20,
        paddingTop: 35,
        paddingBottom: 35,

    },
    navContainerH: {
        alignItems: "center",
        justifyContent: 'center',
        height: 50,
        // marginLeft: 20,
        backgroundColor: 'white',
        padding: 20,
        // paddingTop: 35,
        // paddingBottom: 35,

        flexDirection: 'row',
        width: 150,
        marginRight: 20,
        transform: [
            {rotateY: '180deg'},
        ],
    },
    text: {
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black'
    },
    top: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'flex-start',
        padding: 15,
        backgroundColor: 'white',
        width: '100%'
    },
    contentBody: {
        width: '100%',
        // backgroundColor:'red',
        flexDirection: 'row',
        marginBottom: 20,
        alignItems: "center",
        justifyContent: 'center',
    },
    content: {
        // backgroundColor: 'white',
        // paddingRight: 30,
        width: '60%',
        paddingTop: 15,

    },
    placeTxt: {
        fontSize: 11,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'gray'
    },
    location: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: '#42b72a'
    },
    contentTxt: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'gray'
    },
    image: {
        width: 100,
        height: 100
    },
    topProfile: {
        backgroundColor: 'white',
        alignItems: "center",
        justifyContent: 'center',
        padding: 8,
        paddingBottom: 30
    },
    topHeader: {

    },
    title: {
        paddingRight: 10,
        fontSize: 14,
        color: 'black',
        textAlign: 'center',
        paddingTop: 10

    },
    subTitle: {
        paddingRight: 10,
        fontSize: 12,
        color: 'rgb(50, 50, 50)',
        textAlign: 'center'
    },
    travelContainer: {
       backgroundColor: 'rgb(63, 175, 40)',
        padding: 10,
        paddingRight: 20,
        paddingLeft: 20,
        flexDirection: 'row',
        position: 'absolute',
        bottom: -20,
        borderRadius: 5
    },
    travelTxt: {
        paddingRight: 15,
        fontSize: 14,
        color: 'white'
    },
    infoContainer: {
        backgroundColor: 'white',
        padding: 15,
        marginRight: 15,
        marginLeft: 15,
    },
    infoRow: {
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingTop: 10
    }
});
