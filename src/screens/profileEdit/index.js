/**
 * Created by n.moharami on 4/29/2019.
 */
import React, {Component} from 'react';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://ppt.azarinpro.info';
Axios.defaults.baseURL = url;
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
import Loader from '../../components/loader'
import AppHeader from "../../components/appHeader";
import ImagePicker from 'react-native-image-picker';
import Icon from 'react-native-vector-icons/dist/Feather';
import FIcon from 'react-native-vector-icons/dist/FontAwesome';
import AIcon from 'react-native-vector-icons/dist/AntDesign';
import MIcon from 'react-native-vector-icons/dist/MaterialIcons';
import ModalFilterPicker from 'react-native-modal-filter-picker'
import Selectbox from 'react-native-selectbox'
import SwitchButton from 'switch-button-react-native';
import MapView, { Marker } from 'react-native-maps';
import marker2 from '../../assets/marker.png'

import {
    View,
    BackHandler,
    ScrollView,
    Text,
    TouchableOpacity,
    ImageBackground,
    TextInput,
    Alert,
    AsyncStorage,
    Image
} from "react-native";

class ProfileEdit extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            fname: this.props.user.fname,
            lname: this.props.user.lname,
            mobile: this.props.user.mobile === null ? '':  this.props.user.mobile,
            email:  this.props.user.email,
            username:  this.props.user.display_name,
            password: '',
            new_password: '',
            password_confirmation: '',
            passChange: false,
            address: '',
            province: this.props.user && this.props.user.state ? this.props.user.state.id : 0 ,
            provinceTitle: this.props.user && this.props.user.state ? this.props.user.state.title : '',
            town: this.props.user && this.props.user.city ? this.props.user.city.id : 0,
            townTitle: this.props.user && this.props.user.city ? this.props.user.city.title : '',
            country: this.props.user && this.props.user.country ? this.props.user.country.id : 0 ,
            countryTitle: this.props.user && this.props.user.country ? this.props.user.country.title : '',
            countries: [],
            provinces: [],
            city: [],
            skillsData: [],
            userSkills: [],
            visibleinput1: false,
            visibleinput2: false,
            visibleinput3: false,
            visibleinput4: false,
            skills: {key: 0, label: "افزودن مهارت", value: 0},
            skillsTitle: '',
            loading: true,
            loading1: false,
            loading2: false,
            loading3: false,
            loading4: false,
            src: null,
            markers: [],
            lat: null,
            lng: null,
            color: 'rgb(227, 5, 26)',
            activeSwitch: 1,
            initialLat: this.props.user.latitude === null ? 35.715298 : this.props.user.latitude,
            initialLng: this.props.user.longitude === null ? 51.404343 : this.props.user.longitude,
            skillAdding: []
        };
        this.onBackPress = this.onBackPress.bind(this);
    }

    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        this.setState({loading1: true, loading4: true});
        Axios.get('/country/country-data').then(response => {
            this.setState({countries: response.data, loading1: false, loading: true});
            AsyncStorage.getItem('token').then((info) => {
                if(info !== null) {

                    // // this wiiiiiiiiil be removed
                    // Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;
                    // Axios.get('/core/user').then(response=> {
                    //     store.dispatch({type: 'USER_INFO_FETCHED', payload: response.data.data});
                    // })
                    //     .catch((error) => {
                    //         Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                    //         this.setState({loading: false});
                    //     });
                    //
                    // ////////////////////


                    const newInfo = JSON.parse(info);
                    console.log('token', newInfo.token)
                    Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;
                    Axios.get('/core/skill/show').then(response => {
                        this.setState({skillsData: response.data.data, loading4: false});
                        Axios.get('/core/skill/user-skill?id='+this.props.user.id).then(response => {
                            console.log('id', this.props.user)
                            this.setState({userSkills: response.data.data, loading: false});
                            console.log('response.data.data uuuuser skills', response.data)
                        })
                            .catch((error) => {
                                Alert.alert('', 'خطایی رخ داده لطفا مجددا سعی نمایید')
                                this.setState({loading: false});
                                console.log('errore  user skilll show',error.response);
                            });
                        console.log('response.data.data skills', response.data)
                    })
                        .catch((error) => {
                            Alert.alert('', 'خطایی رخ داده لطفا مجددا سعی نمایید')
                            this.setState({loading4: false});
                            console.log('errore skilll show',error.response);
                        });
                    console.log('response.data.data countrie', response.data)
                }
                else {
                    console.log('it is not login')
                }
            });
        })
            .catch((error) => {
                Alert.alert('', 'خطایی رخ داده لطفا مجددا سعی نمایید')
                this.setState({loading1: false});
                console.log(error);
            });





    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
        navigator.geolocation.clearWatch(this.watchID);
    }
    selectPhotoTapped() {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true,

            }
        };
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };

                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.setState({src: response.uri})
            }
        });
    }
    getState(id) {
        this.setState({loading2:true});
        Axios.get('/country/states-data/'+id).then(response => {
            this.setState({provinces: response.data, loading2: false});
            console.log('states', response.data);

        })
            .catch((error) =>{
            Alert.alert('', 'خطایی رخ داده لطفا مجددا سعی نمایید')
                this.setState({loading2: false});
            });
    }
    getCities(id) {
        // console.log('provinceTitle', this.state.citTitle)
        this.setState({loading3:true});
        Axios.get('/country/cities-data/'+id).then(response => {
            this.setState({city: response.data.city});
            console.log('cities', response.data);
            Axios.get('https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input='+this.state.provinceTitle+'&inputtype=textquery&fields=geometry&key=AIzaSyACjNKR5CCk0nGL5uBGm2IME_c6FfML9eg').then(response => {
                console.log('ressssssssss map', response)
                this.setState({loading3: false, initialLat: response.data.candidates[0].geometry.location.lat, initialLng: response.data.candidates[0].geometry.location.lng});
                // console.log('MMMMAPPPPP', 'https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input='+ this.state.provinceTitle+'&inputtype=textquery&fields=geometry&key=AIzaSyACjNKR5CCk0nGL5uBGm2IME_c6FfML9eg');
                console.log('initialLat', this.state.initialLat);
                console.log('initialLat', this.state.initialLng);

            })
                .catch((error) =>{
                    // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                    // this.setState({loading2: false});
                    console.log(error)
                    this.setState({modalVisible: true, loading2: false});

                });
        })
            .catch((error) =>{
                Alert.alert('', 'خطایی رخ داده لطفا مجددا سعی نمایید')
                this.setState({loading3: false});
            });
    }
    onShowinput1 = () => {
        this.setState({ visibleinput1: true });
    }

    onSelectInput1 = (picked) => {
        console.log('picked', picked)
        this.setState({
            country: picked,
            visibleinput1: false
        }, () =>{
            console.log('ppppicked', picked)
            this.state.countries.map((item) => {
                if(picked === item.id) {
                    this.setState({countryTitle: item.title}, () => {this.getState(picked);console.log(this.state.countryTitle)});
                }
            })
        })
    }
    onCancelinput1 = () => {
        this.setState({
            visibleinput1: false
        })
    }
    onShowinput2 = () => {
        this.setState({ visibleinput2: true });
    }

    onSelectInput2 = (picked) => {
        console.log('picked', picked)
        this.setState({
            province: picked,
            visibleinput2: false
        }, () =>{
            console.log('ppppicked', picked)
            this.state.provinces.map((item) => {
                if(picked === item.id) {
                    this.setState({provinceTitle: item.title}, () => {this.getCities(picked);console.log(this.state.provinceTitle)});
                }
            })
        })
    }
    onCancelinput2 = () => {
        this.setState({
            visibleinput2: false
        })
    }
    onShowinput3 = () => {
        this.setState({ visibleinput3: true });
    }
    onSelectInput3 = (picked) => {
        console.log('picked', picked)
        this.setState({
            town: picked,
            visibleinput3: false
        }, () =>{
            console.log('ppppicked', picked)
            this.state.city.map((item) => {
                if(picked === item.id) {
                    this.setState({townTitle: item.title}, () => {console.log(this.state.townTitle)});
                }
            })
        })
    }
    onCancelinput3 = () => {
        this.setState({
            visibleinput3: false
        })
    }
    handlePress(e) {
        console.log('map', e)
        this.setState({
            markers: [
                {
                    coordinate: e.coordinate,
                }
            ],
            lat: e.coordinate.latitude,
            lng: e.coordinate.longitude
        })
    }

    updateInfo() {
        if(this.state.passChange && (this.state.password === '' || this.state.new_password === '' || this.state.password_confirmation === '') ) {
            Alert.alert('', 'لطفا تمامی اطلاعات مربوط به تغییر پسورد را صحیح وارد نمایید')
        }
        if(this.state.passChange && (this.state.new_password !== '' && this.state.password_confirmation !== '' && this.state.new_password !== this.state.password_confirmation) ) {
            Alert.alert('', 'تکرار گذرواژه تطابق ندارد')
        }
        else {
            this.setState({loading: true});
            let formdata = new FormData();
            this.state.fname !== '' && formdata.append("fname", this.state.fname)
            this.state.lname !== '' && formdata.append("lname", this.state.lname)
            this.state.mobile !== '' && formdata.append("mobile", this.state.mobile)
            this.state.username !== '' && formdata.append("display_name", this.state.username)
            // formdata.append("password", this.state.password)
            this.state.email !== '' && formdata.append("email", this.state.email)
            this.state.country !== 0 && formdata.append("country_id", this.state.country)
            this.state.town !== 0 && formdata.append("city_id", this.state.town)
            this.state.province !== 0 && formdata.append("state_id", this.state.province)

            if(this.state.lat !== null && this.state.lng !== null) {
                formdata.append("latitude", this.state.lat)
                formdata.append("longitude", this.state.lng)
            }
            if(this.state.passChange) {
                formdata.append("password", this.state.password)
                formdata.append("new_password", this.state.new_password)
                formdata.append("password_confirmation", this.state.password_confirmation)
            }
            if(this.state.skillAdding.length !== 0) {
                let usSK = [];
                this.state.userSkills.length !==0 && this.state.userSkills.map((item)=> {
                    usSK.push(item.skill.id)
                })
                let skAD = [];
                this.state.skillAdding.map((item) => {
                    skAD.push(item.id)
                })
                // let newusSK= usSK.concat(this.state.skillAdding);// this will be filter
                let newusSK= usSK.concat(skAD);
                let deduped = newusSK.filter(function (sandwich, index) {
                    return newusSK.indexOf(sandwich) === index;
                });
                console.log('newusSK', newusSK)
                console.log('usSK', usSK)
                console.log('deduuuuped', deduped)
                deduped.map((item) => {
                    formdata.append("skill[]", item)
                })
            }
            if(this.state.src !== null) {
                formdata.append("type", "image");
                formdata.append("file", {
                    uri: this.state.src,
                    type: 'image/jpeg',
                    name: 'file'
                });
            }
            console.log('formdata formdata', formdata)

            Axios.post('/core/update', formdata
            ).then(response=> {
                this.setState({loading: false});
                Alert.alert('','پروفایل شما با موفقیت تغییر یافت');
                console.log('update response', response.data);
            })
                .catch((response) => {
                    if(response.response.data.message === 'Unauthenticated.'){
                        this.setState({
                            loading: false
                        }, () => {
                            Alert.alert('','لطفا ابتدا وارد شوید');
                            this.setState({loading: false});
                        })
                    }
                    else {
                        Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                        this.setState({loading: false});

                    }
                });
        }
    }
    render() {
        const {user} = this.props;
        let items = [];
        items.push({key: 0, label: 'افزودن مهارت', value: 0})
        // skillsArray.map((item, index) => {items.push({key: index, label: item, value: index})})
        this.state.skillsData.length !== 0 && this.state.skillsData.map((item, index) => {items.push({key: index+1, label: item.title, value: item.id})});

        if(this.state.loading){
            return (<Loader />)
        } else return (
            <View style={styles.container}>
                <AppHeader pageTitle="ویرایش پروفایل" icon={false}  edit={true} updateInfo={() => this.updateInfo()} />
                <ScrollView style={styles.scroll}>
                    <View style={styles.body}>
                        <ImageBackground style={styles.Image} source={{uri: this.state.src === null ? (this.props.user.attachments ? "http://ppt.azarinpro.info/files?uid="+this.props.user.attachments.uid : "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTgJ_xjHIEHFLZfUV7lfksCLV0oMFtV6dDeTYNC9WUVFYiKRYOvNQ")  : this.state.src}}>
                            <TouchableOpacity onPress={() => this.selectPhotoTapped()} style={{
                                backgroundColor:'rgba(0,0,0,.6)',
                                height: 100,
                                width: 110,
                                alignItems: "center",
                                justifyContent: "center",
                                borderRadius: 6
                            }}>
                                {/*<View style={styles.addImage}>*/}
                                    <Icon name="edit" size={20} color="white"/>
                                {/*</View>*/}
                            </TouchableOpacity>
                        </ImageBackground>
                        <Text style={styles.addText}>ویرایش عکس پروفایل</Text>
                        <Text style={styles.text}>نام</Text>
                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginBottom: 20, marginTop: 5, borderRadius: 7 }}>
                            <TextInput
                                maxLength={15}
                                placeholder={'نام'}
                                placeholderTextColor={'gray'}
                                underlineColorAndroid='transparent'
                                value={this.state.fname}
                                style={{
                                    height: 40,
                                    paddingRight: 15,
                                    paddingLeft: 15,
                                    width: '100%',
                                    color: 'gray',
                                    fontSize: 14,
                                }}
                                onChangeText={(text) => {this.setState({fname: text})}}
                            />
                        </View>

                        <Text style={styles.text}>نام خانوادگی</Text>
                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginTop: 5, marginBottom: 10, borderRadius: 7}}>
                            <TextInput
                                maxLength={15}
                                placeholder={'نام خانوادگی'}
                                placeholderTextColor={'gray'}
                                underlineColorAndroid='transparent'
                                value={this.state.lname}
                                style={{
                                    height: 40,
                                    paddingRight: 15,
                                    paddingLeft: 15,
                                    width: '100%',
                                    color: 'gray',
                                    fontSize: 14
                                }}
                                onChangeText={(text) => {this.setState({lname: text})}}
                            />
                        </View>
                        <Text style={styles.text}>موبایل</Text>
                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginTop: 5, marginBottom: 10, borderRadius: 7}}>
                            <TextInput
                                maxLength={15}
                                placeholder={'موبایل'}
                                keyboardType={'numeric'}
                                placeholderTextColor={'gray'}
                                underlineColorAndroid='transparent'
                                value={this.state.mobile}
                                style={{
                                    height: 40,
                                    paddingRight: 15,
                                    paddingLeft: 15,
                                    width: '100%',
                                    color: 'gray',
                                    fontSize: 14
                                }}
                                onChangeText={(text) => {this.setState({mobile: text})}}
                            />
                        </View>
                        <Text style={styles.text}>نام کاربری</Text>
                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginTop: 5, marginBottom: 10, borderRadius: 7}}>
                            <TextInput
                                maxLength={15}
                                placeholder={'نام کاربری'}
                                placeholderTextColor={'gray'}
                                underlineColorAndroid='transparent'
                                value={this.state.username}
                                style={{
                                    height: 40,
                                    paddingRight: 15,
                                    paddingLeft: 15,
                                    width: '100%',
                                    color: 'gray',
                                    fontSize: 14
                                }}
                                onChangeText={(text) => {this.setState({username: text})}}
                            />
                        </View>
                        <Text style={styles.text}>کشور</Text>
                        <TouchableOpacity onPress={this.onShowinput1} style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginTop: 5, marginBottom: 10, borderRadius: 7}}>
                            <Icon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                            <Text style={{paddingTop: 5, paddingRight: 10,  fontFamily: 'IRANSansMobile(FaNum)', color: 'gray' }}>{this.state.loading1 ? "در حال بارگذاری..." : this.state.country === 0 ? "انتخاب نشده" : this.state.countryTitle}</Text>
                            <ModalFilterPicker
                                visible={this.state.visibleinput1}
                                onSelect={this.onSelectInput1}
                                onCancel={this.onCancelinput1}
                                options={ this.state.countries.map((item) => {return {key: item.id, label: item.title, value: item.id}})}
                                placeholderText="جستجو ..."
                                cancelButtonText="لغو"
                                // filterTextInputStyle={{textAlign: 'right'}}
                                optionTextStyle={{width: '100%'}}
                                titleTextStyle={{textAlign: 'right'}}
                            />
                        </TouchableOpacity>

                        <Text style={styles.text}>استان</Text>
                        <TouchableOpacity onPress={this.onShowinput2} style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginTop: 5, marginBottom: 10, borderRadius: 7}}>
                            <Icon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                            <Text style={{paddingTop: 5, paddingRight: 10,  fontFamily: 'IRANSansMobile(FaNum)', color: 'gray' }}>{this.state.loading2 ? "در حال بارگذاری..." : this.state.province === 0 ? "انتخاب نشده" : this.state.provinceTitle}</Text>
                            <ModalFilterPicker
                                visible={this.state.visibleinput2}
                                onSelect={this.onSelectInput2}
                                onCancel={this.onCancelinput2}
                                options={ this.state.provinces.map((item) => {return {key: item.id, label: item.title, value: item.id}})}
                                placeholderText="جستجو ..."
                                cancelButtonText="لغو"
                                // filterTextInputStyle={{textAlign: 'right'}}
                                optionTextStyle={{width: '100%'}}
                                titleTextStyle={{textAlign: 'right'}}
                            />
                        </TouchableOpacity>

                        <Text style={styles.text}>شهر</Text>
                        <TouchableOpacity onPress={this.onShowinput3} style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginTop: 5, marginBottom: 10, borderRadius: 7}}>
                            <Icon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                            <Text style={{paddingTop: 5, paddingRight: 10,  fontFamily: 'IRANSansMobile(FaNum)', color: 'gray' }}>{this.state.loading3 ? "در حال بارگذاری..." : this.state.town === 0 ? "انتخاب نشده" : this.state.townTitle}</Text>
                            <ModalFilterPicker
                                visible={this.state.visibleinput3}
                                onSelect={this.onSelectInput3}
                                onCancel={this.onCancelinput3}
                                options={ this.state.city.map((item) => {return {key: item.id, label: item.title, value: item.id}})}
                                placeholderText="جستجو ..."
                                cancelButtonText="لغو"
                                // filterTextInputStyle={{textAlign: 'right'}}
                                optionTextStyle={{width: '100%'}}
                                titleTextStyle={{textAlign: 'right'}}
                            />
                        </TouchableOpacity>
                        {
                            this.state.userSkills.length !== 0 ?
                                <View style={{width: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                    <Text style={[styles.text, {paddingBottom: 10}]}>مهارت ها</Text>
                                    <ScrollView style={{
                                        transform: [
                                            { scaleX: -1},
                                            // {  rotate: '180deg'},

                                        ],
                                    }}
                                                horizontal={true} showsHorizontalScrollIndicator={false}
                                    >
                                        <View style={styles.navitemContainer}>
                                            {
                                                this.state.userSkills.length !== 0 &&
                                                this.state.userSkills.map((item ,index)=> {
                                                    return <TouchableOpacity onPress={() => Actions.home()} key={index}>
                                                        <View style={[styles.navContainerH]}>
                                                            {/*<Image source={{uri: "http://fitclub.ws/files?uid="+item.attachments[0].uid+"&width=104&height=92" }} style={{ width: 50, height: 50, tintColor: this.state.active === item.id ? 'red': 'gray'}} />*/}
                                                            <Text style={[styles.lebel, {color: 'white', paddingRight: 8}]}>{item.skill.title}</Text>
                                                            <MIcon name="work" size={15} color="white" />
                                                        </View>
                                                    </TouchableOpacity>
                                                })
                                            }
                                        </View>
                                    </ScrollView>
                                </View> : null

                        }
                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginTop: 20, marginBottom: 10, borderRadius: 7}}>
                            {/*<Icon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>*/}
                            <Selectbox
                                style={{height: 42, paddingTop: '3%', paddingRight: 10}}
                                selectLabelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingTop: 0}}
                                optionLabelStyle={{textAlign: 'center', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                selectedItem={this.state.skills}
                                cancelLabel="لغو"
                                onChange={(itemValue) =>{
                                    this.setState({
                                        skills: itemValue
                                    }, () => {
                                        let skillArray = this.state.skillAdding;
                                        skillArray.push({id: itemValue.value, title: itemValue.label})
                                        this.setState({skillAdding: skillArray, skillsTitle: itemValue.label}, () => {console.log(this.state.skillsTitle)});
                                    })}}
                                items={items} />
                        </View>

                        {
                            this.state.skillAdding.length !== 0 ?
                                <View style={{width: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                    <Text style={[styles.text, {paddingBottom: 10}]}>مهارت های افزوده شده</Text>
                                    <ScrollView style={{
                                        transform: [
                                            { scaleX: -1},
                                            // {  rotate: '180deg'},

                                        ],
                                    }}
                                                horizontal={true} showsHorizontalScrollIndicator={false}
                                    >
                                        <View style={styles.navitemContainer}>
                                            {
                                                this.state.skillAdding.length !== 0 &&
                                                this.state.skillAdding.map((item ,index)=> {
                                                    return <TouchableOpacity onPress={() => this.handlePress('home')} key={index}>
                                                        <View style={[styles.navContainerH, {backgroundColor: '#4682B4'}]}>
                                                            {/*<Image source={{uri: "http://fitclub.ws/files?uid="+item.attachments[0].uid+"&width=104&height=92" }} style={{ width: 50, height: 50, tintColor: this.state.active === item.id ? 'red': 'gray'}} />*/}
                                                            <Text style={[styles.lebel, {color: 'white', paddingRight: 8}]}>{item.title}</Text>
                                                            <AIcon name="pluscircleo" size={15} color="white" />
                                                        </View>
                                                    </TouchableOpacity>
                                                })
                                            }
                                        </View>
                                    </ScrollView>
                                </View> : null

                        }

                        <View style={styles.question}>
                            <SwitchButton
                                onValueChange={(val) => this.setState({ activeSwitch: val, passChange: !this.state.passChange, color: this.state.color === 'rgb(227, 5, 26)' ? 'rgb(32, 193, 189)': 'rgb(227, 5, 26)' })}
                                text1 = 'خیر'
                                text2 = 'بله'
                                switchWidth = {78}
                                switchHeight = {38}
                                switchdirection = 'ltr'
                                switchBorderRadius = {100}
                                switchSpeedChange = {500}
                                switchBorderColor = '#d4d4d4'
                                switchBackgroundColor = '#fff'
                                btnBorderColor = 'transparent'
                                btnBackgroundColor = {this.state.color}
                                fontColor = '#b1b1b1'
                                activeFontColor = '#fff'
                            />
                            <Text style={[styles.questionText, {width:'75%'}]}>مایل به تغییر پسورد هستید؟ </Text>
                        </View>
                        {
                            this.state.passChange ?
                                <View style={{width: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                    {/*<Text style={styles.text}>کلمه عبور</Text>*/}
                                    <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginTop: 5, marginBottom: 10, borderRadius: 7}}>
                                        <TextInput
                                            maxLength={15}
                                            placeholder={'کلمه عبور فعلی'}
                                            secureTextEntry
                                            placeholderTextColor={'gray'}
                                            underlineColorAndroid='transparent'
                                            value={this.state.password}
                                            style={{
                                                height: 40,
                                                paddingRight: 15,
                                                width: '100%',
                                                color: 'gray',
                                                fontSize: 14,
                                                textAlign: 'right'
                                            }}
                                            onChangeText={(text) => {this.setState({password: text})}}
                                        />
                                    </View>
                                    <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginTop: 5, marginBottom: 10, borderRadius: 7}}>
                                        <TextInput
                                            maxLength={15}
                                            placeholder={'کلمه عبور جدید'}
                                            secureTextEntry
                                            placeholderTextColor={'gray'}
                                            underlineColorAndroid='transparent'
                                            value={this.state.new_password}
                                            style={{
                                                height: 40,
                                                paddingRight: 15,
                                                width: '100%',
                                                color: 'gray',
                                                fontSize: 14,
                                                textAlign: 'right'
                                            }}
                                            onChangeText={(text) => {this.setState({new_password: text})}}
                                        />
                                    </View>
                                    <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginTop: 5, marginBottom: 20, borderRadius: 7}}>
                                        <TextInput
                                            maxLength={15}
                                            placeholder={'تکرار کلمه عبور جدید'}
                                            secureTextEntry
                                            placeholderTextColor={'gray'}
                                            underlineColorAndroid='transparent'
                                            value={this.state.password_confirmation}
                                            style={{
                                                height: 40,
                                                paddingRight: 15,
                                                width: '100%',
                                                color: 'gray',
                                                fontSize: 14,
                                                textAlign: 'right'
                                            }}
                                            onChangeText={(text) => {this.setState({password_confirmation: text})}}
                                        />
                                    </View>
                                    {/*<TouchableOpacity onPress={() => null} style={styles.advertise}>*/}
                                        {/*<Text style={styles.buttonTitle}>تایید</Text>*/}
                                    {/*</TouchableOpacity>*/}
                                </View>
                                : null
                        }
                        <Text style={[styles.questionText, {paddingBottom: 20}]}>آدرس خود را بر روی نقشه پیدا کنید و کافی است فقط روی نقشه کلیک کنید.</Text>
                        <MapView
                            style={styles.mapStyle}
                            region={{
                                latitude: this.state.initialLat,
                                longitude: this.state.initialLng,
                                latitudeDelta: 0.0922,
                                longitudeDelta: 0.0421,
                            }}
                            onPress={(e) =>this.handlePress(e.nativeEvent)}
                        >
                            {this.state.markers.map((marker, index) => {
                                return (
                                    <Marker key={index} {...marker} >
                                        <Image source={marker2} style={{width: 50, height: 50, resizeMode: 'contain'}} />
                                    </Marker>
                                )
                            })}
                        </MapView>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
function mapStateToProps(state) {
    return {
        user: state.auth.user,
    }
}
export default connect(mapStateToProps)(ProfileEdit);
