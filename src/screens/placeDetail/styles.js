
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(244, 244, 244)'
    },
    scroll: {
        paddingTop: 0,
        paddingRight: 20,
        paddingLeft: 20,
        // position: 'absolute',
        // top: 190,
        // bottom: 60,
        width: '100%',
        paddingBottom: 50
    },
    body: {
        paddingTop: 10,
        width: '100%',
        paddingBottom: 100
    },
    navitemContainer: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-between',
        paddingTop: 45,
        paddingBottom: 30,
        // paddingRight: 20,
        height: 120,
        width: '100%',
    },
    navContainer: {
        alignItems: "center",
        justifyContent: 'center',
        width: 64,
        height: 56,
        // marginLeft: 20,
        backgroundColor: 'white'
    },
    text: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black'
    },
    top: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'flex-start',
        padding: 15,
        backgroundColor: 'white',
        width: '100%'
    },
    contentBody: {
        width: '100%',
        // backgroundColor:'red',
        flexDirection: 'row',
        marginBottom: 20,
        alignItems: "flex-start",
        justifyContent: 'space-between',
    },
    weatherRow: {
        flexDirection: 'row',
        paddingTop: 5
    },
    weather: {
        width: '40%',
        // paddingTop: 15,
        alignItems: 'flex-start',
        justifyContent: 'flex-start',

    },
    content: {
        // backgroundColor: 'white',
        // paddingRight: 30,
        width: '60%',
        paddingTop: 15,
        alignItems: 'flex-end',
        justifyContent: 'center',
        alignSelf: 'flex-end'

    },
    button: {
        alignItems: "center",
        justifyContent: 'center',
        borderRadius: 7,
        borderColor: 'rgb(66, 183, 42)',
        borderWidth: 1,
        width: 70,
        marginLeft: 5
    },
    title: {
        fontSize: 13,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black'
    },
    placeTxt: {
        fontSize: 11,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'gray',
        textAlign: 'right'
    },
    infoRow: {
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingTop: 10
    },
    location: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: '#42b72a'
    },
    contentTxt: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'gray'
    }
});
