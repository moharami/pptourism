/**
 * Created by n.moharami on 4/18/2019.
 */
import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, BackAndroid, TextInput, ActivityIndicator, AsyncStorage, BackHandler, Share, Image} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import SIcon from 'react-native-vector-icons/dist/SimpleLineIcons';
import FIcon from 'react-native-vector-icons/dist/Feather';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://ppt.azarinpro.info';
Axios.defaults.baseURL = url;
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
import Loader from '../../components/loader'
import PlaceFooter from "../../components/placeFooter";
import SlideShow from "../../components/slideShow";
import weather from "../../assets/weather.png";
import moment_jalaali from 'moment-jalaali'
import HTML from 'react-native-render-html';
import InfoModal from './infoModal'

class PlaceDetail extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: false,
            homeActive: true,
            modalVisible: false,
            modalTitle: '',
            weather: {},
            eventValidator: false,
            placeValidator: false,
            events: [],
            places: [],
            travels: [],
            item: []
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    shareCode() {
        Share.share(
            {
                title:   "اشتراک گذاری پپیتوریسم" ,
                message: 'http://ppt.azarinpro.info/allbusiness',
                url: 'http://ppt.azarinpro.info/allbusiness',
            },
        )
    }
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        console.log('sssssssssssssssssssssssssssssssssubNamethis.props.subName', this.props.subName)

        // this.setState({loading: true });
        if(this.props.subName === 'شهر') {
            this.setState({loading: true });
            console.log('this.props.item.translations.title', this.props.item.translations[0].title)
            console.log('this.props.item', this.props.item)
            Axios.get('/country/weather?city='+this.props.item.translations[0].title).then(response => {
                this.setState({weather: response.data.data});
                console.log('response.data.data',  response.data.data)
                Axios.get('/country/city/single?id='+this.props.item.id).then(response => {
                    this.setState({loading: false, travels: response.data.data.travelogues});
                })
                    .catch((error) => {
                        console.log(error);
                        this.setState({loading: false});
                    });
            })
                .catch((error) => {
                    console.log(error.response);
                    this.setState({loading: false});
                });
        }
        else if(this.props.subName === 'استان') {
            this.setState({loading: true });
            Axios.get('/country/state/single?id='+this.props.item.id).then(response => {
                this.setState({loading: false, travels: response.data.data.travelogues});
            })
                .catch((error) => {
                    console.log(error);
                    this.setState({loading: false});
                });
        }
        else if(this.props.subName === 'کشور') {
            this.setState({loading: true });
            Axios.get('/country/single?id='+this.props.item.id).then(response => {
                this.setState({loading: false, travels: response.data.data.travelogues});
            })
                .catch((error) => {
                    console.log(error);
                    this.setState({loading: false});
                });
        }
        else if(this.props.subName === 'مکان') {
            console.log(' responsssse mmmmmmakkkkan inja makan ast',1)
            this.setState({loading: true });
            console.log('this.props.item.id', this.props.item.id)
            Axios.post('/business/single', {id: this.props.item.id}).then(response => {
                this.setState({loading: false, item: response.data.data});
                console.log(' responsssse mmmmmmakkkkan', response.data)
            })
                .catch((error) => {
                    console.log(error);
                    this.setState({loading: false});
                });
        }
        else if(this.props.subName === 'رویداد') {
            this.setState({loading: true });
            Axios.get('/country/event/show?id='+this.props.item.id).then(response => {
                this.setState({loading: false, item: response.data.data});
                console.log('response roydadewwwwwwwwwwwwwwwwwwwwwwwwwwwwww ', response.data.data)
            })
                .catch((error) => {
                    console.log(error);
                    this.setState({loading: false});
                });
        }
    }
    closeModal() {
        this.setState({modalVisible: false});
    }
    render() {
        console.log('item kkkkke mle jjjjashne ', this.props.item)
        const myHtml = '<p>' + this.props.item.content + '</p>';
        const photos = [

        ];
        if(this.state.loading){
            return (<Loader />)
        } else return (
            <View style={styles.container}>
                <View style={styles.top}>
                    <TouchableOpacity onPress={() => this.onBackPress()}>
                        <FIcon name="arrow-left" size={23} color="gray" />
                    </TouchableOpacity>
                </View>
                {/*<View>*/}
                    {/*<SlideShow activeSlide={1} />*/}
                {/*</View>*/}
                <ScrollView style={styles.scroll}>
                    <View style={styles.body}>
                        {/*<SlideShow activeSlide={1} images={this.props.item.attachments} item={this.props.item}/>*/}
                        <SlideShow activeSlide={1} images={this.props.item.attachments}/>
                        <View style={styles.contentBody}>
                            <View style={styles.weather}>
                                {
                                    this.props.subName === 'شهر' ?
                                        <View style={styles.weatherRow}>
                                            <Image source={weather} style={{width: 50, resizeMode: 'contain',}}/>
                                            <View style={{padding: 5}}>
                                                <View style={{flexDirection: 'row', position: 'relative', zIndex: 2}}>
                                                    <Text style={styles.title}>{Math.floor(this.state.weather.main.temp)}</Text>
                                                    <Text style={[styles.title, {paddingBottom: 30, position: 'absolute', zIndex: 3, top: -3, left: 15}]}>0c</Text>
                                                </View>
                                                <Text style={[styles.title, {fontSize: 12}]}>{this.state.weather.name} - {this.state.weather.weather[0].description}</Text>
                                            </View>
                                        </View> :  <Text style={[styles.contentTxt, {paddingTop: 20}]}>آخرین بروزرسانی: { moment_jalaali(this.props.item.updated_at, 'YYYY-MM-DD').format('jYYYY/jMM/jDD')}</Text>
                                }
                                {
                                    this.props.subName === 'شهر' ||  this.props.subName === 'استان' || this.props.subName === 'کشور' ?
                                        <View style={styles.weatherRow}>
                                            <TouchableOpacity onPress={() => Actions.travelogue({travels: this.state.travels, place: this.props.item.title, subName: this.props.subName, placeId: this.props.item.id, item: this.props.item})} style={styles.button}>
                                                <Text style={[styles.title, {color: 'rgb(66, 183, 42)', fontSize: 12}]}>سفرنامه ها</Text>
                                            </TouchableOpacity>
                                            {/*{*/}
                                            {/*this.state.eventValidator ?*/}
                                            {/*<TouchableOpacity onPress={() => Actions.events()} style={styles.button}>*/}
                                            {/*<Text style={[styles.title, {color: 'rgb(66, 183, 42)', fontSize: 12}]}>رویدادها</Text>*/}
                                            {/*</TouchableOpacity>*/}
                                            {/*: null*/}
                                            {/*}*/}
                                            {/*{*/}
                                            {/*this.state.placeValidator ?*/}
                                            {/*<TouchableOpacity onPress={() => Actions.places()} style={styles.button}>*/}
                                            {/*<Text style={[styles.title, {color: 'rgb(66, 183, 42)', fontSize: 12}]}>تمام مکان ها</Text>*/}
                                            {/*</TouchableOpacity>*/}
                                            {/*: null*/}
                                            {/*}*/}
                                        </View> : null
                                }
                            </View>
                            <View style={styles.content}>
                                <Text style={styles.title}>{this.props.item.title}</Text>
                                {
                                    this.props.fav !== undefined ?
                                        <Text style={styles.placeTxt}>تعداد رای :{this.props.fav} نفر</Text>
                                        :
                                        <Text style={styles.placeTxt}>{this.props.subName}</Text>
                                }
                                {/*<View style={styles.infoRow}>*/}
                                    {/*<Text style={styles.contentTxt}>توسط آرش انوشه</Text>*/}
                                    {/*<Text style={styles.location}>ایران- تهران . </Text>*/}
                                {/*</View>*/}
                                <View style={styles.infoRow}>
                                    {
                                        this.props.item.user ?
                                            <Text style={styles.contentTxt}>توسط {this.props.item.user.fname } {this.props.item.user.lname }</Text>
                                            :
                                            <Text style={styles.contentTxt}>  {this.props.item.created_at !== null ? moment_jalaali(this.props.item.created_at, 'YYYY-MM-DD').format('jYYYY/jMM/jDD') : null}</Text>
                                    }
                                    {
                                        this.props.item.country || this.props.item.city ?
                                            <Text style={styles.location}>{this.props.item.country ? this.props.item.country.title : null}- { this.props.item.city ? this.props.item.city.title : null} . </Text>
                                            : (
                                            this.props.item.city_name || this.props.item.country_name ?
                                                <Text style={styles.location}>{this.props.item.country_name ? this.props.item.country_name : null}{this.props.item.city_name && this.props.item.country_name ? '-' : null} { this.props.item.city_name ? this.props.item.city_name : null} . </Text>
                                                : null
                                        )
                                    }
                                </View>
                            </View>
                        </View>
                        {/*<Text style={[styles.placeTxt, {lineHeight: 20}]}>{this.props.item.content}</Text>*/}
                        {
                            this.props.item.content !== null ?
                                <HTML html={myHtml} tagsStyles={{
                                    p: {
                                        alignSelf: "flex-end",
                                        fontFamily: 'IRANSansMobile(FaNum)',
                                        color: 'rgba(51, 54, 64, 1)',
                                        lineHeight: 30,
                                        paddingBottom: 10,
                                        paddingTop: 20,
                                        fontSize: 14
                                    },
                                    img: {
                                        width: 300,
                                        height: 200,
                                        marginRight: 'auto',
                                        marginLeft: 'auto'
                                    }
                                }} />
                                : <Text> </Text>
                        }
                        <View style={styles.navitemContainer}>
                            {
                                this.props.subName === 'کشور' || this.props.subName === 'شهر'|| this.props.subName === 'استان'|| this.props.subName === 'روستا' ?
                                    <TouchableOpacity onPress={() => this.setState({modalTitle: 'تقویم', modalVisible: true })}>
                                        <View style={styles.navContainer}>
                                            <Icon name="calendar" size={18} color="#00BFFF" />
                                            <Text style={[styles.text, {color: 'black'}]}>تقویم</Text>
                                        </View>
                                    </TouchableOpacity> :
                                    <TouchableOpacity onPress={() => this.shareCode()}>
                                        <View style={styles.navContainer}>
                                            <FIcon name="share-2" size={18} color="#00BFFF" />
                                            <Text style={[styles.text, {color: 'black'}]}>اشتراک گذاری</Text>
                                        </View>
                                    </TouchableOpacity>
                            }
                            <TouchableOpacity onPress={() => Actions.tracking()}>
                                <View style={styles.navContainer}>
                                    <FIcon name="send" size={18} color="#00BFFF" />
                                    <Text style={[styles.text, {color: 'black'}]}>مسیریابی</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => Actions.locations()}>
                                <View style={styles.navContainer}>
                                    <FIcon name="map-pin" size={18} color="#00BFFF" />
                                    <Text style={[styles.text, {color: 'black'}]}>نقشه شهر</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => Actions.favorites()}>
                                <View style={styles.navContainer}>
                                    <SIcon name="pin" size={18} color="#00BFFF" />
                                    <Text style={[styles.text, {color: 'black'}]}>علاقه مندی</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <InfoModal
                            item={this.props.item}
                            subName={this.props.subName}
                            title={this.state.modalTitle}
                            closeModal={(title) => this.closeModal(title)}
                            modalVisible={this.state.modalVisible}
                        />
                    </View>
                </ScrollView>
                <PlaceFooter active="general" item={this.props.subName === 'مکان' || this.props.subName === 'رویداد' ? this.state.item :  this.props.item} fav={this.props.fav}  subName={this.props.subName} comments={this.props.subName === 'مکان' || this.props.subName === 'رویداد' ? this.state.item.comments :  [] } />
            </View>
        );
    }
}
export default PlaceDetail;
