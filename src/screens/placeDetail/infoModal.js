
import React, { Component } from 'react';
import {
    Modal,
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    ScrollView,
    Alert
}
    from 'react-native'
import Icon from 'react-native-vector-icons/dist/FontAwesome'
import FIcon from 'react-native-vector-icons/dist/Feather'
import Axios from 'axios';
export const url = 'http://ppt.azarinpro.info';
Axios.defaults.baseURL = url;
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
import moment_jalaali from 'moment-jalaali'
import PersianCalendarPicker from '../../components/persian_calender/src/index';
import {
    BallIndicator,
    SkypeIndicator,
} from 'react-native-indicators';
import {Actions} from 'react-native-router-flux'

class InfoModal extends Component {
    state = {
        text: '',
        modalVisible: false,
        loading: true,
        what: '',
        tags: [],
        tag: 0,
        active: '',
        events: [],
        eventsNum: [],
        selectedStartDate: null
    };
    componentWillMount() {
        if(this.props.subName === 'کشور' || this.props.subName === 'شهر'|| this.props.subName === 'استان'|| this.props.subName === 'روستا')  {
            let baseAdd = "";
            if(this.props.subName === 'کشور') {
                baseAdd = "/country/event/calender?country_id="+ this.props.item.id
            }
            else if(this.props.subName === 'شهر') {
                baseAdd = "/country/event/calender?country_id="+ this.props.item.state.country_id+"&city_id="+this.props.item.id
            }
            if(this.props.subName === 'استان') {
                baseAdd = "/country/event/calender?country_id="+ this.props.item.country_id+"&state_id="+this.props.item.id
            }
            if(this.props.subName === 'روستا') {
                baseAdd = "/country/event/calender?country_id="+ this.props.item.state.country_id+"&village_id="+this.props.item.id
            }
            console.log('bbbbbbbbbbbase addtes',baseAdd)
            Axios.get(baseAdd).then(response=> {
                this.setState({loading: false, events: response.data.data.event});
                let arr = [];
                if(response.data.data.event.length !== 0) {
                    response.data.data.event.map((item) => {
                        for(let i=item.start_day; i<=item.end_day; i++) {
                            arr.push(i)
                        }
                    })
                    this.setState({eventsNum: [...new Set(arr)]})
                    console.log('arr', arr)
                    console.log('arr', [...new Set(arr)])
                }
                console.log('event data', response.data.data)
            })
                .catch((error) => {
                    console.log(error)
                    Alert.alert('مشکلی در برقراری ارتباط با سرور رخ داده لطفا مجددا تلاش نمایید')
                    this.setState({loading: false});
                });
        }
    }
    closeModal() {
        this.setState({modalVisible: false});
    }

    onDateChange(date) {
        this.setState({ selectedStartDate: date });
    }
    render() {
        return (
            <View style={styles.container}>
                <Modal animationType = {"fade"} transparent = {true}
                       visible = {this.props.modalVisible}
                       onRequestClose = {() => this.props.onChange(false)}
                       onBackdropPress= {() => this.props.onChange(!this.state.modalVisible)}
                >
                    <View style = {styles.modal}>
                        <View style={styles.box}>
                            <View style={styles.headerContainer}>
                                <TouchableOpacity onPress={() => this.props.closeModal()}>
                                    <Icon name="close" size={20} color={ "black"} />
                                </TouchableOpacity>
                                <Text style={styles.headerText}>{this.props.title}</Text>
                            </View>
                            {
                                this.state.loading ?
                                    <View style={{width: '100%', height: 120}}>
                                        <BallIndicator color='#21b34f' size={50} style={{height: 90}} />
                                    </View> :
                                    <ScrollView>
                                        <View style={{width: '100%', alignItems: 'center', justifyContent: 'center'}}>
                                            <View style={styles.calenderContainer}>
                                                <PersianCalendarPicker
                                                    // onDateChange={this.onDateChange}
                                                    width={250}
                                                    height={250}
                                                    initialDate={this.state.events.length === 0 ? undefined : this.state.events[0].created_at}
                                                    eventsNum={this.state.eventsNum}
                                                />
                                            </View>
                                            <View style={styles.eventsContainer}>
                                                {
                                                    this.state.events.length !== 0 && this.state.events.map((item, index) => {
                                                        return <View style={styles.eventContainer} key={index}>
                                                            <TouchableOpacity onPress={() => {this.props.closeModal();Actions.reset('placeDetail', {item: item, subName: 'رویداد'})}} style={styles.moreContainer}>
                                                                <Text style={styles.moreText}>اطلاعات بیشتر</Text>
                                                            </TouchableOpacity>
                                                            <View style={styles.textContainer}>
                                                                <Text style={styles.titleText}>{item.title}</Text>
                                                                <View style={styles.footerContainer}>
                                                                    <Text style={styles.footerText}>{item.start_year+'/'+item.start_month+'/'+item.start_day} - {item.end_year+'/'+item.end_month+'/'+item.end_day}</Text>
                                                                    <FIcon name="clock" size={18} color="gray" />
                                                                </View>
                                                            </View>
                                                            <View style={styles.circleContainer}>
                                                                <Text style={styles.circleText}>{item.start_day}</Text>
                                                            </View>
                                                        </View>
                                                    })
                                                }
                                            </View>
                                        </View>
                                    </ScrollView>
                            }
                        </View>
                    </View>
                </Modal>
            </View>
        )
    }
}
export default InfoModal

const styles = StyleSheet.create ({
    container: {
        alignItems: 'center',
        // backgroundColor: 'white',
        padding: 10,
        position: 'absolute',
        bottom: -200
    },
    modal: {
        position: 'relative',
        zIndex: 0,
        flexGrow: 1,
        justifyContent: 'center',
        // flex: 1,
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,.6)',
        paddingRight: 20,
        paddingLeft: 20
    },
    text: {
        color: '#3f2949',
        marginTop: 10
    },
    box: {
        width: '100%',
        backgroundColor: 'white',
        // backgroundColor: 'rgb(218, 218, 218)',
        alignItems: 'center',
        justifyContent: 'flex-start',
        padding: 10,
        borderRadius: 15,
        paddingBottom: 20,
        height: '70%'
    },
    picker: {
        height: 40,
        backgroundColor: 'white',
        width: '40%',
        color: 'black',
        borderColor: 'lightgray',
        borderWidth: 1,
        // elevation: 4,
        borderRadius: 10,
        // marginBottom: 20,
        // overflow: 'hidden'
    },
    input: {
        width: '100%',
        fontSize: 16,
        paddingTop: 0,
        textAlign: 'right',
    },
    searchButton: {
        width: '90%',
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgb(20, 122, 170)'
    },
    searchText:{
        color: 'white'
    },
    // picker: {
    //     height:50,
    //     width: "100%",
    //     alignSelf: 'flex-end'
    // },
    label: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'rgb(60, 60, 60)'
    },
    headerContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 8,
        borderBottomWidth: 1,
        borderBottomColor: 'rgb(237, 237, 237)',
        marginBottom: 20
    },
    headerText: {
        fontSize: 15,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black'
    },
    advertise: {
        width: '35%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 30,
        backgroundColor: '#8dc63f',
        padding: 5,
        // marginTop: 0
    },
    buttonTitle: {
        fontSize: 14,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    inputLabel: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        alignSelf: 'flex-end',
        paddingRight: '10%',
        paddingBottom: 5
    },
    insContainer: {
        width: '100%',
        flexDirection: 'row',
        flexWrap: 'wrap',
        // alignItems: 'center',
        // justifyContent: 'space-between',
        padding: 8
    },
    info: {
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingTop: 20,
        paddingBottom: 20,
        borderBottomColor: 'white',
        borderBottomWidth: 1
    },
    rowsContainer: {
        width: '70%'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    itemContainer: {
        width: '48%'
    },
    title: {
        color: 'rgb(60, 60, 60)',
        fontSize: 11,
        fontFamily: 'IRANSansMobile(FaNum)',
        alignSelf: 'flex-end',
        paddingRight: 10
    },
    imageContainer: {
        borderRadius: 15,
        padding: 7,
        backgroundColor: 'white',
        width: '20%',
        marginRight: 10,
        marginLeft: 10
    },
    calenderContainer: {
        width: 250,
        height: 250
    },
    eventContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'flex-end',
        marginBottom: 20,
        paddingRight: 10
    },
    circleContainer: {
        width: 40,
        height: 40,
        borderRadius: 60,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#7cc576'
    },
    circleText: {
        color: 'white',
        fontSize: 14,
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    textContainer: {
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    titleText: {
        color: 'black',
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingRight: 10,
        paddingLeft: 10,
    },
    footerContainer: {
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: 'center',
        paddingRight: 10
    },
    footerText: {
        color: 'gray',
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingLeft: 10,
        paddingRight: 10
    },
    moreText: {
        color: 'white',
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    moreContainer: {
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        paddingRight: 10,
        paddingLeft: 10,
        paddingTop: 3,
        paddingBottom: 3,
        backgroundColor: '#7da7d9',
        alignSelf: 'center'
    },
    eventsContainer: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',

    }
})