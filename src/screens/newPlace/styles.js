/**
 * Created by n.moharami on 4/29/2019.
 */

import { StyleSheet, Dimensions} from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(234, 234, 234)',

    },
    scroll: {
        paddingTop: 0,
        paddingRight: 20,
        paddingLeft: 20,
        // marginBottom: 90,
        // backgroundColor: 'red',
        position: 'absolute',
        top: 60,
        bottom: 0,
        width: '100%'

    },
    body: {
        // paddingBottom: 150,
        paddingTop: 10,
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor: 'green',
        width: '100%',
        paddingBottom: 90
    },
    content: {
        // paddingBottom: 150,
        paddingTop: 10,
        // alignItems: 'flex-start',
        // justifyContent: 'center',
        // backgroundColor: 'green',
        width: '100%'
    },

    imageContainer: {
        position: 'absolute',
        zIndex: 20,
        // top: 50,
        // left: 50,
        // width: 60,
        backgroundColor: 'red'
    },
    image: {
        position: 'absolute',
        zIndex: 22,
        top: 0,
        left: 0,
        width: 50,
        height: 50,
        resizeMode: 'stretch'
    },
    pic: {
        position: 'absolute',
        zIndex: 21,
        top: 3,
        left: 5,
        width: 40,
        height: 38,
        borderRadius: 200,
        resizeMode: 'stretch'
    },
    navitemContainer: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-around',
        // paddingTop: 25,
        // paddingBottom: 30,
        // paddingRight: 20,
        // height: 120,
        width: '100%',
    },
    navContainer: {
        alignItems: "center",
        justifyContent: 'center',
        width: '100%',
        height: 50,
        // marginLeft: 20,
        backgroundColor: 'white',
        padding: 20,
        paddingTop: 35,
        paddingBottom: 35,

    },
    navContainerH: {
        alignItems: "center",
        justifyContent: 'center',
        height: 35,
        // marginLeft: 20,
        backgroundColor: 'rgb(230, 115, 7)',
        padding: 0,
        // paddingTop: 35,
        // paddingBottom: 35,

        flexDirection: 'row',
        width: 140,
        marginRight: 15,
        transform: [
            {rotateY: '180deg'},
        ],
        borderRadius: 7
    },
    text: {
        fontSize: 13,
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'right',
        color: 'black',
        alignSelf: 'flex-end'

    },
    mapStyle: {
        width: '100%',
        height: 200
    },


    // map: {
    //     ...StyleSheet.absoluteFillObject
    // },
    bubble: {
        flex: 1,
        backgroundColor: "rgba(255,255,255,0.7)",
        paddingHorizontal: 18,
        paddingVertical: 12,
        borderRadius: 20
    },
    latlng: {
        width: 200,
        alignItems: "stretch"
    },
    button: {
        width: 80,
        paddingHorizontal: 12,
        alignItems: "center",
        marginHorizontal: 10
    },
    buttonContainer: {
        flexDirection: "row",
        marginVertical: 20,
        backgroundColor: "transparent"
    },
    addImage: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 100,
        height: 100,
        backgroundColor: 'rgb(59, 140, 219)',
        borderRadius: 6,
        marginBottom: 13
    },
    addText: {
        color: 'black',
        fontSize: 12,
        // paddingTop: 10
    },
    Image: {
        width: 80,
        height: 80,
        borderRadius: 6,
        marginLeft: 13,
        // marginBottom: 30,
        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden',
        margin: 10,

    },
    addText2: {
        color: 'white',
        fontSize: 10,
        paddingTop: 10,
        textAlign: 'center'
    },
    lebel: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black'
    },
    question: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingTop: 20,
        paddingBottom: 20,
        paddingRight: 10,
        paddingLeft: 10
    },
    questionText: {
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12
    },
    advertise: {
        width: '30%',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        backgroundColor: '#8dc63f',
        padding: 5,
        marginBottom: 20,
        marginTop: 30,
    },
    buttonTitle: {
        fontSize: 14,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    top: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'flex-start',
        padding: 15,
        backgroundColor: 'white',
        width: '100%'
    },
    title: {
        fontSize: 15,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black'
    },
    Image2: {
        width: 80,
        height: 80,
        borderRadius: 6,
        marginLeft: 7,
        marginBottom: 13,
        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden'
    },
    photoContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: 20,
        paddingRight: 30,
        flexWrap: 'wrap',
        backgroundColor: 'lightgray',
        borderRadius: 10,
        margin: 10
    },
    addImage2: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 80,
        height: 80,
        backgroundColor: 'rgb(59, 140, 219)',
        borderRadius: 6,
        // marginBottom: 13
    },

});
