/**
 * Created by n.moharami on 4/29/2019.
 */
import React, {Component} from 'react';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://ppt.azarinpro.info';
Axios.defaults.baseURL = url;
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
import Loader from '../../components/loader'
import ImagePicker from 'react-native-image-picker';
import Icon from 'react-native-vector-icons/dist/Feather';
import MIcon from 'react-native-vector-icons/dist/MaterialCommunityIcons';
import ModalFilterPicker from 'react-native-modal-filter-picker'
import Selectbox from 'react-native-selectbox'
import CustomMultiPicker from "react-native-multiple-select-list";

import {
    View,
    BackHandler,
    ScrollView,
    Text,
    TouchableOpacity,
    ImageBackground,
    TextInput,
    Alert,
    AsyncStorage,
    Image
} from "react-native";

class NewPlace extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            title: this.props.item && this.props.item.title ? this.props.item.title : '',
            lname: '',
            mobile: this.props.item && this.props.item.phone ? this.props.item.phone : '',
            email: this.props.item && this.props.item.email ? this.props.item.email : '',
            telegram: this.props.item && this.props.item.social_media ? JSON.parse(this.props.item.social_media).telegram : '',
            instagram: this.props.item && this.props.item.social_media ? JSON.parse(this.props.item.social_media).instagram : '',
            googlePlus: this.props.item && this.props.item.social_media ? JSON.parse(this.props.item.social_media).google : '',
            shortDescription: this.props.item && this.props.item.description ? this.props.item.description : '',
            website: this.props.item && this.props.item.website ? this.props.item.website : '',
            address: this.props.item && this.props.item.address ? this.props.item.address : '',
            province: this.props.item && this.props.item.state ? this.props.item.state.id : 0 ,
            provinceTitle: this.props.item && this.props.item.state.title ? this.props.item.state.title : '',
            town: this.props.item && this.props.item.city ? this.props.item.city.id : 0,
            townTitle: this.props.item && this.props.item.city ? this.props.item.city.title : '',
            country: this.props.item && this.props.item.country ? this.props.item.country.id : 0,
            countryTitle: this.props.item && this.props.item.country.title ? this.props.item.country.title : '',
            countries: [],
            provinces: [],
            city: [],
            categoryData: [],
            categoryData1: [],
            categoryData2: [],
            userSkills: [],
            visibleinput1: false,
            visibleinput2: false,
            visibleinput3: false,
            visibleinput4: false,
            skills: {key: 0, label: "افزودن مهارت", value: 0},
            skillsTitle: '',
            mainCat:{key: 0, label: "انتخاب نشده", value: 0},
            mainCatTitle:null,
            // mainCat: this.props.item.category ? {key: this.props.item.category.id, label: this.props.item.category.title, value: this.props.item.category.id} : {key: 0, label: "انتخاب نشده", value: 0},
            // mainCatTitle:this.props.item.category ? this.props.item.category.title : null,
            mainCat1:{key: 0, label: "انتخاب نشده", value: 0},
            mainCat1Title:null,
            mainCat2:{key: 0, label: "انتخاب نشده", value: 0},
            tagTitle:null,
            tag:{key: 0, label: "انتخاب نشده", value: 0},
            tagItems: {},
            mainCat2Title:null,
            loading: false,
            loading1: false,
            loading2: false,
            loading3: false,
            loading4: false,
            src: null,
            markers: [],
            lat: null,
            lng: null,
            color: 'rgb(227, 5, 26)',
            activeSwitch: 1,
            initialLat: this.props.item && this.props.user.latitude === null ? 35.715298 : this.props.user.latitude,
            initialLng: this.props.item && this.props.user.longitude === null ? 51.404343 : this.props.user.longitude,
            categoryDataItems: [],
            categoryDataItems1: [],
            categoryDataItems2: [],
            forms: [],
            formsInfo: {},
            imageCount: 0,
            imageArray: [],
            selectedTags: [],
            content: this.props.item && this.props.item.content ? this.props.item.content : '',
        };
        this.onBackPress = this.onBackPress.bind(this);
    }

    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        AsyncStorage.getItem('token').then((info) => {
            if(info !== null) {
                this.setState({loading1: true, loading: true});
                Axios.get('/country/country-data').then(response => {
                    this.setState({countries: response.data, loading1: false});

                    Axios.get('/category/show').then(response => {
                        let items = [];
                        items.push({key: 0, label: 'انتخاب کنید', value: 0})
                        response.data.data.length !== 0 && response.data.data.map((item, index) => {items.push({key: item.id, label: item.title, value: item.id})});
                        this.setState({categoryDataItems: items, categoryData: response.data.data});


                        Axios.get('/business/tag/show').then(response => {
                            let items2 = {};
                            response.data.data.length !== 0 && response.data.data.map((item, index) => {
                                items2[item.id]= item.title;
                            });
                            this.setState({tagItems: items2, loading: false});
                        })
                            .catch((error) => {
                                Alert.alert('', 'خطایی رخ داده لطفا مجددا سعی نمایید')
                                this.setState({loading: false});
                                console.log('errore  user skilll show',error.response);
                            });
                    })
                        .catch((error) => {
                            Alert.alert('', 'خطایی رخ داده لطفا مجددا سعی نمایید')
                            this.setState({loading: false});
                            console.log('errore  user skilll show',error.response);
                        });

                })
                    .catch((error) => {
                        Alert.alert('', 'خطایی رخ داده لطفا مجددا سعی نمایید')
                        this.setState({loading1: false});
                        console.log(error);
                    });
            }
            else {
                console.log('it is not login')
                Alert.alert('','لطفا ابتدا وارد شوید');
                Actions.loginPage({newPlace: true});
            }
        });
    }
    getcat1(itemvalue){
        let subs1 = []
        this.state.categoryData.map((item, index)=> {
          if(item.id === itemvalue.value) {
              subs1 = item.subs;

              let items = [];
              items.push({key: 0, label: 'انتخاب کنید', value: 0})
              subs1.length !== 0 && subs1.map((item, index) => {items.push({key: item.id, label: item.title, value: item.id})});
              this.setState({categoryDataItems1: items});

          }
        })
    }
    getcat2(itemvalue){
        let subs1 = [], subs2 = [];
        this.state.categoryData.map((item)=> {
            if(item.id === this.state.mainCat.value) {
                subs1 = item.subs;

                subs1.length !== 0 && subs1.map((item2) => {
                    if(item2.id === itemvalue.value) {
                        subs2 = item2.subs;

                        let items = [];
                        items.push({key: 0, label: 'انتخاب کنید', value: 0})
                        subs2.length !== 0 && subs2.map((item3) => {items.push({key: item3.id, label: item3.title, value: item3.id})});
                        this.setState({categoryDataItems2: items});
                        console.log('formes ', item2)
                    }
                });
            }
        })
    }
    getForm(itemvalue){
        let subs1 = [], subs2 = [], subs3 = [];
        this.state.categoryData.map((item)=> {
            if(item.id === this.state.mainCat.value) {
                subs1 = item.subs;

                subs1.length !== 0 && subs1.map((item2) => {
                    if(item2.id === this.state.mainCat1.value) {
                        subs2 = item2.subs;
                        subs2.length !== 0 && subs2.map((item3) => {
                            if(item3.id === itemvalue.value) {
                                this.setState({forms: item3.form});
                                console.log('forms', item3.form)
                            }
                        });
                    }
                });
            }
        })
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
        navigator.geolocation.clearWatch(this.watchID);
    }
    selectPhotoTapped() {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true,

            }
        };
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };

                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.setState({src: response.uri})
            }
        });
    }
    selectPhotoTapped2() {
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true,

            }
        };

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };

                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };

                let countArray = {id: this.state.imageCount + 1, src: response.uri};
                const newImageArray =  this.state.imageArray;
                newImageArray.push(countArray);
                this.setState({
                    imageCount: ++this.state.imageCount,
                    imageArray: newImageArray
                })
                // let imgArray = this.state.imageArray;
                // let imgData = this.state.imageData;
                // imgArray[item.id-1].src = source;
                // imgData[item.id-1] = response.uri;
                //
                // this.setState({
                //     imageArray: imgArray,
                //     imageData: imgData
                // });
            }
        });
    }
    getState(id) {
        this.setState({loading2:true});
        Axios.get('/country/states-data/'+id).then(response => {
            this.setState({provinces: response.data, loading2: false});
            console.log('states', response.data);

        })
            .catch((error) =>{
                Alert.alert('', 'خطایی رخ داده لطفا مجددا سعی نمایید')
                this.setState({loading2: false});
            });
    }
    getCities(id) {
        // console.log('provinceTitle', this.state.citTitle)
        this.setState({loading3:true});
        Axios.get('/country/cities-data/'+id).then(response => {
            this.setState({city: response.data.city});
            console.log('cities', response.data);
            Axios.get('https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input='+this.state.provinceTitle+'&inputtype=textquery&fields=geometry&key=AIzaSyACjNKR5CCk0nGL5uBGm2IME_c6FfML9eg').then(response => {
                console.log('ressssssssss map', response)
                this.setState({loading3: false, initialLat: response.data.candidates[0].geometry.location.lat, initialLng: response.data.candidates[0].geometry.location.lng});
                // console.log('MMMMAPPPPP', 'https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input='+ this.state.provinceTitle+'&inputtype=textquery&fields=geometry&key=AIzaSyACjNKR5CCk0nGL5uBGm2IME_c6FfML9eg');
                console.log('initialLat', this.state.initialLat);
                console.log('initialLat', this.state.initialLng);

            })
                .catch((error) =>{
                    // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                    // this.setState({loading2: false});
                    console.log(error)
                    this.setState({modalVisible: true, loading2: false});

                });
        })
            .catch((error) =>{
                Alert.alert('', 'خطایی رخ داده لطفا مجددا سعی نمایید')
                this.setState({loading3: false});
            });
    }
    onShowinput1 = () => {
        this.setState({ visibleinput1: true });
    }

    onSelectInput1 = (picked) => {
        console.log('picked', picked)
        this.setState({
            country: picked,
            visibleinput1: false
        }, () =>{
            console.log('ppppicked', picked)
            this.state.countries.map((item) => {
                if(picked === item.id) {
                    this.setState({countryTitle: item.title}, () => {this.getState(picked);console.log(this.state.countryTitle)});
                }
            })
        })
    }
    onCancelinput1 = () => {
        this.setState({
            visibleinput1: false
        })
    }
    onShowinput2 = () => {
        this.setState({ visibleinput2: true });
    }

    onSelectInput2 = (picked) => {
        console.log('picked', picked)
        this.setState({
            province: picked,
            visibleinput2: false
        }, () =>{
            console.log('ppppicked', picked)
            this.state.provinces.map((item) => {
                if(picked === item.id) {
                    this.setState({provinceTitle: item.title}, () => {this.getCities(picked);console.log(this.state.provinceTitle)});
                }
            })
        })
    }
    onCancelinput2 = () => {
        this.setState({
            visibleinput2: false
        })
    }
    onShowinput3 = () => {
        this.setState({ visibleinput3: true });
    }
    onSelectInput3 = (picked) => {
        console.log('picked', picked)
        this.setState({
            town: picked,
            visibleinput3: false
        }, () =>{
            console.log('ppppicked', picked)
            this.state.city.map((item) => {
                if(picked === item.id) {
                    this.setState({townTitle: item.title}, () => {console.log(this.state.townTitle)});
                }
            })
        })
    }
    onCancelinput3 = () => {
        this.setState({
            visibleinput3: false
        })
    }
    handlePress(e) {
        console.log('map', e)
        this.setState({
            markers: [
                {
                    coordinate: e.coordinate,
                }
            ],
            lat: e.coordinate.latitude,
            lng: e.coordinate.longitude
        })
    }

    updateInfo() {

        AsyncStorage.getItem('token').then((info) => {
            if(info !== null) {
                console.log('iyttttttttttttem', this.props.item!== null);

                const newInfo = JSON.parse(info);
                console.log('token', newInfo.token)
                Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;
                if(this.state.title === ''  ) {
                    Alert.alert('', 'لطفا موضوع را صحیح وارد نمایید')
                }
                else if(this.state.country === 0) {
                    Alert.alert('', 'لطفا کشور را صحیح وارد نمایید')
                }
                else if( this.props.item === null && this.state.mainCat2.value === 0) {
                    Alert.alert('', 'لطفا دسته بندی ها را تعیین نمایید')
                }
                else {

                    let formdata = new FormData();
                    this.state.title !== '' && formdata.append("title", this.state.title)
                    this.state.phone !== '' && formdata.append("phone", this.state.mobile)
                    this.state.email !== '' && formdata.append("email", this.state.email)
                    this.state.website !== '' && formdata.append("website", this.state.website)
                    this.state.address !== '' && formdata.append("address", this.state.address)
                    this.state.shortDescription !== '' && formdata.append("description", this.state.shortDescription)
                    this.state.content !== '' &&  formdata.append("content", this.state.content)
                    this.state.country !== 0 && formdata.append("country_id", this.state.country)
                    this.state.town !== 0 && formdata.append("city_id", this.state.town)
                    this.state.province !== 0 && formdata.append("state_id", this.state.province)
                    this.state.mainCat2.value !== 0 && formdata.append("business_cat_id", this.state.mainCat2.value)
                    this.props.item !== null && formdata.append("id", this.props.item.id)
                    if(this.state.telegram !== '' || this.state.instagram !== '' || this.state.google !== '') {
                        let obj = {};
                        obj.telegram = this.state.telegram;
                        obj.instagram  = this.state.instagram;
                        obj.google = this.state.googlePlus;
                        const jsonString= JSON.stringify(obj);
                        formdata.append("social_media", jsonString)
                    }

                    if(Object.keys(this.state.formsInfo).length !== 0) {
                        let obj2 = this.state.formsInfo;
                        const jsonString2= JSON.stringify(obj2);
                        formdata.append("data", jsonString2)
                    }

                    if(this.state.src !== null) {
                        formdata.append("type", "image");
                        formdata.append("mainpic", {
                            uri: this.state.src,
                            type: 'image/jpeg',
                            name: 'mainpic'
                        });
                    }
                    console.log('formdata formdata', formdata)

                    if(this.state.selectedTags.length !== 0) {
                        let data = []
                        this.state.selectedTags.map((item)=> {
                            for(let key in this.state.tagItems) {
                                if(this.state.tagItems[key] === item) {
                                    data.push(parseInt(key))
                                }
                            }
                        })
                        console.log('data', data)
                        if(data.length !== 0) {
                            data.map((item2) => {
                                formdata.append("tag[]", item2)
                            })
                        }
                    }
                    if(this.state.imageArray.length !== 0){
                        this.state.imageArray.map((item) => {
                            formdata.append("type", "image");
                            formdata.append("document[]", {
                                uri: item.src,
                                type: 'image/jpeg',
                                name: 'document[]'
                            });
                        })
                    }
                    this.setState({loading: true});
                    let x = this.props.item !== null ? 'update' : 'create'
                    console.log('iyttttttttttttem', this.props.item);

                    Axios.post('/business/'+x, formdata
                    ).then(response=> {
                        this.setState({loading: false});
                        this.props.item !== null ? Alert.alert('','مکان با موفقیت ویراِیش یافت') : Alert.alert('','مکان با موفقیت ایجاد شد');
                        console.log('update response', response.data);
                    })
                        .catch((response) => {
                            console.log('resssponse erroreie create ', response.response.data.message);
                            if(response.response.data.message === "Unauthenticated."){
                                Alert.alert('','لطفا ابتدا وارد شوید');
                                this.setState({loading: false});
                                Actions.loginPage({newPlace: true});
                            }
                            else{
                                Alert.alert('','خطایی رخ داده مجددا سعی نمایید');
                                this.setState({loading: false});
                            }
                        });
                }
            }
            else {
                console.log('it is not login')
                Alert.alert('','لطفا ابتدا وارد شوید');
                this.setState({loading: false});
                Actions.loginPage({newPlace: true});
            }
        });
    }

    deleteImg(id) {
        const newImageArray = this.state.imageArray.filter(item => item.id !== id);
        this.setState({
            imageArray: newImageArray
        })
    }
    render() {
        const {user} = this.props;
        const userList = {
            "123":"Tom",
            "124":"Michael",
            "125":"Christin"
        };
        console.log('this.state.tagItems', this.state.tagItems)
        if(this.state.loading){
            return (<Loader />)
        } else return (
            <View style={styles.container}>
                <View style={styles.top}>
                    <TouchableOpacity onPress={() => this.onBackPress()}>
                        <Icon name="arrow-left" size={23} color="gray" />
                    </TouchableOpacity>
                    <Text style={[styles.title, {paddingLeft: '30%'}]}>{this.props.item !== null ? 'ویرایش مکان' : 'ثبت مکان جدید'}</Text>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.body}>
                        <Text style={styles.text}>افزودن عکس</Text>
                        <View style={styles.photoContainer}>
                            <ImageBackground style={styles.Image} source={{uri: this.state.src === null ? "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSlzPUy6JN_a_IMHbXR8eD49XIkuqrxSV5opddszq8dpnWI05j-" : this.state.src}}>
                                <TouchableOpacity onPress={() => this.selectPhotoTapped()} style={{
                                    backgroundColor:'rgba(0,0,0,.1)',
                                    height: 80,
                                    width: 80,
                                    alignItems: "center",
                                    justifyContent: "center",
                                    borderRadius: 6
                                }}>
                                    <Icon name="camera" size={20} color="white"/>
                                    <Text style={styles.addText2}>افزودن عکس اصلی</Text>
                                </TouchableOpacity>
                            </ImageBackground>
                            <TouchableOpacity onPress={() => this.selectPhotoTapped2()}>
                                <View style={styles.addImage2}>
                                    <Icon name="camera" size={20} color="white"/>
                                    <Text style={styles.addText2}>افزودن عکس های اسلایدر</Text>
                                </View>
                            </TouchableOpacity>
                            {this.state.imageCount !== 0 ?
                                this.state.imageArray.map((item, index) => {return <ImageBackground style={styles.Image2} source={{uri: item.src}} key={index}>
                                    <View style={{
                                        backgroundColor:'rgba(0,0,0,.6)',
                                        height: 40,
                                        width: 40,
                                        alignItems: "center",
                                        justifyContent: "center",
                                        borderRadius: 30
                                    }}>
                                        <TouchableOpacity onPress={() => this.deleteImg(item.id)}>
                                            <MIcon name="close" size={20} color="white" />
                                        </TouchableOpacity>
                                    </View>
                                </ImageBackground>
                                }) : null
                            }
                        </View>
                        <Text style={styles.text}>موضوع</Text>
                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginBottom: 20, marginTop: 5, borderRadius: 7 }}>
                            <TextInput
                                maxLength={15}
                                placeholder={'نام'}
                                placeholderTextColor={'gray'}
                                underlineColorAndroid='transparent'
                                value={this.state.title}
                                style={{
                                    height: 40,
                                    paddingRight: 15,
                                    width: '100%',
                                    color: 'gray',
                                    fontSize: 14,
                                }}
                                onChangeText={(text) => {this.setState({title: text})}}
                            />
                        </View>
                        <Text style={styles.text}>کشور</Text>
                        <TouchableOpacity onPress={this.onShowinput1} style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginTop: 5, marginBottom: 10, borderRadius: 7}}>
                            <Icon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                            <Text style={{paddingTop: 5, paddingRight: 10,  fontFamily: 'IRANSansMobile(FaNum)', color: 'gray' }}>{this.state.loading1 ? "در حال بارگذاری..." : this.state.country === 0 ? "انتخاب نشده" : this.state.countryTitle}</Text>
                            <ModalFilterPicker
                                visible={this.state.visibleinput1}
                                onSelect={this.onSelectInput1}
                                onCancel={this.onCancelinput1}
                                options={ this.state.countries.map((item) => {return {key: item.id, label: item.title, value: item.id}})}
                                placeholderText="جستجو ..."
                                cancelButtonText="لغو"
                                // filterTextInputStyle={{textAlign: 'right'}}
                                optionTextStyle={{width: '100%'}}
                                titleTextStyle={{textAlign: 'right'}}
                            />
                        </TouchableOpacity>

                        <Text style={styles.text}>استان</Text>
                        <TouchableOpacity onPress={this.onShowinput2} style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginTop: 5, marginBottom: 10, borderRadius: 7}}>
                            <Icon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                            <Text style={{paddingTop: 5, paddingRight: 10,  fontFamily: 'IRANSansMobile(FaNum)', color: 'gray' }}>{this.state.loading2 ? "در حال بارگذاری..." : this.state.province === 0 ? "انتخاب نشده" : this.state.provinceTitle}</Text>
                            <ModalFilterPicker
                                visible={this.state.visibleinput2}
                                onSelect={this.onSelectInput2}
                                onCancel={this.onCancelinput2}
                                options={ this.state.provinces.map((item) => {return {key: item.id, label: item.title, value: item.id}})}
                                placeholderText="جستجو ..."
                                cancelButtonText="لغو"
                                // filterTextInputStyle={{textAlign: 'right'}}
                                optionTextStyle={{width: '100%'}}
                                titleTextStyle={{textAlign: 'right'}}
                            />
                        </TouchableOpacity>

                        <Text style={styles.text}>شهر</Text>
                        <TouchableOpacity onPress={this.onShowinput3} style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginTop: 5, marginBottom: 10, borderRadius: 7}}>
                            <Icon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                            <Text style={{paddingTop: 5, paddingRight: 10,  fontFamily: 'IRANSansMobile(FaNum)', color: 'gray' }}>{this.state.loading3 ? "در حال بارگذاری..." : this.state.town === 0 ? "انتخاب نشده" : this.state.townTitle}</Text>
                            <ModalFilterPicker
                                visible={this.state.visibleinput3}
                                onSelect={this.onSelectInput3}
                                onCancel={this.onCancelinput3}
                                options={ this.state.city.map((item) => {return {key: item.id, label: item.title, value: item.id}})}
                                placeholderText="جستجو ..."
                                cancelButtonText="لغو"
                                // filterTextInputStyle={{textAlign: 'right'}}
                                optionTextStyle={{width: '100%'}}
                                titleTextStyle={{textAlign: 'right'}}
                            />
                        </TouchableOpacity>
                        {
                            this.props.item !== null ? null : <View style={[styles.body, {paddingBottom: 0, marginBottom: 0}]}>
                                <Text style={styles.text}>دسته بندی </Text>
                                <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginTop: 20, marginBottom: 10, borderRadius: 7}}>
                                    {/*<Icon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>*/}
                                    <Selectbox
                                        style={{height: 42, paddingTop: '3%', paddingRight: 10}}
                                        selectLabelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingTop: 0}}
                                        optionLabelStyle={{textAlign: 'center', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                        selectedItem={this.state.mainCat}
                                        cancelLabel="لغو"
                                        onChange={(itemValue) =>{
                                            this.setState({
                                                mainCat: itemValue
                                            }, () => {
                                                this.setState({ mainCatTitle: itemValue.label}, () => {this.getcat1(itemValue);console.log(this.state.mainCatTitle)});
                                            })}}
                                        items={this.state.categoryDataItems} />
                                </View>
                                <Text style={styles.text}>زیر دسته 1 </Text>
                                <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginTop: 20, marginBottom: 10, borderRadius: 7}}>
                                    {/*<Icon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>*/}
                                    <Selectbox
                                        style={{height: 42, paddingTop: '3%', paddingRight: 10}}
                                        selectLabelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingTop: 0}}
                                        optionLabelStyle={{textAlign: 'center', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                        selectedItem={this.state.mainCat1}
                                        cancelLabel="لغو"
                                        onChange={(itemValue) =>{
                                            this.setState({
                                                mainCat1: itemValue
                                            }, () => {
                                                this.setState({ mainCat1Title: itemValue.label}, () => {this.getcat2(itemValue);console.log(this.state.mainCat1Title)});
                                            })}}
                                        items={this.state.categoryDataItems1} />
                                </View>
                                <Text style={styles.text}>زیر دسته 2 </Text>
                                <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginTop: 20, marginBottom: 10, borderRadius: 7}}>
                                    {/*<Icon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>*/}
                                    <Selectbox
                                        style={{height: 42, paddingTop: '3%', paddingRight: 10}}
                                        selectLabelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingTop: 0}}
                                        optionLabelStyle={{textAlign: 'center', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}
                                        selectedItem={this.state.mainCat2}
                                        cancelLabel="لغو"
                                        onChange={(itemValue) =>{
                                            this.setState({
                                                mainCat2: itemValue
                                            }, () => {
                                                this.setState({ mainCat2Title: itemValue.label}, () => {this.getForm(itemValue);console.log(this.state.mainCat2Title)});
                                            })}}
                                        items={this.state.categoryDataItems2} />
                                </View>
                            </View>
                        }
                        {
                            this.state.forms.length !== 0 ? this.state.forms.map((item, index)=> {
                                return <View style={{width: '100%', alignItems: 'center', justifyContent: 'center'}} key={index}>
                                    <Text style={[styles.text, {color: 'green', padding: 5}]}>{item.title}</Text>
                                    <TextInput
                                        placeholder="توضیحات"
                                        placeholderTextColor={'gray'}
                                        underlineColorAndroid='transparent'
                                        value={this.state.text}
                                        style={{
                                            paddingRight: 15,
                                            paddingLeft: 15,
                                            fontFamily: 'IRANSansMobile(FaNum)',
                                            fontSize: 13,
                                            color: 'black',
                                            width: '85%',
                                            backgroundColor: 'white',
                                            height: 42,
                                            marginBottom: 5,
                                            borderRadius: 7,
                                        }}
                                        onChangeText={(text) => {
                                            let data = this.state.formsInfo;
                                            data[item.id] = text;
                                            this.setState({formsInfo: data})
                                        }}
                                    />
                                </View>
                            }) : null
                        }

                        <Text style={styles.text}>ایمیل</Text>
                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginTop: 5, marginBottom: 20, borderRadius: 7}}>
                            <TextInput
                                maxLength={15}
                                keyboardType={'email-address'}
                                placeholder={'ایمیل'}
                                placeholderTextColor={'gray'}
                                underlineColorAndroid='transparent'
                                value={this.state.email}
                                style={{
                                    height: 40,
                                    paddingRight: 15,
                                    width: '100%',
                                    color: 'gray',
                                    fontSize: 14
                                }}
                                onChangeText={(text) => {this.setState({email: text})}}
                            />
                        </View>
                        <Text style={styles.text}>موبایل</Text>
                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginTop: 5, marginBottom: 20, borderRadius: 7}}>
                            <TextInput
                                maxLength={15}
                                placeholder={'موبایل'}
                                keyboardType={'numeric'}
                                placeholderTextColor={'gray'}
                                underlineColorAndroid='transparent'
                                value={this.state.mobile}
                                style={{
                                    height: 40,
                                    paddingRight: 15,
                                    width: '100%',
                                    color: 'gray',
                                    fontSize: 14
                                }}
                                onChangeText={(text) => {this.setState({mobile: text})}}
                            />
                        </View>
                        <Text style={styles.text}>وبسایت</Text>
                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginTop: 5, marginBottom: 20, borderRadius: 7}}>
                            <TextInput
                                maxLength={15}
                                placeholder={'وبسایت'}
                                placeholderTextColor={'gray'}
                                underlineColorAndroid='transparent'
                                value={this.state.website}
                                style={{
                                    height: 40,
                                    paddingRight: 15,
                                    width: '100%',
                                    color: 'gray',
                                    fontSize: 14
                                }}
                                onChangeText={(text) => {this.setState({website: text})}}
                            />
                        </View>
                        <Text style={styles.text}>آدرس</Text>
                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginTop: 5, marginBottom: 20, borderRadius: 7}}>
                            <TextInput
                                maxLength={15}
                                placeholder={'آدرس'}
                                placeholderTextColor={'gray'}
                                underlineColorAndroid='transparent'
                                value={this.state.address}
                                style={{
                                    height: 40,
                                    paddingRight: 15,
                                    width: '100%',
                                    color: 'gray',
                                    fontSize: 14
                                }}
                                onChangeText={(text) => {this.setState({address: text})}}
                            />
                        </View>
                        <Text style={styles.text}>توضیحات کوتاه</Text>
                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginTop: 5, marginBottom: 20, borderRadius: 7}}>
                            <TextInput
                                maxLength={15}
                                placeholder={'توضیحات کوتاه'}
                                placeholderTextColor={'gray'}
                                underlineColorAndroid='transparent'
                                value={this.state.shortDescription}
                                style={{
                                    height: 40,
                                    paddingRight: 15,
                                    width: '100%',
                                    color: 'gray',
                                    fontSize: 14
                                }}
                                onChangeText={(text) => {this.setState({shortDescription: text})}}
                            />
                        </View>
                        <Text style={styles.text}>توضیحات</Text>
                        <View style={{width: '100%', alignItems: 'center', justifyContent: 'center', marginTop: 10}}>
                            <TextInput
                                multiline = {true}
                                numberOfLines = {6}
                                value={this.state.content}
                                onChangeText={(text) => this.setState({content: text})}
                                placeholderTextColor={'rgb(142, 142, 142)'}
                                underlineColorAndroid='transparent'
                                placeholder="توضیحات..."
                                style={{
                                    // height: 45,
                                    paddingRight: 15,
                                    width: '95%',
                                    fontSize: 14,
                                    color: 'rgb(142, 142, 142)',
                                    // textAlign:'right',
                                    backgroundColor: 'white',
                                    borderWidth: 1,
                                    borderColor: 'rgb(236, 236, 236)',
                                    borderRadius: 10
                                }}
                            />
                        </View>
                        <Text style={styles.text}>تلگرام</Text>
                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginTop: 5, marginBottom: 20, borderRadius: 7}}>
                            <TextInput
                                maxLength={15}
                                placeholder={'تلگرام'}
                                placeholderTextColor={'gray'}
                                underlineColorAndroid='transparent'
                                value={this.state.telegram}
                                style={{
                                    height: 40,
                                    paddingRight: 15,
                                    width: '100%',
                                    color: 'gray',
                                    fontSize: 14
                                }}
                                onChangeText={(text) => {this.setState({telegram: text})}}
                            />
                        </View>
                        <Text style={styles.text}>اینستاگرام</Text>
                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginTop: 5, marginBottom: 20, borderRadius: 7}}>
                            <TextInput
                                maxLength={15}
                                placeholder={'اینستاگرام'}
                                placeholderTextColor={'gray'}
                                underlineColorAndroid='transparent'
                                value={this.state.instagram}
                                style={{
                                    height: 40,
                                    paddingRight: 15,
                                    width: '100%',
                                    color: 'gray',
                                    fontSize: 14
                                }}
                                onChangeText={(text) => {this.setState({instagram: text})}}
                            />
                        </View>
                        <Text style={styles.text}>گوگل پلاس</Text>
                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginTop: 5, marginBottom: 20, borderRadius: 7}}>
                            <TextInput
                                maxLength={15}
                                placeholder={'گوگل پلاس'}
                                placeholderTextColor={'gray'}
                                underlineColorAndroid='transparent'
                                value={this.state.googlePlus}
                                style={{
                                    height: 40,
                                    paddingRight: 15,
                                    width: '100%',
                                    color: 'gray',
                                    fontSize: 14
                                }}
                                onChangeText={(text) => {this.setState({googlePlus: text})}}
                            />
                        </View>
                        <Text style={styles.text}>تگ ها</Text>
                        {/*<View style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginTop: 5, marginBottom: 20, borderRadius: 7}}>*/}
                            {/*/!*<Icon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>*!/*/}
                            {/*<Selectbox*/}
                                {/*style={{height: 42, paddingTop: '3%', paddingRight: 10}}*/}
                                {/*selectLabelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13, paddingTop: 0}}*/}
                                {/*optionLabelStyle={{textAlign: 'center', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 15}}*/}
                                {/*selectedItem={this.state.tag}*/}
                                {/*cancelLabel="لغو"*/}
                                {/*onChange={(itemValue) =>{*/}
                                    {/*this.setState({*/}
                                        {/*tag: itemValue*/}
                                    {/*}, () => {*/}
                                        {/*this.setState({ tagTitle: itemValue.label}, () => {console.log(this.state.tagTitle)});*/}
                                    {/*})}}*/}
                                {/*items={this.state.tagItems} />*/}
                        {/*</View>*/}
                        {
                            Object.keys(this.state.tagItems).length !== 0 ?
                                <View style={{width: '95%'}}>
                                    <CustomMultiPicker
                                        options={this.state.tagItems}
                                        search={true} // should show search bar?
                                        multiple={true} //
                                        placeholder={"جستجو"}
                                        placeholderTextColor={'#757575'}
                                        returnValue={"label"} // label or value
                                        callback={(res)=>{ console.log(res);this.setState({selectedTags: res})}} // callback, array of selected items
                                        rowBackgroundColor={"#eee"}
                                        rowHeight={30}
                                        rowRadius={5}
                                        iconColor={"#00a2dd"}
                                        iconSize={20}
                                        selectedIconName={"ios-checkmark-circle"}
                                        unselectedIconName={"ios-radio-button-off"}
                                        scrollViewHeight={130}
                                        selected={[]} // list of options which are selected by default
                                    />
                                </View>
                                : null
                        }
                        <TouchableOpacity onPress={() => this.updateInfo()} style={styles.advertise}>
                            <Text style={styles.buttonTitle}>تایید</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
function mapStateToProps(state) {
    return {
        user: state.auth.user,
    }
}
export default connect(mapStateToProps)(NewPlace);
