
import { StyleSheet, Dimensions} from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(234, 234, 234)',

    },
    scroll: {
        paddingTop: 0,
        paddingRight: 20,
        paddingLeft: 20,
        position: 'absolute',
        top: 190,
        bottom: 60,
        width: '100%'

    },
    body: {
        paddingTop: 10,
        position: 'relative',
        zIndex: 2,
        width: '100%'
    },
    imageContainer: {
        position: 'absolute',
        zIndex: 20,
        // top: 50,
        // left: 50,
        // width: 60,
        backgroundColor: 'red'
    },
    image: {
        position: 'absolute',
        zIndex: 22,
        top: 0,
        left: 0,
        width: 50,
        height: 50,
        resizeMode: 'stretch'
    },
    pic: {
        position: 'absolute',
        zIndex: 21,
        top: 3,
        left: 5,
        width: 40,
        height: 38,
        borderRadius: 200,
        resizeMode: 'stretch'
    },
    navitemContainer: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-between',
        transform: [
            {rotateY: '180deg'},
        ],
        // paddingTop: 45,
        // paddingBottom: 30,
        paddingLeft: 20,
        paddingBottom: 40,
        // height: 120
    },
    navContainer: {
        alignItems: "center",
        justifyContent: 'center',
        width: 184,
        // height: 156,
        // paddingBottom: 40,
        // marginLeft: 20,
        marginRight: 20,
    },
    text: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        // paddingBottom: 55,
        position: 'absolute',
        color: 'black',
        bottom: 15

    },
    mapStyle: {
        width: '100%',
        height: Dimensions.get('screen').height
    },
});
