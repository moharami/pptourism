import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, BackAndroid, TextInput, ActivityIndicator, AsyncStorage, BackHandler, Alert, Image} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://ppt.azarinpro.info';
Axios.defaults.baseURL = url;
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
import Loader from '../../components/loader'
import FooterMenu from "../../components/footerMenu/index";
import AppHeader from "../../components/appHeader";
import MapView, { Marker } from 'react-native-maps';
import marker1 from '../../assets/marker.png'
import marker_bold from '../../assets/marker-bold.png'
import marker_location from '../../assets/marker-location.png'
import pic from '../../assets/test.jpg'
import MapPlace from '../../components/mapPlace'

class Locations extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: false,
            homeActive: true,
            markers: [{
                coordinates: {
                    latitude: 35.715298,
                    longitude: 51.404343,
                },
            },
                {
                    // title: 'hello',
                    coordinates: {
                        latitude: 35.715299,
                        longitude: 51.414348,
                    },
                }],
            lat: null,
            lng: null

        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);

    }

    render() {
        if(this.state.loading){
            return (<Loader />)
        } else return (
            <View style={styles.container}>
                <AppHeader pageTitle="اطراف من" city="تهران" />
                <View style={styles.body}>
                    <MapView
                        style={styles.mapStyle}
                        initialRegion={{
                            latitude: 35.715298,
                            longitude: 51.404343,
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421,
                        }}
                    >
                        {this.state.markers.map((marker, index) => (
                            <Marker key={index} coordinate={marker.coordinates}
                                    title={marker.title} >
                                {/*<View style={styles.imageContainer}>*/}
                                    {/*<Image source={pic} style={styles.pic}/>*/}
                                    {/*<Image source={marker_bold} style={styles.image}/>*/}
                                {/*</View>*/}
                                <View style={styles.imageContainer}>
                                    <Image source={pic} style={[styles.pic, {width: 30, height: 28}]}/>
                                    <Image source={marker_location} style={[styles.image, { width: 40, height: 40}]}/>
                                </View>
                            </Marker>
                        ))}
                    </MapView>
                </View>
                <FooterMenu active="home"/>

                <ScrollView style={{position: 'absolute',
                    right: 5,
                    paddingRight: 30,
                    bottom: 40,
                    zIndex: 9990,  transform: [
                    { scaleX: -1},
                    // {  rotate: '180deg'},

                ],}}
                            horizontal={true} showsHorizontalScrollIndicator={false}
                >
                    <View style={styles.navitemContainer}>
                        <TouchableOpacity onPress={() => this.handlePress('home')}>
                            <View style={styles.navContainer}>
                                <MapPlace />
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.handlePress('home')}>
                            <View style={styles.navContainer}>
                                <MapPlace />
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.handlePress('home')}>
                            <View style={styles.navContainer}>
                                <MapPlace />
                            </View>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
export default Locations;
