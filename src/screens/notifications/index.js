import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, BackAndroid, TextInput, ActivityIndicator, AsyncStorage, BackHandler, Alert, Image} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import FIcon from 'react-native-vector-icons/dist/Feather';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://ppt.azarinpro.info';
Axios.defaults.baseURL = url;
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
import Loader from '../../components/loader'
import FooterMenu from "../../components/footerMenu/index";
import AppHeader from "../../components/appHeader";
import homeIcon from "../../assets/homeIcon.png";
import Notification from "../../components/notification";


class Notifications extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: false,
            homeActive: true

        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);

    }

    render() {
        if(this.state.loading){
            return (<Loader />)
        } else return (
            <View style={styles.container}>
                <AppHeader pageTitle="اعلانات" icon={false} />
                <ScrollView style={styles.scroll}>
                    <View style={styles.body}>
                        <Text style={[styles.title, {paddingLeft: '31%', paddingTop: 30, paddingRight: '35%'}]}>موردی یافت نشد</Text>
                        {/*<Notification/>*/}
                        {/*<Notification/>*/}
                    </View>
                </ScrollView>
                <FooterMenu active="notifications" />
            </View>
        );
    }
}
export default Notifications;
