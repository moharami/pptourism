import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, BackAndroid, TextInput, ActivityIndicator, AsyncStorage, BackHandler, Alert, Image} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/dist/FontAwesome5';
import FIcon from 'react-native-vector-icons/dist/Feather';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://ppt.azarinpro.info';
Axios.defaults.baseURL = url;
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
import Loader from '../../components/loader'
import FooterMenu from "../../components/footerMenu/index";
import FavoriteItem from "../../components/favoriteItem";
import AppHeader from "../../components/appHeader";
import {
    BallIndicator
} from 'react-native-indicators';

class Favorites extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: true,
            homeActive: true,
            likedItems: [],
            favorites: [],
            loadingFav: []
        }
        ;
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);

        AsyncStorage.getItem('token').then((info) => {
            if(info !== null) {
                const newInfo = JSON.parse(info);
                console.log('token', newInfo.token)
                Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;
                Axios.get('/business/liked').then(response=> {
                    this.setState({loading: false, likedItems: response.data.data});
                    console.log('response.data.data' , response.data.data)
                })
                    .catch((error) => {
                        Alert.alert('مشکلی در برقراری ارتباط با سرور رخ داده لطفا مجددا تلاش نمایید')
                        this.setState({loading: false});
                    });

            }
            else {
                console.log('it is not login')
                Actions.loginPage({travel: true})
            }
        });
    }
    showItems(id) {
        AsyncStorage.getItem('token').then((info) => {
            if(info !== null) {
                const newInfo = JSON.parse(info);
                console.log('token', newInfo.token);
                Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;
                let load = this.state.loadingFav;
                load[id] = true
                this.setState({loadingFav: load});
                Axios.get('/business/liked-business?cat_id='+id).then(response=> {
                    let load = this.state.loadingFav;
                    load[id] = false
                    let fav = this.state.favorites;
                    fav[id] = response.data.data.data;

                    this.setState({loadingFav: load, favorites: fav});
                    console.log('response show itemmmm ', response.data.data.data)
                })
                    .catch((error) => {
                        Alert.alert('مشکلی در برقراری ارتباط با سرور رخ داده لطفا مجددا تلاش نمایید')
                        this.setState({loading: false});
                    });

            }
            else {
                console.log('it is not login')
                Actions.loginPage({travel: true})
            }
        });
    }
    render() {
        if(this.state.loading){
            return (<Loader />)
        } else return (
            <View style={styles.container}>
                <AppHeader pageTitle="لیست علاقه مندی ها" icon={false} />
                <ScrollView style={styles.scroll}>
                    <View style={styles.body}>
                        {
                            this.state.likedItems.length !==0 ?this.state.likedItems.map((item, index)=>{
                                return <View style={{width: '100%'}} key={index}>
                                    <View style={[styles.catContainer, {justifyContent: this.state.loadingFav[item.cat_id] !== false ? 'space-between': 'flex-end'}]}>
                                        {
                                            this.state.loadingFav[item.cat_id] === true ?
                                                <View style={{width: 30, height: 30}}>
                                                    <BallIndicator color='#21b34f' size={20} style={{height: 90}} />
                                                </View> :
                                                (
                                                    this.state.loadingFav[item.cat_id] !== false ?
                                                        <TouchableOpacity onPress={() => this.showItems(item.cat_id)} style={styles.moreContainer}>
                                                            <Text style={styles.moreText}>اطلاعات بیشتر</Text>
                                                        </TouchableOpacity>
                                                        : null
                                                )
                                        }

                                        <View style={styles.right} >
                                            <Text style={styles.text}>{item.title}</Text>
                                            <Icon name={"layer-group"} size={20} color="rgb(50, 50, 50)" style={{paddingLeft: 10}} />
                                        </View>
                                    </View>
                                    {
                                        this.state.loadingFav[item.cat_id] === false ?
                                            this.state.favorites[item.cat_id].map((item2, index)=> {
                                                return <FavoriteItem item={item2} key={index} />
                                            })
                                            : null
                                    }
                                </View>

                            })  :   <Text style={[styles.text, {alignSelf: 'center', color: 'gray'}]}>موردی یافت نشد</Text>

                        }
                    </View>
                </ScrollView>
                <FooterMenu active="favorites" />
            </View>
        );
    }
}
export default Favorites;

