

import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(234, 234, 234)',

    },
    scroll: {
        paddingTop: 0,
        paddingRight: 20,
        paddingLeft: 20,
        // marginBottom: 90,
        // backgroundColor: 'red',
        position: 'absolute',
        top: 80,
        bottom: 60,
        width: '100%'

    },
    body: {
        // paddingBottom: 150,
        padding: 10,
        // alignItems: 'flex-start',
        // justifyContent: 'center',
        // backgroundColor: 'green',
        width: '100%',
        backgroundColor: 'white'
    },
    navitemContainer: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-between',
        transform: [
            {rotateY: '180deg'},
        ],
        paddingTop: 45,
        paddingBottom: 30,
        paddingRight: 20,
        height: 120
    },
    navContainer: {
        alignItems: "center",
        justifyContent: 'center',
        width: 94,
        height: 96,
        paddingBottom: 40,
        marginLeft: 20,
        backgroundColor: 'white'

    },
    catContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: "center",
        marginBottom: 20
    },
    text: {
        fontSize: 13,
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'right',
        color: 'black',
        alignSelf: 'flex-end'

    },
    moreContainer: {
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        paddingRight: 10,
        paddingLeft: 10,
        paddingTop: 3,
        paddingBottom: 3,
        backgroundColor: '#FFD700',
        // alignSelf: 'center',
    },
    moreText: {
        color: 'white',
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    right: {
        flexDirection: 'row',
    }
});
