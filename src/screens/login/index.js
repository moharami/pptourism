/**
 * Created by n.moharami on 4/23/2019.
 */
import React, {Component} from 'react';
import { Text, View, BackHandler, TextInput, AsyncStorage,TouchableOpacity, Image, Alert} from 'react-native';
import styles from './styles'
import Loader from '../../components/loader'
import {Actions} from 'react-native-router-flux'
import bg from '../../assets/bg4.png'
import logo from '../../assets/logo.png'
import google from '../../assets/google.jpg'
import usa from '../../assets/usa.png'

import {store} from '../../config/store';

class Login extends Component {
    constructor(props){
        super(props);
        this.state = {
            email: '',
            password: '',
            login: true,
            loading: false,
            loginModal: false,
            signupModal: false,
            logout: false
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    onBackPress(){
        this.logout();
        return true;
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    logout() {
        Alert.alert(
            'خروج از حساب کاربری',
            'آیا از  خروج خود مطمئن هستید؟',
            [
                {text: 'خیر', onPress: () => this.setState({logout: false}), style: 'cancel'},
                {text: 'بله', onPress: () =>  this.setState({logout: true}, () => {BackHandler.exitApp()})},
            ]
        );
    }
    componentWillUpdate() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    render() {
        return (
            <View style={styles.container}>
                <Image source={bg} style={{width: '100%'}} />
                <Image source={logo} style={{width: '30%', resizeMode: 'contain', position: 'absolute',
                    zIndex: 10,
                    top: -30,
                    left: '35%',
                }} />
                <Text style={styles.title}>پرتال فارسی توریسم</Text>
                <Text style={styles.subTitle}>Persian Portal Tourism</Text>
                <Text style={{
                    fontFamily: 'IRANSansMobile(FaNum)',
                    fontSize: 12,
                    textAlign: 'center',
                    color: 'gray',
                    position: 'absolute', bottom: 20, left: '27%'
                }}>طراحی و توسعه توسط آذرین وب</Text>

                <View style={styles.changeLanguage}>
                    <Image source={usa} style={{width: 18, height: 18, resizeMode: 'stretch'}} />
                    <Text style={{fontSize: 12, paddingLeft: 3, color: 'rgb(50, 50, 50)'}}>English</Text>
                </View>

                <View style={styles.body}>
                    <View style={styles.googleLogin}>
                        <Text>ورود سریع با حساب گوگل</Text>
                        <Image source={google} style={{width: 30, height: 30, resizeMode: 'stretch'}} />
                    </View>
                    <View style={styles.loginContainer}>
                        <TouchableOpacity onPress={() => Actions.places()} style={styles.button}>
                            <Text style={styles.label}>تمام مکان ها</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => Actions.events()} style={styles.button}>
                            <Text style={styles.label}>رویدادها</Text>
                        </TouchableOpacity>
                    </View>
                    {/*<View style={{alignItems: 'center', justifyContent: 'space-between'}}>*/}
                        {/*<Text style={{  fontFamily: 'IRANSansMobile(FaNum)',*/}
                        {/*fontSize: 14, textAlign: 'center', paddingRight: '5%',*/}
                        {/*color: 'white', paddingBottom: '15%'}}>طراحی و توسعه توسط آذرین وب</Text>*/}
                    {/*</View>*/}
                    {/*<View style={{alignItems: 'center', justifyContent: 'space-between'}}>*/}

                    {/*</View>*/}
                </View>
            </View>
        );
    }
}
export default Login;