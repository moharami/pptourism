/**
 * Created by n.moharami on 5/4/2019.
 */

import { StyleSheet, Dimensions} from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(244, 244, 244)'
    },
    scroll: {
        paddingTop: 0,
        paddingRight: 20,
        paddingLeft: 20,
        // position: 'absolute',
        // top: 190,
        // bottom: 60,
        width: '100%',
        paddingBottom: 50,
    },
    body: {
        paddingTop: 10,
        width: '100%',
        paddingBottom: 100
    },
    navitemContainer: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-between',
        paddingTop: 15,
        paddingBottom: 15,
        // paddingRight: 20,
        // height: 120,
        width: '100%'
    },
    navContainer: {
        alignItems: "center",
        justifyContent: 'center',
        width: 200,
        backgroundColor: 'white',
        padding: 20,
        borderRadius: 15,
        elevation: 3,
        marginRight: 20
    },
    text: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black'
    },
    top: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'flex-start',
        padding: 15,
        backgroundColor: 'white',
        width: '100%'
    },
    contentBody: {
        width: '100%',
        // backgroundColor:'red',
        flexDirection: 'row',
        marginBottom: 20,
        alignItems: "flex-start",
        justifyContent: 'space-between',
    },
    weatherRow: {
        flexDirection: 'row'
    },
    weather: {
        width: '40%',
        // paddingTop: 15,
        alignItems: 'flex-start',
        justifyContent: 'flex-start',

    },
    content: {
        // backgroundColor: 'white',
        // paddingRight: 30,
        width: '60%',
        paddingTop: 15,
        alignItems: 'flex-end',
        justifyContent: 'center'

    },
    button: {
        alignItems: "center",
        justifyContent: 'center',
        borderRadius: 7,
        borderColor: 'rgb(66, 183, 42)',
        borderWidth: 1,
        width: 70,
        marginLeft: 5
    },
    title: {
        fontSize: 13,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black'
    },
    placeTxt: {
        fontSize: 11,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'gray',
        textAlign: 'right'
    },
    infoRow: {
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingTop: 10
    },
    location: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: '#42b72a'
    },
    contentTxt: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'gray'
    },
    title1: {
        paddingRight: 10,
        fontSize: 13,
        color: 'black',
        textAlign: 'center',
        paddingTop: 10

    },
    subTitle: {
        paddingRight: 10,
        fontSize: 11,
        color: 'gray',
        textAlign: 'center',

    },
    iconRightEditContainer: {
        backgroundColor: 'rgb(55, 157, 237)',
        width: 18,
        height: 18,
        borderRadius: 32,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 3
    },
    advertise: {
        width: '50%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#8dc63f',
        padding: 5,
        alignSelf: 'center',
        marginTop: 30
        // marginTop: 0
    },
    buttonTitle: {
        fontSize: 12,
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    subImage: {
        // width: 160,
        width: Dimensions.get('screen').height*.25,
        height: 120,
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        overflow: 'hidden',
        margin: 5
    },
    imageText: {
        backgroundColor: "transparent",
        fontSize: 14,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: "white",
        textAlign: 'center'

    },
    advLabel: {
        fontSize: 13,
        color: 'rgb(70, 70, 70)',
        paddingLeft: '23%',
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingTop: 35,
        alignSelf: 'center'
    }
});