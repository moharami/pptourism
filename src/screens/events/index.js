
import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, BackAndroid, TextInput, ActivityIndicator, AsyncStorage, BackHandler, Alert, ImageBackground} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import SIcon from 'react-native-vector-icons/dist/SimpleLineIcons';
import FIcon from 'react-native-vector-icons/dist/Feather';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://ppt.azarinpro.info';
Axios.defaults.baseURL = url;
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
import Loader from '../../components/loader'
import PlaceFooter from "../../components/placeFooter";
import SlideShow from "../../components/slideShow";
import weather from "../../assets/weather.png";
import moment_jalaali from 'moment-jalaali'
import HTML from 'react-native-render-html';
import img from '../../assets/usa.png'

class Events extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: false,
            homeActive: true,
            modalTitle: '',
            weather: {}

        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        // this.setState({loading: true });
        if(this.props.subName === 'شهر') {
            this.setState({loading: true });
            Axios.get('/country/weather?city='+this.props.item.translations.title).then(response => {
                this.setState({loading: false, weather: response.data.data });
            })
                .catch((error) => {
                    console.log(error);
                    this.setState({loading: false});
                });
        }
    }
    render() {
        const myHtml = '<p>' +'rfsdfsdkfljsdkfldsf'+'</p>';
        const photos = [

        ];
        if(this.state.loading){
            return (<Loader />)
        } else return (
            <View style={styles.container}>
                <View style={styles.top}>
                    <TouchableOpacity onPress={() => this.onBackPress()}>
                        <FIcon name="arrow-left" size={23} color="gray" />
                    </TouchableOpacity>
                </View>
                {/*<View>*/}
                {/*<SlideShow activeSlide={1} />*/}
                {/*</View>*/}
                <ScrollView style={styles.scroll}>
                    <View style={styles.body}>
                        {
                            this.state.list.length !==0 ? this.state.list.map((item, index)=>{
                                return <View style={{borderRadius: 10}} key={index}>
                                    <ImageBackground source={{uri: baseImg}} style={styles.subImage}>
                                        <View style={{
                                            backgroundColor:'rgba(0,0,0,.4)',
                                            height: 120,
                                            width: "100%",
                                            alignItems: "center",
                                            justifyContent: "center",
                                            padding: 20,
                                            borderRadius: 10
                                        }}>
                                            <Text style={styles.imageText}>{item.title}</Text>
                                        </View>
                                    </ImageBackground>
                                </View>
                            }) : <Text style={styles.advLabel}>ویترینی برای نمایش وجود ندارد</Text>
                        }
                    </View>
                </ScrollView>
                <PlaceFooter active="general"/>
            </View>
        );
    }
}
export default Events;
