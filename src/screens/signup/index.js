/**
 * Created by n.moharami on 5/1/2019.
 */

import React, {Component} from 'react';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'http://ppt.azarinpro.info';
Axios.defaults.baseURL = url;
Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
import Loader from '../../components/loader'
import Icon from 'react-native-vector-icons/dist/Feather';

import {
    View,
    BackHandler,
    ScrollView,
    Text,
    TouchableOpacity,
    AsyncStorage,
    TextInput,
    Alert
} from "react-native";

class Signup extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            fname: '',
            lname: '',
            mobile: '',
            username: '',
            password: '',
            password_confirmation: '',
            email: '',
            skills: {key: 0, label: "افزودن مهارت", value: 0},
            skillsTitle: '',
            loading: false,
            src: null

        };
        this.onBackPress = this.onBackPress.bind(this);
    }

    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        // this.setState({loading1: true, loading4: true});
        // Axios.get('/country/country-data').then(response => {
        //     this.setState({countries: response.data, loading1: false});
        //
        //     // Axios.defaults.headers.common['Authorization'] = 'Bearer ' + newInfo.token;
        //     Axios.get('/core/skill/show').then(response => {
        //         this.setState({countries: response.data, loading1: false, loading4: false});
        //         console.log('response.data.data countrie', response.data)
        //     })
        //         .catch((error) => {
        //             Alert.alert('', 'خطایی رخ داده لطفا مجددا سعی نمایید')
        //             this.setState({loading1: false});
        //             console.log(error);
        //         });
        //     console.log('response.data.data countrie', response.data)
        // })
        //     .catch((error) => {
        //         Alert.alert('', 'خطایی رخ داده لطفا مجددا سعی نمایید')
        //         this.setState({loading1: false});
        //         console.log(error);
        //     });
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
        navigator.geolocation.clearWatch(this.watchID);
    }
    signup() {
        const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if(this.state.fname === '' || this.state.lname === '' || this.state.username === '' || this.state.mobile === ''  || this.state.password === ''|| this.state.password_confirmation === ''|| this.state.email === ''){
            Alert.alert('', 'لطفا تمام موارد الزامی را پر نمایید');
        }
        else if(this.state.password !== this.state.password_confirmation) {
            Alert.alert('', 'کلمه عبور با یکدیگر تطابق ندارد');
        }
        else if(this.state.password.length < 6) {
            Alert.alert('', 'کلمه عبور باید حداقل 6 کاراکتر داشته باشد')

        }
        else if(this.state.email !== '' && reg.test(this.state.email) === false) {
            Alert.alert('', 'لطفا ایمیل را صحیح وارد نمایید')
        }
        else {
            this.setState({loading: true});
            console.log('signup start')
            Axios.post('/signup', {
                fname: this.state.fname,
                lname: this.state.lname,
                mobile: this.state.mobile,
                display_name: this.state.username,
                password: this.state.password,
                email: this.state.email,
                password_confirmation: this.state.password_confirmation
            }).then(response=> {
                this.setState({loading: false});
                console.log('info', response.data);
                if(response.data.data && response.data.data === true) {
                    Alert.alert('', 'ثبت نام شما با موفقیت انجام شد')
                }
                else {
                    Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                }
            })
                .catch((error) => {
                    Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                    this.setState({loading: false});
                });
        }

    }
    render() {
        if(this.state.loading){
            return (<Loader />)
        } else return (
            <View style={styles.container}>
                <View style={styles.top}>
                    <TouchableOpacity onPress={() => this.onBackPress()}>
                        <Icon name="arrow-left" size={23} color="gray" />
                    </TouchableOpacity>
                    <Text style={[styles.title, {paddingLeft: '39%'}]}>ثبت نام</Text>
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.body}>
                        <Text style={styles.text}>نام</Text>
                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginBottom: 20, marginTop: 5, borderRadius: 7 }}>
                            <TextInput
                                placeholder={'نام'}
                                placeholderTextColor={'gray'}
                                underlineColorAndroid='transparent'
                                value={this.state.fname}
                                style={{
                                    height: 40,
                                    paddingRight: 15,
                                    width: '100%',
                                    color: 'gray',
                                    fontSize: 14,
                                    // textAlign: 'right',
                                    // elevation: 4,
                                }}
                                onChangeText={(text) => {this.setState({fname: text})}}
                            />
                        </View>

                        <Text style={styles.text}>نام خانوادگی</Text>
                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginTop: 5, marginBottom: 10, borderRadius: 7}}>
                            <TextInput
                                placeholder={'نام خانوادگی'}
                                placeholderTextColor={'gray'}
                                underlineColorAndroid='transparent'
                                value={this.state.lname}
                                style={{
                                    height: 40,
                                    paddingRight: 15,
                                    width: '100%',
                                    color: 'gray',
                                    fontSize: 14,
                                    // textAlign: 'right'
                                }}
                                onChangeText={(text) => {this.setState({lname: text})}}
                            />
                        </View>
                        <Text style={styles.text}>موبایل(اختیاری)</Text>
                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginTop: 5, marginBottom: 10, borderRadius: 7}}>
                            <TextInput
                                placeholder={'موبایل'}
                                keyboardType={'numeric'}
                                placeholderTextColor={'gray'}
                                underlineColorAndroid='transparent'
                                value={this.state.mobile}
                                style={{
                                    height: 40,
                                    paddingRight: 15,
                                    width: '100%',
                                    color: 'gray',
                                    fontSize: 14
                                }}
                                onChangeText={(text) => {this.setState({mobile: text})}}
                            />
                        </View>
                        <Text style={styles.text}>نام کاربری</Text>
                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginTop: 5, marginBottom: 10, borderRadius: 7}}>
                            <TextInput
                                placeholder={'نام کاربری'}
                                placeholderTextColor={'gray'}
                                underlineColorAndroid='transparent'
                                value={this.state.username}
                                style={{
                                    height: 40,
                                    paddingRight: 15,
                                    width: '100%',
                                    color: 'gray',
                                    fontSize: 14
                                }}
                                onChangeText={(text) => {this.setState({username: text})}}
                            />
                        </View>
                        <Text style={styles.text}>ایمیل</Text>
                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginTop: 5, marginBottom: 10, borderRadius: 7}}>
                            <TextInput
                                keyboardType={'email-address'}
                                placeholder={'ایمیل'}
                                placeholderTextColor={'gray'}
                                underlineColorAndroid='transparent'
                                value={this.state.email}
                                style={{
                                    height: 40,
                                    paddingRight: 15,
                                    width: '100%',
                                    color: 'gray',
                                    fontSize: 14,
                                }}
                                onChangeText={(text) => {this.setState({email: text})}}
                            />
                        </View>
                        <Text style={styles.text}>کلمه عبور</Text>
                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginTop: 5, marginBottom: 10, borderRadius: 7}}>
                            <TextInput
                                placeholder={'کلمه عبور'}
                                secureTextEntry
                                placeholderTextColor={'gray'}
                                underlineColorAndroid='transparent'
                                value={this.state.password}
                                style={{
                                    height: 40,
                                    paddingRight: 15,
                                    width: '100%',
                                    color: 'gray',
                                    fontSize: 14,
                                    textAlign: 'right'
                                }}
                                onChangeText={(text) => {this.setState({password: text})}}
                            />
                        </View>
                        <Text style={styles.text}>تکرار کلمه عبور</Text>
                        <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '95%', backgroundColor: 'white', height: 42, marginTop: 5, marginBottom: 10, borderRadius: 7}}>
                            <TextInput
                                placeholder={'کلمه عبور'}
                                secureTextEntry
                                placeholderTextColor={'gray'}
                                underlineColorAndroid='transparent'
                                value={this.state.password_confirmation}
                                style={{
                                    height: 40,
                                    paddingRight: 15,
                                    width: '100%',
                                    color: 'gray',
                                    fontSize: 14,
                                    textAlign: 'right'
                                }}
                                onChangeText={(text) => {this.setState({password_confirmation: text})}}
                            />
                        </View>
                        <TouchableOpacity onPress={() => this.signup()} style={styles.advertise}>
                            <Text style={styles.buttonTitle}>تایید</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
export default Signup;
