
const INITIAL_STATE = {
    businesses: [],
    events: []
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'BUSINESS_FETCHED':
            return { ...state, businesses: action.payload };
        case 'EVENTS_FETCHED':
            return { ...state, events: action.payload };

        case 'NEW_BUSINESS_FETCHED':
            const newArray = [...state.businesses, ...action.payload];
            return {
                ...state,
                posts: newArray,
            };
        case 'NEW_EVENTS_FETCHED':
            const newArray2 = [...state.events, ...action.payload];
            return {
                ...state,
                posts: newArray2,
            };
        default:
            return state;
    }
};
