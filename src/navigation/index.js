import React, {Component} from 'react';
import {Dimensions} from 'react-native';
import {Router, Stack, Scene, Drawer, Actions, ActionConst} from 'react-native-router-flux'
import Sidebar from '../sidebar'
import Home from '../screens/home'
import Search from '../screens/search'
import Locations from '../screens/locations'
import Notifications from '../screens/notifications'
import Favorites from '../screens/favorites'
import PlaceDetail from '../screens/placeDetail'
import PlaceDetailMore from '../screens/placeDetailMore'
import Tracking from '../screens/tracking'
import Comments from '../screens/comments'
import Profile from '../screens/profile'
import ProfileEdit from '../screens/profileEdit'
import Login from '../screens/login'
import LoginPage from '../screens/loginPage'
import Signup from '../screens/signup'
import NewPlace from '../screens/newPlace'
import Travelogue from '../screens/travelogue'
import TravelDetail from '../screens/travelDetail'
import Events from '../screens/events'
import Support from '../screens/support'
import AddedPlace from '../screens/addedPlace'
import {store} from '../config/store';
import {Provider, connect} from 'react-redux';

class MainDrawerNavigator extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    componentWillReceiveProps(nextProps) {
        /** Active Drawer Swipe **/
        if (nextProps.navigation.state.index === 0)
            this._drawer.blockSwipeAbleDrawer(false);

        if (nextProps.navigation.state.index === 0 && this.props.navigation.state.index === 0) {
            this._drawer.blockSwipeAbleDrawer(false);
            this._drawer.close();
        }

        /** Block Drawer Swipe **/
        if (nextProps.navigation.state.index > 0) {
            this._drawer.blockSwipeAbleDrawer(true);
        }
    }
    render() {
        const RouterWidthRedux = connect()(Router);
        return (
            <Provider store={store}>
                <RouterWidthRedux
                    ref={ref => this._drawer = ref}
                    openDrawer={() =>this._drawer.open()}
                    closeDrawer={() =>this._drawer.close()}>
                    <Drawer
                        type="reset"
                        store={store}
                        drawerPosition='right'
                        key="drawer"
                        contentComponent={Sidebar}
                        drawerWidth={Dimensions.get('window').width* .77}
                        closeDrawer={() => this._drawer.close()}
                        hideNavBar>
                        <Scene>
                            <Scene key="home" initial component={Home} title="Home" hideNavBar />
                            <Scene key="search" component={Search} title="Search" hideNavBar />
                            <Scene key="locations" component={Locations} title="Locations" hideNavBar />
                            <Scene key="notifications" component={Notifications} title="Notifications" hideNavBar />
                            <Scene key="favorites" component={Favorites} title="Favorites" hideNavBar />
                            <Scene key="placeDetail" component={PlaceDetail} title="PlaceDetail" hideNavBar />
                            <Scene key="placeDetailMore" component={PlaceDetailMore} title="PlaceDetailMore" hideNavBar />
                            <Scene key="comments" component={Comments} title="Comments" hideNavBar />
                            <Scene key="tracking" component={Tracking} title="Tracking" hideNavBar />
                            <Scene key="profileEdit"  component={ProfileEdit} title="ProfileEdit" hideNavBar />
                            <Scene key="profile" component={Profile} title="Profile" hideNavBar />
                            <Scene key="login" component={Login} title="Login" hideNavBar />
                            <Scene key="signup" component={Signup} title="Signup" hideNavBar />
                            <Scene key="loginPage"  component={LoginPage} title="LoginPage" hideNavBar />
                            <Scene key="travelogue" component={Travelogue} title="Travelogue" hideNavBar />
                            <Scene key="events" component={Events} title="Events" hideNavBar />
                            <Scene key="newPlace" component={NewPlace} title="NewPlace" hideNavBar />
                            <Scene key="travelDetail" component={TravelDetail} title="TravelDetail" hideNavBar />
                            <Scene key="support" component={Support} title="Support" hideNavBar />
                            <Scene key="addedPlace" component={AddedPlace} title="AddedPlace" hideNavBar />
                        </Scene>
                    </Drawer>
                </RouterWidthRedux>
            </Provider>
        );
    }
}
export default MainDrawerNavigator;



