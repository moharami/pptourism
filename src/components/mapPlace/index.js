
import React, {Component} from 'react';
import {TouchableOpacity, View, Text, BackHandler, Image} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import image from '../../assets/test.jpg'

class MapPlace extends Component {

    constructor(props){
        super(props);
        this.state = {
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    render() {
        return (
            <View style={styles.container}>
                {/*<Image source={image} style={styles.image} />*/}
                <Image source={{uri: "https://www.athenaspahotel.com/media/cache/jadro_resize/rc/Tv22O4rW1550816149/jadroRoot/medias/_a1a8429.jpg"}} style={styles.image} />
                <View style={styles.content}>
                    <Text style={styles.title}>رستوران سنتی محمد طاهر</Text>
                    <View style={styles.infoRow}>
                        <Text style={styles.contentTxt}> . 1397/10/05</Text>
                        <Text style={styles.location}>ایران- تهران . </Text>
                    </View>
                </View>
                {/*<View style={styles.circleContainer}>*/}
                    {/*<Text style={styles.circleTxt}>4.5</Text>*/}
                {/*</View>*/}
            </View>
        );
    }
}
export default MapPlace;