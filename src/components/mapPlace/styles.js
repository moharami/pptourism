import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({

    container: {
        position: 'relative',
        zIndex: 1,
        width: '100%',
        marginBottom: 20,
        borderRadius: 20,

    },
    image: {
        width: '100%',
        height: 65,
        resizeMode: 'stretch',
        borderTopRightRadius: 60,
        borderTopLeftRadius: 60
    },
    content: {
        backgroundColor: 'white',
        borderBottomRightRadius: 15,
        borderBottomLeftRadius: 15,
        paddingBottom: 10

        // padding: 20

    },
    title: {
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black',
        paddingTop: 10,
        paddingRight: 10,
    },
    placeTxt: {
        fontSize: 11,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'gray'
    },
    infoRow: {
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingBottom: 3,
        paddingRight: 10
    },
    location: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: '#42b72a'
    },
    contentTxt: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'gray'
    },
    circleContainer: {
        width: 40,
        height: 40,
        borderRadius: 50,
        position: 'absolute',
        top: 40,
        right: -12,
        backgroundColor: '#42b72a',
        alignItems: 'center',
        justifyContent: 'center',
    },
    circleTxt: {
        fontSize: 11,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'white'
    }
});