import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({

    container: {
        position: 'relative',
        zIndex: 1,
        width: '100%',
        marginBottom: 20,
        borderRadius: 5,
        flexDirection: 'row',
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'

    },
    image: {
        width: '18%',
        height: 55,
        resizeMode: 'stretch',
        borderRadius: 40,
    },
    content: {
        backgroundColor: 'white',
        borderBottomRightRadius: 5,
        borderTopRightRadius: 5,
        // padding: 20,
        width: '67%',
        padding: 20,


    },
    title: {
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black'
    },
    placeTxt: {
        fontSize: 11,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'gray'
    },
    infoRow: {
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingTop: 10
    },
    location: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: '#42b72a'
    },
    contentTxt: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'gray'
    },
    labelContainer: {
        borderColor: '#42b72a',
        borderWidth: 1,
        paddingRight: 5,
        paddingLeft: 5,
        borderRadius: 3,
        position: 'absolute',
        top: 10,
        left: 10
    },
    label: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: '#42b72a'
    }
});