import React, {Component} from 'react';
import {TouchableOpacity, View, Text, BackHandler, Image} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import image from '../../assets/test.jpg'

class Notification extends Component {

    constructor(props){
        super(props);
        this.state = {
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    render() {
        return (
            <View style={styles.container}>
                {/*<Image source={image} style={styles.image} />*/}
                <View style={styles.content}>
                    <Text style={styles.title}>رستوران سنتی محمد طاهر</Text>
                    <View style={styles.infoRow}>
                        <Text style={styles.placeTxt}>رستوران</Text>
                    </View>
                </View>
                <Image source={{uri: "https://www.athenaspahotel.com/media/cache/jadro_resize/rc/Tv22O4rW1550816149/jadroRoot/medias/_a1a8429.jpg"}} style={styles.image} />
                <View style={styles.labelContainer}>
                    <Text style={styles.label}>آگهی</Text>
                </View>
            </View>
        );
    }
}
export default Notification;