import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({

    container: {
        position: 'relative',
        zIndex: 1,
        width: '100%',
        marginBottom: 20,
        borderRadius: 5
    },
    image: {
        width: '100%',
        height: 130,
        resizeMode: 'stretch',
        borderTopRightRadius: 5,
        borderTopLeftRadius: 5
    },
    content: {
        backgroundColor: 'white',
        borderBottomRightRadius: 5,
        borderBottomLeftRadius: 5,
        padding: 20

    },
    title: {
        fontSize: 13,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black',
        alignSelf: 'flex-end'
    },
    placeTxt: {
        fontSize: 11,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'gray'
    },
    infoRow: {
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingTop: 10
    },
    location: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: '#42b72a'
    },
    contentTxt: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'gray',
    },
    circleContainer: {
        width: 50,
        height: 50,
        borderRadius: 50,
        position: 'absolute',
        top: 100,
        left: 30,
        backgroundColor: '#42b72a',
        alignItems: 'center',
        justifyContent: 'center',
    },
    circleTxt: {
        fontSize: 14,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'white'
    }
});