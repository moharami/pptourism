import React, {Component} from 'react';
import {TouchableOpacity, View, Text, BackHandler, Image} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import image from '../../assets/test.jpg'
import moment_jalaali from 'moment-jalaali'

class Place extends Component {

    constructor(props){
        super(props);
        this.state = {
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    render() {
        return (
            <TouchableOpacity onPress={() => Actions.placeDetail({item: this.props.item, subName: this.props.name})} style={styles.container}>
                {/*<Image source={image} style={styles.image} />*/}
                {/*<Image source={{uri: "https://www.athenaspahotel.com/media/cache/jadro_resize/rc/Tv22O4rW1550816149/jadroRoot/medias/_a1a8429.jpg"}} style={styles.image} />*/}
                {
                    this.props.item.attachments.length !==0 ?
                        (
                            this.props.item.attachments.length === undefined ?
                                <Image source={{uri: "http://ppt.azarinpro.info/files?uid="+this.props.item.attachments.uid}} style={styles.image} />
                                :
                                <Image source={{uri: "http://ppt.azarinpro.info/files?uid="+this.props.item.attachments[0].uid}} style={styles.image} />
                        )
                        :
                        <Image source={{uri: "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw0NDQ0NDQ0NDQ0NDQ0NDg0NDQ8NDQ0NFREWFhURExMYHSggGBolGxUWITEhJSk3Li4uFx8zODMtNygtLjcBCgoKBQUFDgUFDisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIALcBEwMBIgACEQEDEQH/xAAaAAEBAQEBAQEAAAAAAAAAAAAAAgEDBAUH/8QANBABAQACAAEIBwgCAwAAAAAAAAECEQMEEiExQWFxkQUTFDJRUqEiM2JygYKxwdHhQvDx/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/EABQRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/AP0QAAAAAAGgA0GNGgxo3QMNN0AzRpWjQJ0K0Akbo0CTTdAJGgMY0BgAAAAAAAAAAAAANCNAGgDdDQNGm6boGabpum6BOjStGgTo0rRoEaZpemWAnTNL0nQJFMBLKpNBgUAAAAAAAAAAAIEBsUyNBrWRUAbI2RsgMkVI2RsgM03TZG6BOjS9GgRo0vRoHPTNOmk2AixNjppNgIsYupoJTV1FBIUAAAAAAAAAAAIEBUUyKgCoyKgNipCRUAkVISKkBmm6duFwMsuqdHxvRHq4XIZ/yu+6f5B4Zjvqd+HyPO9f2Z39fk932OHOzH+a48Tlnyz9b/gFcPkeE6/tXv6vJw9IcLXNyk1Pdv8ASMuNnbLbei711R7uLj6zDo7ZueIPj6ZY6WJsBzsTY6WJsBzsTXSooJqK6VFBzo2sAAAAAAAAAAAIEBeKkxcBsVGRUBUXw8LeiS290duQcPDLKzOb6Nzp1H0888OFPhOySdYPFwuQZX3rMe7rr2cPkuGPZu/G9LzcTl1vuzXfemvbctY7vZN0HPiceTqxyyvdLrzefPjcS9lxndLvzd/asO/yb7Vh3+QPFzMvhl5U9Xl8t8q9vtOHf5HtOHf5A8Pq8vlvlXs5HbzdWWa6tzsV7Th3+R7Th3+QPJyng2Z3Utl6eiOF4WXy5eVfR9qw7/JXD4+OV1N76+oHyc8bOuWeM052Pf6S68fCvFQc6mrqKCKjJ0rnkCKxtYAAAAAAAAAAAQIC4uIxXAVFRMXAdODnzcplOy7/AEfX5Vhz+HddOpzo+NH1vR/E52Gu3Ho/TsB86Prcf7u/lfO4/D5udnZ1zwfR4/3d/KD52Memcly12b+DjwctZS3qlfSlmt9nxB86zXRetjpx8pcrZ1OYDHp5Lwt3nXqnV4ufG4VmWpN76YDjXbkXv/tv9OOU10V25F7/AO2/0B6S97Hwrw17vSXvY+FeGgipqqmgioyXUZA51jawAAAAAAAAAAAgQF4riIqAuKiIqAuPX6P4nNzk7Muj9ex44vGg+l6R4fRMvh0Xw/7/AC78f7u/lJZxeH+bHyv/AKco+7y/KD50VKiV6OTcHndN92fUHNfCw511590e7icLHKas8NdjODwphNdfeC8ZqajQB8/luOs9/GbOQ+/+2/078vx3jv5b9K8/IL9v9t/oG+kvex8K8Ne30n72Phf5eG0GVFVUUGVzyXUZAisbWAAAAAAAAAAAECAuKRFQFRURFQFxUrnKuUH0/RfE6MsP3T+3q5V93n4Pkcm4vMzxy7Jenw7X2crjZq2WXs3AfIxs6N9Xb2PZjy6SamGpO/8A09HqeF8uH0PVcL5cPoDj7f8Ah+p7f+H6u3quF8uH0PVcL5cPoDj7f+H6s9v/AAfX/Tv6rhfLh9D1XC+XD6A83E5bMsbOZ1zXX/pHo/7z9t/mPZ6nhfLh9G4YcPG7kxl+M0Dx+lPex8L/AC8Fr2+lbOdjq9l/l4LQZU1tTQZUVVTQTWNrAAAAAAAAAAACBAVGpigbFbQ0FytlRFbBcrZUSt2DpK3bntuwXs2jbdgrZanbNgrbLU7ZaDbWWs2zYNtTaMAqKpNBNCgAAAAAAAAAAAANjWANawBTdpaCtt2jbQXs2nZsF7No23YK2zbNs2Cts2zbNg3bKMA2wYDU1rKDKAAAAAAAAAAAAAA1gDRjQaMAUMAVs2wBu27SArbNsAbsYwGjAAYAAwAAAAAAAAAAAAAAAAAABu2ANAAawBoAAwBrAAAAYAAAAAAAAAAAP//Z"}} style={styles.image} />
                }
                <View style={styles.content}>
                    <Text style={styles.title}>{this.props.item.title}</Text>
                    <Text style={styles.placeTxt}>{this.props.name}</Text>
                    <View style={styles.infoRow}>
                        {
                            this.props.item.owner_user && this.props.item.owner_user !== null ?
                                <Text style={styles.contentTxt}>توسط {this.props.item.owner_user.fname} {this.props.item.owner_user.lname}</Text>
                                : <Text style={styles.contentTxt}>  {this.props.item.created_at !== null ? moment_jalaali(this.props.item.created_at, 'YYYY-MM-DD').format('jYYYY/jMM/jDD') : null}</Text>
                        }
                        {/*<Text style={styles.contentTxt}> . 1397/10/05</Text>*/}
                        {/*<Text style={styles.location}>ایران- تهران . </Text>*/}

                        {
                            this.props.item.country || this.props.item.city ?
                                <Text style={styles.location}>{this.props.item.country ? this.props.item.country.title : null}- { this.props.item.city ? this.props.item.city.title : null} . </Text>
                                : (
                                this.props.item.city_name || this.props.item.country_name ?
                                    <Text style={styles.location}>{this.props.item.country_name ? this.props.item.country_name : null}{this.props.item.city_name && this.props.item.country_name ? '-' : null} { this.props.item.city_name ? this.props.item.city_name : null} . </Text>
                                    : null
                            )
                        }
                    </View>
                </View>
                {/*<View style={styles.circleContainer}>*/}
                    {/*<Text style={styles.circleTxt}>4.5</Text>*/}
                {/*</View>*/}
            </TouchableOpacity>
        );
    }
}
export default Place;