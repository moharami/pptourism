import React, {Component} from 'react';
import {TouchableOpacity, View, Text, BackHandler, AsyncStorage, Alert} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import FIcon from 'react-native-vector-icons/dist/Feather';
import EIcon from 'react-native-vector-icons/dist/Entypo';
import {Actions} from 'react-native-router-flux'

class AppHeader extends Component {

    constructor(props){
        super(props);
        this.state = {
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    filterAction() {
        AsyncStorage.getItem('token').then((info) => {
            if(info === null) {
                console.log('it is not login')
                Alert.alert('','لطفا ابتدا وارد شوید');
                Actions.loginPage({profile: true})
            }
            else {
                Actions.profile()
            }
        });
    }
    render() {
        return (
            <View style={styles.top}>
                {
                    this.props.profile === false || this.props.edit === true ?
                        <TouchableOpacity onPress={() => this.onBackPress()}>
                            <FIcon name="arrow-left" size={23} color="gray" />
                        </TouchableOpacity>
                        :
                        <TouchableOpacity onPress={() => this.filterAction()}>
                            <FIcon name="user" size={23} color="gray" />
                        </TouchableOpacity>
                }

                <TouchableOpacity onPress={() => null} style={styles.headerTitleContainer}>
                    <Text style={[styles.headerTitle, {fontSize: 12, color: 'gray'}]}>{this.props.city}</Text>
                    {
                        this.props.icon === false ? null :
                            <EIcon name="location-pin" size={25} color="gray" />

                    }
                    <Text style={styles.headerTitle}>{this.props.pageTitle}</Text>
                </TouchableOpacity>
                {
                    this.props.profile === false ?
                        <TouchableOpacity onPress={() => this.onBackPress()}>
                            <FIcon name="filter" size={23} color="gray" />
                        </TouchableOpacity>
                        :
                        this.props.edit ?
                            <TouchableOpacity onPress={() => this.props.updateInfo()} style={styles.iconRightEditContainer}>
                                <FIcon name="check" size={18} color="white" />
                            </TouchableOpacity>
                            :
                        <TouchableOpacity onPress={() => Actions.drawerOpen()}>
                            <Icon name="bars" size={25} color="gray" />
                        </TouchableOpacity>
                }
            </View>
        );
    }
}
export default AppHeader;