import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    top: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'space-between',
        padding: 15,
        backgroundColor: 'white',
        width: '100%'
    },
    headerTitleContainer: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'center',
    },
    headerTitle: {
        color: 'black',
        fontSize: 14,
        fontFamily: 'IRANSansMobile(FaNum)',
        // paddingRight: '34%'
    },
    name: {
        color: 'gray',
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',
    },
    iconRightEditContainer: {
        backgroundColor: 'rgba(51, 197, 117, 1)',
        width: 32,
        height: 32,
        borderRadius: 32,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10
    },
});