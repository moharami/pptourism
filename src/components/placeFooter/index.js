/**
 * Created by n.moharami on 4/18/2019.
 */
import React from 'react';
import {Text, View, TouchableOpacity, Image, Alert} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'

export default class PlaceFooter extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            active: 0,
            loading: false,
            modalVisible: false
        };
    }

    render(){
        // if(this.state.loading){
        //     return (<Loader />)
        // }
        // else
        return (
            <View style={styles.container}>
                <TouchableOpacity style={styles.item} onPress={() => Actions.placeDetail({comments: this.props.comments, item: this.props.item, subName: this.props.subName, fav: this.props.fav})}>
                    <View style={[styles.navContainer, { borderTopColor: this.props.active === 'general' ? '#21b34f':  'transparent', borderTopWidth: this.props.active === 'general' ? 2: 0}]}>
                        <Text style={{color: this.props.active === 'general' ? '#21b34f': 'black', fontSize: 12, fontFamily: 'IRANSansMobile(FaNum)'}} >اطلاعات کلی</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.item} onPress={() => Actions.placeDetailMore({comments: this.props.comments, item: this.props.item, fav: this.props.fav, subName: this.props.subName})}>
                    <View style={[styles.navContainer, { borderTopColor: this.props.active === 'detail' ? '#21b34f':  'transparent', borderTopWidth: this.props.active === 'detail' ? 2: 0}]}>
                        <Text style={{color:this.props.active === 'detail' ? '#21b34f': 'black', fontSize: 12, fontFamily: 'IRANSansMobile(FaNum)'}} >جزئیات</Text>
                    </View>
                </TouchableOpacity>
                {
                    this.props.active === 'travel' ?
                        <TouchableOpacity style={styles.item} onPress={() =>  Actions.travelogue({comments: this.props.comments, item: this.props.item, subName: this.props.subName, fav: this.props.fav })}>
                            <View style={[styles.navContainer, { borderTopColor: this.props.active === 'travel' ? '#21b34f':  'transparent', borderTopWidth: this.props.active === 'travel' ? 2: 0}]}>
                                <Text style={{color: this.props.active === 'travel' ? '#21b34f': 'black', fontSize: 12, fontFamily: 'IRANSansMobile(FaNum)'}}>سفرنامه ها</Text>
                            </View>
                        </TouchableOpacity>
                        :
                        <TouchableOpacity style={styles.item} onPress={() =>  Actions.comments({comments: this.props.comments, item: this.props.item, subName: this.props.subName, fav: this.props.fav })}>
                            <View style={[styles.navContainer, { borderTopColor: this.props.active === 'comment' ? '#21b34f':  'transparent', borderTopWidth: this.props.active === 'comment' ? 2: 0}]}>
                                <Text style={{color: this.props.active === 'comment' ? '#21b34f': 'black', fontSize: 12, fontFamily: 'IRANSansMobile(FaNum)'}}>نظر و امتیاز</Text>
                            </View>
                        </TouchableOpacity>
                }

            </View>
        );
    }
}