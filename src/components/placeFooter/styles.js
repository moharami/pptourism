/**
 * Created by n.moharami on 4/18/2019.
 */

import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'center',
        backgroundColor: 'white',
        position: 'absolute',
        bottom: 0,
        right: 0,
        left: 0,
        zIndex: 9999
    },
    navContainer: {
        alignItems: "center",
        justifyContent: 'center',
        paddingBottom: 15,
        paddingTop: 15,
    },
    item: {
        width: '33%'
    }
});