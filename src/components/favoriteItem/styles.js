import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({

    container: {
        position: 'relative',
        zIndex: 1,
        width: '100%',
        marginBottom: 20,
        borderRadius: 10,
        flexDirection: 'row',
        backgroundColor: 'rgb(247, 247, 247)'
    },
    image: {
        width: '33%',
        height: 100,
        resizeMode: 'stretch',
        borderTopLeftRadius: 10,
        borderBottomLeftRadius: 10
    },
    content: {
        backgroundColor: 'rgb(247, 247, 247)',
        borderBottomRightRadius: 5,
        borderTopRightRadius: 5,
        // padding: 20,
        width: '67%',
        // paddingTop: 10,
        paddingRight: 10,
        alignItems: 'flex-end',
        justifyContent: 'center',

    },
    title: {
        fontSize: 13,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'black',
        textAlign: 'right'
    },
    placeTxt: {
        fontSize: 11,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'gray'
    },
    infoRow: {
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingTop: 10
    },
    location: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: '#42b72a'
    },
    contentTxt: {
        fontSize: 10,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'gray'
    },
    circleContainer: {
        width: 30,
        height: 30,
        borderRadius: 50,
        position: 'absolute',
        top: 40,
        left: '28%',
        backgroundColor: '#42b72a',
        alignItems: 'center',
        justifyContent: 'center',
    },
    circleTxt: {
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'white'
    }
});