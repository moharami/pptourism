import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    commentContainer: {
        flex: 1,
        width: '100%',
        alignItems: 'flex-start',
        justifyContent: 'flex-end',
        // flexDirection: 'row',
        backgroundColor: 'white',
        padding: 5,
        paddingTop: 15,
        paddingBottom: 15,
        paddingLeft: 15,
        marginBottom: 20
        // borderBottomWidth: 1,
        // borderColor: 'rgb(207, 207, 207)'
    },
    contentContainer: {
        // paddingRight: 15
        width: '80%'
    },
    headerContainer: {
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        flexDirection: 'row',
        width: '100%'
    },
    title: {
        paddingRight: 10,
        fontSize: 14,
        color: 'black'
    },
    subTitle: {
        paddingRight: 10,
        fontSize: 12,
        color: 'gray'
    },
    labelContainer: {
        alignItems: 'flex-end',
        justifyContent: 'flex-end',
        padding: 15
    },
    label: {
        paddingRight: 4,
        paddingLeft: 4,
        color: 'rgb(66, 183, 42)',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12
    },
    rowContainer: {
        flexDirection: 'row'
    },
    right: {
        flexDirection: 'row',
        width: '100%',
        backgroundColor: 'white',
        alignItems: 'flex-start',
        justifyContent: 'flex-end',
        marginBottom: 20
    },
    content: {
        fontSize: 11,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'rgb(100, 100, 100)',
        paddingLeft: 5,
        lineHeight: 15,
        paddingRight: 15,
        paddingTop: 15

    },
    imageContainer: {
        // borderRadius: 100,
        padding: 23,
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor: 'rgb(168, 170, 173)',
        // marginRight: 10,
        marginLeft: 10,
        // margin: 15,
        backgroundColor: 'rgb(66,183, 42)'
    },
    image: {
        width: 20,
        height: 20,
        tintColor: 'white'
    }
});
