/**
 * Created by n.moharami on 4/22/2019.
 */
import React, {Component} from 'react';
import {TouchableOpacity, View, Text, Image} from 'react-native';
import styles from './styles'
import user from '../../assets/user.png'

class RouteItem extends Component {
    constructor(props){
        super(props);
        this.state = {
        };
    }
    render() {
        return (
            <View style={styles.right}>
                <View style={styles.labelContainer}>
                    <View style={styles.rowContainer}>
                        <Text style={styles.subTitle}>(امتیاز 5 / 3.5)</Text>
                        <Text style={styles.title}>رستوران علی بابا</Text>
                    </View>
                    <Text style={styles.subTitle}>لواسان</Text>
                </View>
                <View style={styles.imageContainer}>
                    <Image source={user} style={styles.image} />
                </View>
            </View>
        );
    }
}
export default RouteItem;