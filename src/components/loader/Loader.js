import React from 'react';
import {View, Text, ActivityIndicator} from 'react-native';
import {
    BallIndicator,
    BarIndicator,
    DotIndicator,
    MaterialIndicator,
    PacmanIndicator,
    PulseIndicator,
    SkypeIndicator,
    UIActivityIndicator,
    WaveIndicator,
} from 'react-native-indicators';

import styles from './styles';

const Loader = (props) => {
    return (
        <View style={styles.container}>
            {/*<Text style={[styles.text, {color: 'red'}]}>در حال دریافت اطلاعات</Text>*/}
            <View style={{width: '100%', height: 60}}>
                <SkypeIndicator color='#21b34f' size={50} style={{height: 90}} />
            </View>
            <Text style={[styles.text, {color: 'rgb(50, 50, 50)', paddingTop: 20}]}>در حال دریافت اطلاعات</Text>
        </View>
    );
};
export default Loader;

