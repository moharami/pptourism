
import React, {Component} from 'react';
import {TouchableOpacity, View, Text, Image} from 'react-native';
import styles from './styles'
import user from '../../assets/user.png'
import Icon from 'react-native-vector-icons/dist/FontAwesome'
import moment from 'moment'
import moment_jalaali from 'moment-jalaali'
moment.locale('fa')

class CommentItem extends Component {
    constructor(props){
        super(props);
        this.state = {
            harm: [],
            benefit: []
        };
    }
    componentWillMount() {
        if(this.props.item.data !== null) {
            const data = JSON.parse(this.props.item.data);
            console.log('data', data);
            let harm = [], benefit = [];
            data.map((item)=>{
                if(item.type === 'harm'){
                    harm.push(item.data)
                }
                else if(item.type === 'benefit'){
                    benefit.push(item.data)
                }
            })
            this.setState({harm: harm, benefit: benefit})
        }
    }
    render() {
        console.log('this.props.item.user.attachments', this.props.item.user.attachments)
        // console.log('this.props.item.user.attachments', this.props.item.user.attachments.length)
        return (
            <View style={styles.commentContainer}>

                <View style={styles.headerContainer}>
                    <View style={styles.left}>
                        <Text style={styles.label}>{this.props.item.point} / 5</Text>

                    </View>
                    <View style={styles.right}>
                        <View style={styles.labelContainer}>
                            <View style={styles.rowContainer}>
                                {/*<Text style={styles.subTitle}>(عضو سایت)</Text>*/}
                                <Text style={styles.title}>{this.props.item.user.fname} {this.props.item.user.lname}</Text>
                            </View>
                            <Text style={styles.subTitle}>{moment_jalaali(this.props.item.created_at, 'YYYY-MM-DD').format('jYYYY/jMM/jDD')}</Text>
                        </View>
                        <View style={styles.imageContainer}>
                            <Image  source={ this.props.item.user.attachments ? {uri :"http://ppt.azarinpro.info/files?uid="+ this.props.item.user.attachments.uid }: user} style={this.props.item.user.attachments ? styles.image2 : styles.image} />
                        </View>
                    </View>
                </View>
                <Text style={styles.content}>{this.props.item.comment}</Text>
                <Text style={[styles.text, {color: 'green', alignSelf: 'flex-end', paddingTop: 10, paddingRight: 20}]}>مزایا: </Text>
                {
                    this.state.harm.length !==0 && this.state.harm.map((item, index)=> {
                       return <View style={styles.footer} key={index}>
                           <Text style={styles.text}>{item}</Text>
                           <Icon name={"circle"} size={10} color="green" style={{paddingLeft: 10}} />
                       </View>
                    })
                }
                <Text style={[styles.text, {color: '#ae1b1f', alignSelf: 'flex-end', paddingTop: 10, paddingRight: 20}]}>معایب: </Text>
                {
                    this.state.benefit.length !==0 && this.state.benefit.map((item, index)=> {
                        return <View style={styles.footer} key={index}>
                            <Text style={styles.text}>{item}</Text>
                            <Icon name={"circle"} size={10} color="#ae1b1f" style={{paddingLeft: 10}} />
                        </View>
                    })
                }
            </View>
        );
    }
}
export default CommentItem;