import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    commentContainer: {
        flex: 1,
        width: '100%',
        alignItems: 'flex-start',
        justifyContent: 'flex-end',
        // flexDirection: 'row',
        backgroundColor: 'white',
        padding: 5,
        paddingTop: 15,
        paddingBottom: 15,
        paddingLeft: 15,
        marginBottom: 20
        // borderBottomWidth: 1,
        // borderColor: 'rgb(207, 207, 207)'
    },
    contentContainer: {
        // paddingRight: 15
        width: '80%'
    },
    headerContainer: {
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        flexDirection: 'row',
        width: '100%'
    },
    title: {
        paddingRight: 10,
        fontSize: 14,
        color: 'black'
    },
    subTitle: {
        paddingRight: 10,
        fontSize: 12,
        color: 'gray',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    labelContainer: {
        alignItems: 'flex-end',
        justifyContent: 'flex-end'
    },
    label: {
        paddingRight: 4,
        paddingLeft: 4,
        color: 'rgb(66, 183, 42)',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12
    },
    rowContainer: {
        flexDirection: 'row'
    },
    right: {
        flexDirection: 'row'
    },
    content: {
        fontSize: 11,
        fontFamily: 'IRANSansMobile(FaNum)',
        color: 'rgb(100, 100, 100)',
        paddingLeft: 5,
        lineHeight: 15,
        paddingRight: 15,
        paddingTop: 15,
        alignSelf: 'flex-end'

    },
    imageContainer: {
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgb(168, 170, 173)',
        marginRight: 10,
        marginLeft: 10,
        width: 30,
        height: 30,
    },
    image: {
        padding: 10,
        width: 20,
        height: 20,
        tintColor: 'white'
    },
    image2: {
        width: 30,
        height: 30,
        borderRadius: 30,
    },
    footer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingRight: 35,
        padding: 5
    }
});