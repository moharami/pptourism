import React from 'react';
import {Text, View, TouchableOpacity, Image, Alert} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import FIcon from 'react-native-vector-icons/dist/Feather';
import AIcon from 'react-native-vector-icons/dist/AntDesign';
import SIcon from 'react-native-vector-icons/dist/SimpleLineIcons';
import location from '../../assets/fLocation.png';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import Axios from 'axios'
import AlertModal from './alertModal';
export const url = 'http://ppt.azarinpro.info';
Axios.defaults.baseURL = url;Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
import Loader from '../../components/loader'

export default class FooterMenu extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            active: 0,
            loading: false,
            modalVisible: false
        };
    }

    render(){
        // if(this.state.loading){
        //     return (<Loader />)
        // }
        // else
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={() => Actions.home()}>
                    <View style={[styles.navContainer, { borderTopColor: this.props.active === 'profile' ? 'rgba(255, 193, 39, 1)':  'transparent', borderTopWidth: this.props.active === 'profile' ? 2: 0}]}>
                        <AIcon name="home" size={25} color={this.props.active === 'home' ? '#21b34f': 'black'} />
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => Actions.search()}>
                    <View style={[styles.navContainer, { borderTopColor: this.props.active === 'profile' ? 'rgba(255, 193, 39, 1)':  'transparent', borderTopWidth: this.props.active === 'profile' ? 2: 0}]}>
                        <AIcon name="search1" size={25} color={this.props.active === 'search' ? '#42b72a' : 'black'} />
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => Actions.locations()}>
                    <View style={[styles.navContainer, { borderTopColor: this.props.active === 'bime' ? 'rgba(255, 193, 39, 1)':  'transparent', borderTopWidth: this.props.active === 'bime' ? 2: 0}]}>
                        <Image source={location} style={{ width: 90, resizeMode: 'contain', position: 'relative', bottom: 10}} />
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => Actions.notifications()}>
                    <View style={[styles.navContainer, { borderTopColor: this.props.active === 'bime' ? 'rgba(255, 193, 39, 1)':  'transparent', borderTopWidth: this.props.active === 'bime' ? 2: 0}]}>
                        <AIcon name="bells" size={25} color={this.props.active === 'notifications' ? '#21b34f': 'black'} />
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() =>  Actions.favorites({openDrawer: this.props.openDrawer})}>
                    <View style={[styles.navContainer, { borderTopColor: this.props.active === 'blog' ? 'rgba(255, 193, 39, 1)':  'transparent', borderTopWidth: this.props.active === 'blog' ? 2: 0}]}>
                        <SIcon name="pin" size={25} color={this.props.active === 'favorites' ? '#21b34f': 'black'} />
                    </View>
                </TouchableOpacity>
                <AlertModal
                    closeModal={(title) => this.closeModal(title)}
                    modalVisible={this.state.modalVisible}
                    modelCats={this.state.modelCats}
                />
            </View>
        );
    }
}